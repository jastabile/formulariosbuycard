/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datatypes;

import java.sql.Timestamp;

/**
 *
 * @author Javier
 */
public class DTFormularioAnda {
    
    private int id;
    private String razonSocial;
    private String nombreComercial;
    private String RUT;
    private String nombreContacto;
    private String cedula;
    private String caracterDe;
    private String telefono;
    private String calle;
    private String numero;
    private String apto;
    private String ciudad;
    private String departamento;
    private String celular;
    private String mail;
    private String calleCentral;
    private String numeroCentral;
    private String aptoCentral;
    private String ciudadCentral;
    private String departamentoCentral;
    private String telefonoCentral;
    private String celularCentral;
    private String ramoPpal;
    private String acuerdo;
    private String POS;
    private String fecha;
    private String cuentaCorriente;
    private String cajaAhorro;
    private String titularCuenta;
    private String banco;
    private String localidadBanco;
    private String sucursal;
    private String localidad;
    private String usuario;
    private String descuento;
    private String descuentoCuota;
    private Timestamp horaGuardado;

    public DTFormularioAnda(int id, String razonSocial, String nombreComercial, String RUT,
            String nombreContacto, String cedula, String caracterDe, String telefono, 
            String calle, String numero, String apto, String ciudad, String departamento, 
            String celular, String mail, String calleCentral, String numeroCentral, 
            String aptoCentral, String ciudadCentral, String departamentoCentral, 
            String telefonoCentral, String celularCentral, String ramoPpal, String acuerdo, 
            String POS, String fecha, String cuentaCorriente, String cajaAhorro, String titularCuenta,
            String banco, String localidadBanco, String sucursal, String localidad, 
            String usuario, String descuento, String descuentoCuota) {
        this.id = id;
        this.razonSocial = razonSocial;
        this.nombreComercial = nombreComercial;
        this.RUT = RUT;
        this.nombreContacto = nombreContacto;
        this.cedula = cedula;
        this.caracterDe = caracterDe;
        this.telefono = telefono;
        this.calle = calle;
        this.numero = numero;
        this.apto = apto;
        this.ciudad = ciudad;
        this.departamento = departamento;
        this.celular = celular;
        this.mail = mail;
        this.calleCentral = calleCentral;
        this.numeroCentral = numeroCentral;
        this.aptoCentral = aptoCentral;
        this.ciudadCentral = ciudadCentral;
        this.departamentoCentral = departamentoCentral;
        this.telefonoCentral = telefonoCentral;
        this.celularCentral = celularCentral;
        this.ramoPpal = ramoPpal;
        this.acuerdo = acuerdo;
        this.fecha = fecha;
        this.cuentaCorriente = cuentaCorriente;
        this.cajaAhorro = cajaAhorro;
        this.titularCuenta = titularCuenta;
        this.banco = banco;
        this.localidadBanco = localidadBanco;
        this.sucursal = sucursal;
        this.localidad = localidad;
        this.usuario = usuario;
        this.descuento = descuento;
        this.descuentoCuota = descuentoCuota;
        this.POS = POS;
    }

    public DTFormularioAnda(int id, String razonSocial, String nombreComercial, String RUT, 
            String nombreContacto, String cedula, String caracterDe, String telefono, 
            String calle, String numero, String apto, String ciudad, String departamento, 
            String celular, String mail, String calleCentral, String numeroCentral, 
            String aptoCentral, String ciudadCentral, String departamentoCentral, 
            String telefonoCentral, String celularCentral, String ramoPpal, String acuerdo, 
            String POS, String fecha, String cuentaCorriente, String cajaAhorro, String titularCuenta, String banco,
            String localidadBanco, String sucursal, String localidad, String usuario, String descuento, 
            String descuentoCuota, Timestamp horaGuardado) {
        this.id = id;
        this.razonSocial = razonSocial;
        this.nombreComercial = nombreComercial;
        this.RUT = RUT;
        this.nombreContacto = nombreContacto;
        this.cedula = cedula;
        this.caracterDe = caracterDe;
        this.telefono = telefono;
        this.calle = calle;
        this.numero = numero;
        this.apto = apto;
        this.ciudad = ciudad;
        this.departamento = departamento;
        this.celular = celular;
        this.mail = mail;
        this.calleCentral = calleCentral;
        this.numeroCentral = numeroCentral;
        this.aptoCentral = aptoCentral;
        this.ciudadCentral = ciudadCentral;
        this.departamentoCentral = departamentoCentral;
        this.telefonoCentral = telefonoCentral;
        this.celularCentral = celularCentral;
        this.ramoPpal = ramoPpal;
        this.acuerdo = acuerdo;
        this.POS = POS;
        this.fecha = fecha;
        this.cuentaCorriente = cuentaCorriente;
        this.cajaAhorro = cajaAhorro;
        this.titularCuenta = titularCuenta;
        this.banco = banco;
        this.localidadBanco = localidadBanco;
        this.sucursal = sucursal;
        this.localidad = localidad;
        this.usuario = usuario;
        this.descuento = descuento;
        this.descuentoCuota = descuentoCuota;
        this.horaGuardado = horaGuardado;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitularCuenta() {
        return titularCuenta;
    }

    public String getBanco() {
        return banco;
    }

    public String getLocalidadBanco() {
        return localidadBanco;
    }

    public String getFecha() {
        return fecha;
    }

    public String getCuentaCorriente() {
        return cuentaCorriente;
    }

    public String getCajaAhorro() {
        return cajaAhorro;
    }

    public String getSucursal() {
        return sucursal;
    }

    public String getLocalidad() {
        return localidad;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getDescuento() {
        return descuento;
    }

    public String getDescuentoCuota() {
        return descuentoCuota;
    }

    public int getId() {
        return id;
    }

    public String getCalleCentral() {
        return calleCentral;
    }

    public String getNumeroCentral() {
        return numeroCentral;
    }

    public String getAptoCentral() {
        return aptoCentral;
    }

    public String getCiudadCentral() {
        return ciudadCentral;
    }

    public String getDepartamentoCentral() {
        return departamentoCentral;
    }

    public String getTelefonoCentral() {
        return telefonoCentral;
    }

    public String getCelularCentral() {
        return celularCentral;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public String getRUT() {
        return RUT;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public String getCedula() {
        return cedula;
    }

    public String getCaracterDe() {
        return caracterDe;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getCalle() {
        return calle;
    }

    public String getNumero() {
        return numero;
    }

    public String getApto() {
        return apto;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getDepartamento() {
        return departamento;
    }

    public String getCelular() {
        return celular;
    }

    public String getMail() {
        return mail;
    }

    public String getRamoPpal() {
        return ramoPpal;
    }

    public String getAcuerdo() {
        return acuerdo;
    }

    public String getPOS() {
        return POS;
    }

    public Timestamp getHoraGuardado() {
        return horaGuardado;
    }
    
    
}
