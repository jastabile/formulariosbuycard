/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datatypes;

public class DTCiudad {
    private String codigoCiudad;
    private String localidad;
    private String codigoPostal;
    private String codigoDepartamento;
    private String departamento;

    public DTCiudad(String codigoCiudad, String localidad, String codigoPostal, String codigoDepartamento, String departamento) {
        this.codigoCiudad = codigoCiudad;
        this.localidad = localidad;
        this.codigoPostal = codigoPostal;
        this.codigoDepartamento = codigoDepartamento;
        this.departamento = departamento;
    }

    public String getCodigoCiudad() {
        return codigoCiudad;
    }

    public String getLocalidad() {
        return localidad;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public String getDepartamento() {
        return departamento;
    }

    @Override
    public String toString() {
        return localidad + " / " + departamento;
    }
    
}
