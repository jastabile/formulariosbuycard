/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datatypes;

/**
 *
 * @author Javier
 */
public class DTBanco {
    private String nombre;
    private String entidadMaster;
    private String entidadCabal;
    private String sucursal;
    private String direccion;

    public DTBanco(String nombre, String entidadMaster, String entidadCabal, String sucursal, String direccion) {
        this.nombre = nombre;
        this.entidadMaster = entidadMaster;
        this.entidadCabal = entidadCabal;
        this.sucursal = sucursal;
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEntidadMaster() {
        return entidadMaster;
    }

    public String getEntidadCabal() {
        return entidadCabal;
    }

    public String getSucursal() {
        return sucursal;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEntidadMaster(String entidadMaster) {
        this.entidadMaster = entidadMaster;
    }

    public void setEntidadCabal(String entidadCabal) {
        this.entidadCabal = entidadCabal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
}
