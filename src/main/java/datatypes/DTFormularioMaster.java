/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datatypes;

import java.sql.Timestamp;


public class DTFormularioMaster {
    
    private int id;
    private String pos;
    private String entidad;
    private String sucursal;
    private String nombreBanco;
    private String tarjetaCredito;
    private String maestro;
    private String posnetCelular;
    private String dia;
    private String mes;
    private String anio;
    private String nombreFantasia;
    private String razonSocial;
    private String calle;
    private String numero;
    private String ubicacion;
    private String codigoPostal;
    private String localidad;
    private String direccionAdicional;
    private String telefonos;
    private String CUIT;
    private String RUT;
    private String nombreResponsable;
    private String tipoDocumento;
    private String documento;
    private String ramo;
    private String descripcion;
    private String mail;
    private String tDebito;
    private String numCuentaDebito;
    private String entidadDebito;
    private String sucursalDebito;
    private String plazoPago;
    private String porcDescuento;
    private String shopping;
    private String zonaComercial;
    private String tCredito;
    private String numCuentaCredito;
    private String entidadCredito;
    private String sucursalCredito;
    private String operaEnDolares;
    private String tDolaresCredito;
    private String numCuentaDolaresCredito;
    private String entidadDolaresCredito;
    private String sucursalDolaresCredito;
    private String cuotasCobroAnticipado;
    private String plazoPagoPesos;
    private String plazoPagoDolares;
    private String porcDescuentoPesos;
    private String porcDescuentoDolares;
    private Timestamp horaGuardado;
    private boolean val;

    public DTFormularioMaster(int id, String pos, String entidad, String sucursal,
            String nombreBanco, String tarjetaCredito, String maestro, String posnetCelular, 
            String dia, String mes, String anio, String nombreFantasia, String razonSocial,
            String calle, String numero, String ubicacion, String codigoPostal, String localidad, 
            String direccionAdicional, String telefonos, String CUIT, String RUT, 
            String nombreResponsable, String tipoDocumento, String documento, String ramo, 
            String descripcion, String mail, String tDebito, String numCuentaDebito, 
            String entidadDebito, String sucursalDebito, String plazoPago, String porcDescuento, 
            String shopping, String zonaComercial, String tCredito, String numCuentaCredito,
            String entidadCredito, String sucursalCredito, String operaEnDolares, String tDolaresCredito, 
            String numCuentaDolaresCredito, String entidadDolaresCredito, String sucursalDolaresCredito,
            String cuotasCobroAnticipado, String plazoPagoPesos, String plazoPagoDolares, 
            String porcDescuentoPesos, String porcDescuentoDolares, boolean val) {
        this.id = id;
        this.pos = pos;
        this.entidad = entidad;
        this.sucursal = sucursal;
        this.nombreBanco = nombreBanco;
        this.tarjetaCredito = tarjetaCredito;
        this.maestro = maestro;
        this.posnetCelular = posnetCelular;
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
        this.nombreFantasia = nombreFantasia;
        this.razonSocial = razonSocial;
        this.calle = calle;
        this.numero = numero;
        this.ubicacion = ubicacion;
        this.codigoPostal = codigoPostal;
        this.localidad = localidad;
        this.direccionAdicional = direccionAdicional;
        this.telefonos = telefonos;
        this.CUIT = CUIT;
        this.RUT = RUT;
        this.nombreResponsable = nombreResponsable;
        this.tipoDocumento = tipoDocumento;
        this.documento = documento;
        this.ramo = ramo;
        this.descripcion = descripcion;
        this.mail = mail;
        this.tDebito = tDebito;
        this.numCuentaDebito = numCuentaDebito;
        this.entidadDebito = entidadDebito;
        this.sucursalDebito = sucursalDebito;
        this.plazoPago = plazoPago;
        this.porcDescuento = porcDescuento;
        this.shopping = shopping;
        this.zonaComercial = zonaComercial;
        this.tCredito = tCredito;
        this.numCuentaCredito = numCuentaCredito;
        this.entidadCredito = entidadCredito;
        this.sucursalCredito = sucursalCredito;
        this.operaEnDolares = operaEnDolares;
        this.tDolaresCredito = tDolaresCredito;
        this.numCuentaDolaresCredito = numCuentaDolaresCredito;
        this.entidadDolaresCredito = entidadDolaresCredito;
        this.sucursalDolaresCredito = sucursalDolaresCredito;
        this.cuotasCobroAnticipado = cuotasCobroAnticipado;
        this.plazoPagoPesos = plazoPagoPesos;
        this.plazoPagoDolares = plazoPagoDolares;
        this.porcDescuentoPesos = porcDescuentoPesos;
        this.porcDescuentoDolares = porcDescuentoDolares;
        this.horaGuardado = null;
        this.val = val;
    }

    public DTFormularioMaster(int id, String pos, String entidad, String sucursal, 
            String nombreBanco, String tarjetaCredito, String maestro, String posnetCelular, 
            String dia, String mes, String anio, String nombreFantasia, String razonSocial, 
            String calle, String numero, String ubicacion, String codigoPostal, String localidad, 
            String direccionAdicional, String telefonos, String CUIT, String RUT, 
            String nombreResponsable, String tipoDocumento, String documento, String ramo, 
            String descripcion, String mail, String tDebito, String numCuentaDebito, 
            String entidadDebito, String sucursalDebito, String plazoPago, String porcDescuento,
            String shopping, String zonaComercial, String tCredito, String numCuentaCredito, 
            String entidadCredito, String sucursalCredito, String operaEnDolares, 
            String tDolaresCredito, String numCuentaDolaresCredito, String entidadDolaresCredito, 
            String sucursalDolaresCredito, String cuotasCobroAnticipado, String plazoPagoPesos, 
            String plazoPagoDolares, String porcDescuentoPesos, String porcDescuentoDolares, 
            Timestamp horaGuardado, boolean val) {
        this.id = id;
        this.pos = pos;
        this.entidad = entidad;
        this.sucursal = sucursal;
        this.nombreBanco = nombreBanco;
        this.tarjetaCredito = tarjetaCredito;
        this.maestro = maestro;
        this.posnetCelular = posnetCelular;
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
        this.nombreFantasia = nombreFantasia;
        this.razonSocial = razonSocial;
        this.calle = calle;
        this.numero = numero;
        this.ubicacion = ubicacion;
        this.codigoPostal = codigoPostal;
        this.localidad = localidad;
        this.direccionAdicional = direccionAdicional;
        this.telefonos = telefonos;
        this.CUIT = CUIT;
        this.RUT = RUT;
        this.nombreResponsable = nombreResponsable;
        this.tipoDocumento = tipoDocumento;
        this.documento = documento;
        this.ramo = ramo;
        this.descripcion = descripcion;
        this.mail = mail;
        this.tDebito = tDebito;
        this.numCuentaDebito = numCuentaDebito;
        this.entidadDebito = entidadDebito;
        this.sucursalDebito = sucursalDebito;
        this.plazoPago = plazoPago;
        this.porcDescuento = porcDescuento;
        this.shopping = shopping;
        this.zonaComercial = zonaComercial;
        this.tCredito = tCredito;
        this.numCuentaCredito = numCuentaCredito;
        this.entidadCredito = entidadCredito;
        this.sucursalCredito = sucursalCredito;
        this.operaEnDolares = operaEnDolares;
        this.tDolaresCredito = tDolaresCredito;
        this.numCuentaDolaresCredito = numCuentaDolaresCredito;
        this.entidadDolaresCredito = entidadDolaresCredito;
        this.sucursalDolaresCredito = sucursalDolaresCredito;
        this.cuotasCobroAnticipado = cuotasCobroAnticipado;
        this.plazoPagoPesos = plazoPagoPesos;
        this.plazoPagoDolares = plazoPagoDolares;
        this.porcDescuentoPesos = porcDescuentoPesos;
        this.porcDescuentoDolares = porcDescuentoDolares;
        this.horaGuardado = horaGuardado;
        this.val = val;
    }

    public Timestamp getHoraGuardado() {
        return horaGuardado;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isVal() {
        return val;
    }
    
    public int getId() {
        return id;
    }

    public String getPos() {
        return pos;
    }

    public String getEntidad() {
        return entidad;
    }

    public String getSucursal() {
        return sucursal;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public String getTarjetaCredito() {
        return tarjetaCredito;
    }

    public String getMaestro() {
        return maestro;
    }

    public String getPosnetCelular() {
        return posnetCelular;
    }

    public String getDia() {
        return dia;
    }

    public String getMes() {
        return mes;
    }

    public String getAnio() {
        return anio;
    }

    public String getNombreFantasia() {
        return nombreFantasia;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getCalle() {
        return calle;
    }

    public String getNumero() {
        return numero;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public String getLocalidad() {
        return localidad;
    }

    public String getDireccionAdicional() {
        return direccionAdicional;
    }

    public String getTelefonos() {
        return telefonos;
    }

    public String getCUIT() {
        return CUIT;
    }

    public String getRUT() {
        return RUT;
    }

    public String getNombreResponsable() {
        return nombreResponsable;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public String getRamo() {
        return ramo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getMail() {
        return mail;
    }

    public String gettDebito() {
        return tDebito;
    }

    public String getNumCuentaDebito() {
        return numCuentaDebito;
    }

    public String getEntidadDebito() {
        return entidadDebito;
    }

    public String getSucursalDebito() {
        return sucursalDebito;
    }

    public String getPlazoPago() {
        return plazoPago;
    }

    public String getPorcDescuento() {
        return porcDescuento;
    }

    public String getShopping() {
        return shopping;
    }

    public String getZonaComercial() {
        return zonaComercial;
    }

    public String gettCredito() {
        return tCredito;
    }

    public String getNumCuentaCredito() {
        return numCuentaCredito;
    }

    public String getEntidadCredito() {
        return entidadCredito;
    }

    public String getSucursalCredito() {
        return sucursalCredito;
    }

    public String getOperaEnDolares() {
        return operaEnDolares;
    }

    public String gettDolaresCredito() {
        return tDolaresCredito;
    }

    public String getNumCuentaDolaresCredito() {
        return numCuentaDolaresCredito;
    }

    public String getEntidadDolaresCredito() {
        return entidadDolaresCredito;
    }

    public String getSucursalDolaresCredito() {
        return sucursalDolaresCredito;
    }

    public String getCuotasCobroAnticipado() {
        return cuotasCobroAnticipado;
    }

    public String getPlazoPagoPesos() {
        return plazoPagoPesos;
    }

    public String getPlazoPagoDolares() {
        return plazoPagoDolares;
    }

    public String getPorcDescuentoPesos() {
        return porcDescuentoPesos;
    }

    public String getPorcDescuentoDolares() {
        return porcDescuentoDolares;
    }
}
