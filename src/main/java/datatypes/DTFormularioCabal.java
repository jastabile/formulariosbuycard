/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datatypes;

import java.sql.Timestamp;
import java.util.Date;


        
public class DTFormularioCabal {
    
    private int id;
    private Date fecha;
    private String altaOModificacion;
    private String numEstablecimiento;
    private String nombreFantasia;
    private String razonSocial;
    private String RUT;
    private String tipoRUT;
    private String moneda;
    private String unificaCheques;
    private String tipoDePago;
    private String entidadSucursal;
    private String sucursal;
    private String cuentaNumero;
    private String entidadPagadora;
    private String calle;
    private String numPuerta;
    private String apto;
    private String ciudad;
    private String codPostalDepartamento;
    private String telefono;
    private String pos;
    private String representanteLegal;
    private String ramoPpal;
    private String personaContacto;
    private String mail;
    private String callePago;
    private String numPuertaPago;
    private String aptoPago;
    private String ciudadPago;
    private String codigoPostalDepartamentoPago;
    private String grupo;
    private String subgrupo;
    private String observaciones;
    private String descuento;
    private String descuentoCuota;
    private String usuario;
    private Timestamp horaGuardado;


    public DTFormularioCabal(int id, Date fecha, String altaOModificacion, String numEstablecimiento, String nombreFantasia,
            String razonSocial, String RUT, String tipoRUT, String moneda, String unificaCheques, String tipoDePago, String entidadSucursal,
            String sucursal, String cuentaNumero, String entidadPagadora, String calle, String numPuerta, String apto, String ciudad,
            String codPostalDepartamento, String telefono, String pos, String representanteLegal, String ramoPpal, String personaContacto, 
            String mail, String callePago, String numPuertaPago, String aptoPago, String ciudadPago, String codigoPostalDepartamentoPago, 
            String grupo, String subgrupo, String observaciones, String descuento, String descuentoCuota, String usuario) {
        this.id = id;
        this.fecha = fecha;
        this.altaOModificacion = altaOModificacion;
        this.numEstablecimiento = numEstablecimiento;
        this.nombreFantasia = nombreFantasia;
        this.razonSocial = razonSocial;
        this.RUT = RUT;
        this.tipoRUT = tipoRUT;
        this.moneda = moneda;
        this.unificaCheques = unificaCheques;
        this.tipoDePago = tipoDePago;
        this.entidadSucursal = entidadSucursal;
        this.sucursal = sucursal;
        this.cuentaNumero = cuentaNumero;
        this.entidadPagadora = entidadPagadora;
        this.calle = calle;
        this.numPuerta = numPuerta;
        this.apto = apto;
        this.ciudad = ciudad;
        this.codPostalDepartamento = codPostalDepartamento;
        this.telefono = telefono;
        this.pos = pos;
        this.representanteLegal = representanteLegal;
        this.ramoPpal = ramoPpal;
        this.personaContacto = personaContacto;
        this.mail = mail;
        this.callePago = callePago;
        this.numPuertaPago = numPuertaPago;
        this.aptoPago = aptoPago;
        this.ciudadPago = ciudadPago;
        this.codigoPostalDepartamentoPago = codigoPostalDepartamentoPago;
        this.grupo = grupo;
        this.subgrupo = subgrupo;
        this.observaciones = observaciones;
        this.descuento = descuento;
        this.descuentoCuota = descuentoCuota;
        this.usuario = usuario;
        this.horaGuardado = null;
    }

    public DTFormularioCabal(int id, Date fecha, String altaOModificacion, 
            String numEstablecimiento, String nombreFantasia, String razonSocial,
            String RUT, String tipoRUT, String moneda, String unificaCheques, 
            String tipoDePago, String entidadSucursal, String sucursal, String cuentaNumero, 
            String entidadPagadora, String calle, String numPuerta, String apto, String ciudad, 
            String codPostalDepartamento, String telefono, String pos, String representanteLegal,
            String ramoPpal, String personaContacto, String mail, String callePago, 
            String numPuertaPago, String aptoPago, String ciudadPago, String codigoPostalDepartamentoPago, 
            String grupo, String subgrupo, String observaciones, String descuento, 
            String descuentoCuota, String usuario, Timestamp horaGuardado) {
        this.id = id;
        this.fecha = fecha;
        this.altaOModificacion = altaOModificacion;
        this.numEstablecimiento = numEstablecimiento;
        this.nombreFantasia = nombreFantasia;
        this.razonSocial = razonSocial;
        this.RUT = RUT;
        this.tipoRUT = tipoRUT;
        this.moneda = moneda;
        this.unificaCheques = unificaCheques;
        this.tipoDePago = tipoDePago;
        this.entidadSucursal = entidadSucursal;
        this.sucursal = sucursal;
        this.cuentaNumero = cuentaNumero;
        this.entidadPagadora = entidadPagadora;
        this.calle = calle;
        this.numPuerta = numPuerta;
        this.apto = apto;
        this.ciudad = ciudad;
        this.codPostalDepartamento = codPostalDepartamento;
        this.telefono = telefono;
        this.pos = pos;
        this.representanteLegal = representanteLegal;
        this.ramoPpal = ramoPpal;
        this.personaContacto = personaContacto;
        this.mail = mail;
        this.callePago = callePago;
        this.numPuertaPago = numPuertaPago;
        this.aptoPago = aptoPago;
        this.ciudadPago = ciudadPago;
        this.codigoPostalDepartamentoPago = codigoPostalDepartamentoPago;
        this.grupo = grupo;
        this.subgrupo = subgrupo;
        this.observaciones = observaciones;
        this.descuento = descuento;
        this.descuentoCuota = descuentoCuota;
        this.usuario = usuario;
        this.horaGuardado = horaGuardado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public void setDescuentoCuota(String descuentoCuota) {
        this.descuentoCuota = descuentoCuota;
    }

    public Timestamp getHoraGuardado() {
        return horaGuardado;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescuento() {
        return descuento;
    }

    public String getDescuentoCuota() {
        return descuentoCuota;
    }

    public int getId() {
        return id;
    }
    

    public Date getFecha() {
        return fecha;
    }

    public String getCallePago() {
        return callePago;
    }

    public String getNumPuertaPago() {
        return numPuertaPago;
    }

    public String getAptoPago() {
        return aptoPago;
    }

    public String getCiudadPago() {
        return ciudadPago;
    }

    public String getCodigoPostalDepartamentoPago() {
        return codigoPostalDepartamentoPago;
    }

    public String getGrupo() {
        return grupo;
    }

    public String getSubgrupo() {
        return subgrupo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public String getTipoRUT() {
        return tipoRUT;
    }

    public String getMoneda() {
        return moneda;
    }

    public String getUnificaCheques() {
        return unificaCheques;
    }

    public String getTipoDePago() {
        return tipoDePago;
    }

    public String getEntidadSucursal() {
        return entidadSucursal;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getAltaOModificacion() {
        return altaOModificacion;
    }

    public String getNumEstablecimiento() {
        return numEstablecimiento;
    }

    public String getNombreFantasia() {
        return nombreFantasia;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getRUT() {
        return RUT;
    }

    public String getCuentaNumero() {
        return cuentaNumero;
    }

    public String getEntidadPagadora() {
        return entidadPagadora;
    }

    public String getCalle() {
        return calle;
    }

    public String getNumPuerta() {
        return numPuerta;
    }

    public String getApto() {
        return apto;
    }

    public String getCodPostalDepartamento() {
        return codPostalDepartamento;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getPos() {
        return pos;
    }

    public String getMail() {
        return mail;
    }

    public String getRamoPpal() {
        return ramoPpal;
    }

    public String getRepresentanteLegal() {
        return representanteLegal;
    }

    public String getPersonaContacto() {
        return personaContacto;
    }

    public String getSucursal() {
        return sucursal;
    }

}
