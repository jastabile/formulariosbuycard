/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datatypes;

/**
 *
 * @author Javier
 */
public class DTBancoMontevideo extends DTBanco{
    private String zona;
    private String nombreSucursal;

    public DTBancoMontevideo(String zona, String nombreSucursal, String nombre, String entidadMaster, String entidadCabal, String sucursal, String direccion) {
        super(nombre, entidadMaster, entidadCabal, sucursal, direccion);
        this.zona = zona;
        this.nombreSucursal = nombreSucursal;
    }

    public String getZona() {
        return zona;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    @Override
    public String toString() {
        return zona + " / " + this.getNombre() + " / " + this.getDireccion();
    }


    
}
