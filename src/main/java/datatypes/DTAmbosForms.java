/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datatypes;

/**
 *
 * @author Javier
 */
public class DTAmbosForms {
    private DTFormularioCabal cabal;
    private DTFormularioMaster master;

    public DTAmbosForms(DTFormularioCabal cabal, DTFormularioMaster master) {
        this.cabal = cabal;
        this.master = master;
    }

    public DTFormularioCabal getCabal() {
        return cabal;
    }

    public DTFormularioMaster getMaster() {
        return master;
    }
}
