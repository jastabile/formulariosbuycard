/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datatypes;

/**
 *
 * @author Javier
 */
public class DTBancoInterior extends DTBanco{
    private String ciudad;

    public DTBancoInterior(String ciudad, String nombre, String entidadMaster, String entidadCabal, String sucursal, String direccion) {
        super(nombre, entidadMaster, entidadCabal, sucursal, direccion);
        this.ciudad = ciudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    @Override
    public String toString() {
        return ciudad + " / " + this.getNombre() + " / " + this.getDireccion();
    }
    
}
