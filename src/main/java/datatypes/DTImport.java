/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datatypes;

import java.sql.Timestamp;

/**
 *
 * @author Javier
 */
public class DTImport {
    
    private int id;
    private String tipo;
    private String nombreFantasia;
    private String RUT;
    private String razonSocial;
    private String tipoCabal;
    private String calle;
    private String numero;
    private String apto;
    private boolean montevideo;
    private String codPostal;
    private int localidad;
    private String localidadString;
    private String telefono;
    private String celular;
    private String pos;
    private String mail;
    private String representanteLegal;
    private String documento;
    private boolean cajaAhorro;
    private boolean pesosSelected;
    private boolean dolaresSelected;
    private String pesos;
    private String dolares;
    private int banco;
    private int ramoCabal;
    private int ramoMaster;
    private int color;
    private String observaciones;
    private String estado;
    private String fecha;
    private Timestamp horaGuardado;
    private String bancoString;
    private String ramoString;
    private String comentarios1;
    private String comentarios2;
    
    /**Este tiene los String que "corresponderían" a los combos a cargar, 
     * hay que usarlo como constructor al importar, sin hora guardado*/
    public DTImport(int id, String tipo, String nombreFantasia, String RUT, 
            String razonSocial, String tipoCabal, String calle, String numero, 
            String apto, boolean montevideo, String codPostal, 
            String localidadString, String telefono, String celular, String pos, 
            String mail, String representanteLegal, String documento, 
            String pesos, String dolares, String bancoString, String ramoString, 
            int color, String comentarios1, String comentarios2,
            String observaciones, String estado, String fecha) {
        this.id = id;
        this.tipo = tipo;
        this.nombreFantasia = nombreFantasia;
        this.RUT = RUT;
        this.razonSocial = razonSocial;
        this.tipoCabal = tipoCabal;
        this.calle = calle;
        this.numero = numero;
        this.apto = apto;
        this.montevideo = montevideo;
        this.codPostal = codPostal;
        this.localidadString = localidadString;
        this.telefono = telefono;
        this.celular = celular;
        this.pos = pos;
        this.mail = mail;
        this.representanteLegal = representanteLegal;
        this.documento = documento;
        this.pesos = pesos;
        this.dolares = dolares;
        this.color = color;
        this.observaciones = observaciones;
        this.estado = estado;
        this.fecha = fecha;
        this.bancoString = bancoString;
        this.ramoString = ramoString;
        this.comentarios1 = comentarios1;
        this.comentarios2 = comentarios2;
        this.localidad = -1;
        this.banco = -1;
        this.ramoCabal = -1;
        this.ramoMaster = -1;
        this.color = -1;
    }
    
    /**Este tiene todos los atributos*/
    public DTImport(int id, String tipo, String nombreFantasia, 
            String RUT, String razonSocial, String tipoCabal, 
            String calle, String numero, String apto, boolean montevideo, 
            String codPostal, int localidad, String localidadString, 
            String telefono, String celular, String pos, String mail, 
            String representanteLegal, String documento, boolean cajaAhorro, 
            boolean pesosSelected, boolean dolaresSelected, String pesos, 
            String dolares, int banco, String bancoString, int ramoCabal, 
            int ramoMaster, String ramoString, int color, String comentarios1,
            String comentarios2, String observaciones, String estado, 
            String fecha, Timestamp horaGuardado) {
        this.id = id;
        this.tipo = tipo;
        this.nombreFantasia = nombreFantasia;
        this.RUT = RUT;
        this.razonSocial = razonSocial;
        this.tipoCabal = tipoCabal;
        this.calle = calle;
        this.numero = numero;
        this.apto = apto;
        this.montevideo = montevideo;
        this.codPostal = codPostal;
        this.localidad = localidad;
        this.localidadString = localidadString;
        this.telefono = telefono;
        this.celular = celular;
        this.pos = pos;
        this.mail = mail;
        this.representanteLegal = representanteLegal;
        this.documento = documento;
        this.cajaAhorro = cajaAhorro;
        this.pesosSelected = pesosSelected;
        this.dolaresSelected = dolaresSelected;
        this.pesos = pesos;
        this.dolares = dolares;
        this.banco = banco;
        this.ramoCabal = ramoCabal;
        this.ramoMaster = ramoMaster;
        this.color = color;
        this.observaciones = observaciones;
        this.estado = estado;
        this.fecha = fecha;
        this.horaGuardado = horaGuardado;
        this.bancoString = bancoString;
        this.ramoString = ramoString;
        this.comentarios1 = comentarios1;
        this.comentarios2 = comentarios2;
    }

    /**Este tiene todos los atributos, menos la hora*/
    public DTImport(int id, String tipo, String nombreFantasia, 
            String RUT, String razonSocial, String tipoCabal, 
            String calle, String numero, String apto, boolean montevideo, 
            String codPostal, int localidad, String localidadString, 
            String telefono, String celular, String pos, String mail, 
            String representanteLegal, String documento, boolean cajaAhorro, 
            boolean pesosSelected, boolean dolaresSelected, String pesos, 
            String dolares, int banco, String bancoString, int ramoCabal, 
            int ramoMaster, String ramoString, int color, String comentarios1,
            String comentarios2, String observaciones, String estado, 
            String fecha) {
        this.id = id;
        this.tipo = tipo;
        this.nombreFantasia = nombreFantasia;
        this.RUT = RUT;
        this.razonSocial = razonSocial;
        this.tipoCabal = tipoCabal;
        this.calle = calle;
        this.numero = numero;
        this.apto = apto;
        this.montevideo = montevideo;
        this.codPostal = codPostal;
        this.localidad = localidad;
        this.localidadString = localidadString;
        this.telefono = telefono;
        this.celular = celular;
        this.pos = pos;
        this.mail = mail;
        this.representanteLegal = representanteLegal;
        this.documento = documento;
        this.cajaAhorro = cajaAhorro;
        this.pesosSelected = pesosSelected;
        this.dolaresSelected = dolaresSelected;
        this.pesos = pesos;
        this.dolares = dolares;
        this.banco = banco;
        this.ramoCabal = ramoCabal;
        this.ramoMaster = ramoMaster;
        this.color = color;
        this.observaciones = observaciones;
        this.estado = estado;
        this.fecha = fecha;
        this.bancoString = bancoString;
        this.ramoString = ramoString;
        this.comentarios1 = comentarios1;
        this.comentarios2 = comentarios2;
    }
    
    /**Este tiene los parametros que se pueden tomar del generador*/
    public DTImport(int id, String tipo, String nombreFantasia, 
            String RUT, String razonSocial, String tipoCabal, String calle, 
            String numero, String apto, boolean montevideo, String codPostal, 
            int localidad, String telefono, String celular, String pos, 
            String mail, String representanteLegal, String documento, 
            boolean cajaAhorro, boolean isPesos, boolean isDolares, 
            String pesos, String dolares, int banco, int ramoCabal, 
            int ramoMaster, int color, String observaciones, String fecha) {
        this.id = id;
        this.tipo = tipo;
        this.nombreFantasia = nombreFantasia;
        this.RUT = RUT;
        this.razonSocial = razonSocial;
        this.tipoCabal = tipoCabal;
        this.calle = calle;
        this.numero = numero;
        this.apto = apto;
        this.montevideo = montevideo;
        this.codPostal = codPostal;
        this.localidad = localidad;
        this.telefono = telefono;
        this.celular = celular;
        this.pos = pos;
        this.mail = mail;
        this.representanteLegal = representanteLegal;
        this.documento = documento;
        this.cajaAhorro = cajaAhorro;
        this.pesosSelected = isPesos;
        this.dolaresSelected = isDolares;
        this.pesos = pesos;
        this.dolares = dolares;
        this.banco = banco;
        this.ramoCabal = ramoCabal;
        this.ramoMaster = ramoMaster;
        this.color = color;
        this.observaciones = observaciones;
        this.fecha = fecha;
    }

    public String getBancoString() {
        return bancoString;
    }

    public void setComentarios1(String comentarios1) {
        this.comentarios1 = comentarios1;
    }

    public void setComentarios2(String comentarios2) {
        this.comentarios2 = comentarios2;
    }

    public void setBancoString(String bancoString) {
        this.bancoString = bancoString;
    }

    public String getRamoString() {
        return ramoString;
    }

    public String getComentarios1() {
        return comentarios1;
    }

    public String getComentarios2() {
        return comentarios2;
    }

    public void setRamoString(String ramoString) {
        this.ramoString = ramoString;
    }
    
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getId() {
        return id;
    }

    public int getLocalidad() {
        return localidad;
    }

    public boolean isCajaAhorro() {
        return cajaAhorro;
    }

    public boolean isPesosSelected() {
        return pesosSelected;
    }

    public boolean isDolaresSelected() {
        return dolaresSelected;
    }

    public int getBanco() {
        return banco;
    }

    public String getLocalidadString() {
        return localidadString;
    }
    
    public String getEstado() {
        return estado;
    }
    
    public int getRamoCabal() {
        return ramoCabal;
    }

    public int getRamoMaster() {
        return ramoMaster;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public String getFecha() {
        return fecha;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombreFantasia() {
        return nombreFantasia;
    }

    public String getRUT() {
        return RUT;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public String getTipoCabal() {
        return tipoCabal;
    }

    public String getCalle() {
        return calle;
    }

    public String getNumero() {
        return numero;
    }

    public String getApto() {
        return apto;
    }

    public boolean isMontevideo() {
        return montevideo;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getCelular() {
        return celular;
    }

    public String getPos() {
        return pos;
    }

    public String getMail() {
        return mail;
    }

    public String getRepresentanteLegal() {
        return representanteLegal;
    }

    public String getDocumento() {
        return documento;
    }

    public String getPesos() {
        return pesos;
    }

    public String getDolares() {
        return dolares;
    }

    public Timestamp getHoraGuardado() {
        return horaGuardado;
    }

    public void setNombreFantasia(String nombreFantasia) {
        this.nombreFantasia = nombreFantasia;
    }

    public void setRUT(String RUT) {
        this.RUT = RUT;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setApto(String apto) {
        this.apto = apto;
    }

    public void setLocalidadString(String localidadString) {
        this.localidadString = localidadString;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setBanco(int banco) {
        this.banco = banco;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    
}
