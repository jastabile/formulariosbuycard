/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

public class ControladorExceptions {
    public void logErrors(String msg, Exception e){
        try{
            FileWriter fw = new FileWriter ("exception.txt", true);
//            BufferedWriter out = new BufferedWriter(fw);
//            out.write(msg + "\n");
            PrintWriter pw = new PrintWriter (fw);
            pw.println(new Date());
            e.printStackTrace(pw);
//            out.close();
            pw.close();
        }catch(IOException io){}
    }
    
    public static void logErrorsStatic(String msg, Exception e){
        try{
            FileWriter fw = new FileWriter ("exception.txt", true);
//            BufferedWriter out = new BufferedWriter(fw);
//            out.write(msg + "\n");
            PrintWriter pw = new PrintWriter (fw);
            pw.println(new Date());
            e.printStackTrace(pw);
//            out.close();
            pw.close();
        }catch(IOException io){}
    }
}
