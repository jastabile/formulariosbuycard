/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import datatypes.DTBanco;
import datatypes.DTBancoMontevideo;
import datatypes.DTBancoInterior;
import datatypes.DTCiudad;
import datatypes.DTFormularioAnda;
import datatypes.DTFormularioCabal;
import datatypes.DTFormularioMaster;
import datatypes.DTImport;
import excepciones.BDException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import logica.bancoMontevideo;
import logica.bancoInterior;
import logica.ciudad;
import logica.ramo;
import logica.ramoAnda;
import logica.ramoCabal;
import logica.ramoMaster;
import logica.workerUsuario;
import manejadores.manejador;

/**
 *
 * @author Javier
 */
public class Controlador {
    private static Controlador instance;
    private static manejador mjBancos;
    private static workerUsuario wkUser;
    
    public static Controlador getInstance() {
       if(instance == null) {
          instance = new Controlador();
       }
       return instance;
    }

    public Controlador() {
        mjBancos = manejador.getInstance();
    }
    
    public int buscarVersion() throws BDException{
        return persistencia.persistencia.buscarVersion();
    }
    
//  BANCOS MONTEVIDEO
    public void add(bancoMontevideo banco){
        mjBancos.add(banco);
    }
    
    public void remove(bancoMontevideo banco){
        mjBancos.remove(banco);
    }
    
    public ArrayList<DTBancoMontevideo> listarBancosMontevideo(){
        ArrayList<DTBancoMontevideo> listaDT = new ArrayList();
        ArrayList<bancoMontevideo> listaBancos = mjBancos.getListaBancosMontevideo();
        for (bancoMontevideo b: listaBancos){
            listaDT.add(new DTBancoMontevideo(b.getZona(), b.getNombreSucursal(), b.getNombre(), b.getEntidadMaster(), b.getEntidadCabal(), b.getSucursal(), b.getDireccion()));
        }
        return listaDT;
    }
    
//  BANCOS INTERIOR
    public void add(bancoInterior banco){
        mjBancos.add(banco);
    }
    
    public void remove(bancoInterior banco){
        mjBancos.remove(banco);
    }
    
    public ArrayList<DTBancoInterior> listarBancosInterior(){
        ArrayList<DTBancoInterior> listaDT = new ArrayList();
        ArrayList<bancoInterior> listaBancos = mjBancos.getListaBancosInterior();
        for (bancoInterior b: listaBancos){
            listaDT.add(new DTBancoInterior(b.getCiudad(), b.getNombre(), b.getEntidadMaster(), b.getEntidadCabal(), b.getSucursal(), b.getDireccion()));
        }
        return listaDT;
    }
    
    public ArrayList<DTBanco> listarBancos(){
        ArrayList<DTBanco> lista = new ArrayList();
        ArrayList<DTBancoInterior> listaInterior = listarBancosInterior();
        ArrayList<DTBancoMontevideo> listaMontevideo = listarBancosMontevideo();
        for (DTBancoMontevideo b: listaMontevideo){
            lista.add(b);
        }
        for (DTBancoInterior b: listaInterior){
            lista.add(b);
        }
        return lista;
    }
    
//  RAMOS MASTER  
    public static void add(ramoMaster ramo){
        mjBancos.add(ramo);
    }
    
    public static void remove(ramoMaster ramo){
        mjBancos.remove(ramo);
    }
    
    public ArrayList<ramoMaster> listarRamosMaster(){
        ArrayList<ramoMaster> listaRamos = mjBancos.getListaRamosMaster();
        return listaRamos;
    }
    
//  RAMOS ANDA
    public ArrayList<ramoAnda> listarRamosAnda(){
        ArrayList<ramoAnda> listaRamos = mjBancos.getListaRamosAnda();
        return listaRamos;
    }
    
    public static void add(ramoAnda ramo){
        mjBancos.add(ramo);
    }
    
    public static void remove(ramoAnda ramo){
        mjBancos.remove(ramo);
    }
    
//  RAMOS CABAL  
    public static void add(ramoCabal ramo){
        mjBancos.add(ramo);
    }
    
    public static void remove(ramoCabal ramo){
        mjBancos.remove(ramo);
    }
    
    public ArrayList<ramoCabal> listarRamosCabal(){
        ArrayList<ramoCabal> listaRamos = mjBancos.getListaRamosCabal();
        return listaRamos;
    }
    
//  CIUDADES 
    public void add(ciudad city){
        mjBancos.add(city);
    }
    
    public void remove(ciudad city){
        mjBancos.remove(city);
    }
    
    public ArrayList<DTCiudad> listarCiudades(){
        ArrayList<DTCiudad> listaDT = new ArrayList();
        ArrayList<ciudad> listaCiudades = mjBancos.getListaCiudades();
        for (ciudad c: listaCiudades){
            listaDT.add(new DTCiudad(c.getCodigoCiudad(), c.getLocalidad(), c.getCodigoPostal(), c.getCodigoDepartamento(), c.getDepartamento()));
        }
        return listaDT;
    }
    
//  PERSISTENCIA
    
    //ANDA
    public static int insertarFormCabal(DTFormularioCabal dt) throws BDException{
        return persistencia.persistencia.InsertarFormCabal(dt);
    }
    
    public static void modificarFormCabal(DTFormularioCabal dt) throws BDException{
        persistencia.persistencia.ModificarFormCabal(dt);
    }
    
    public static ArrayList<DTFormularioCabal> buscarCabal() throws BDException{
        return persistencia.persistencia.buscarCabal();
    }
    
    public static void borrarCabal(int idCabal) throws BDException{
        persistencia.persistencia.borrarCabal(idCabal);
    }
    
    
    //MASTER
    public static int insertarFormMaster(DTFormularioMaster dt) throws BDException{
        return persistencia.persistencia.InsertarFormMaster(dt);
    }
    
    public static void modificarFormMaster(DTFormularioMaster dt) throws BDException{
        persistencia.persistencia.ModificarFormMaster(dt);
    }
    
    public static ArrayList<DTFormularioMaster> buscarMaster() throws BDException{
        return persistencia.persistencia.buscarMaster();
    }
    
    public static void borrarMaster(int idMaster) throws BDException{
        persistencia.persistencia.borrarMaster(idMaster);
    }
    
    
    //ANDA
    public static ArrayList<DTFormularioAnda> buscarAnda() throws BDException{
        return persistencia.persistencia.buscarAnda();
    }
    
    public static int insertarFormAnda(DTFormularioAnda dt) throws BDException{
        return persistencia.persistencia.InsertarFormAnda(dt);
    }
    
    public static void modificarFormAnda(DTFormularioAnda dt) throws BDException{
        persistencia.persistencia.ModificarFormAnda(dt);
    }
    
    public static void borrarAnda(int idAnda) throws BDException{
        persistencia.persistencia.borrarAnda(idAnda);
    }
    
    //IMPORTS
    public static ArrayList<DTImport> buscarTodosLlamados() throws BDException{
        return persistencia.persistencia.buscarTodosLlamados();
    }
    
    public static ArrayList<DTImport> buscarTodosPedidos() throws BDException{
        return persistencia.persistencia.buscarTodosPedidos();
    }
    
    public static ArrayList<DTImport> buscarTodosLlamadosYPedidos() throws BDException{
        return persistencia.persistencia.buscarTodosLlamadosYPedidos();
    }
    
//    public static ArrayList<ArrayList<>> buscarTodosLlamadosYPedidosUnoPorFila() throws BDException{
//        return persistencia.persistencia.buscarTodosLlamadosYPedidosUnoPorFila();
//    }
    
    public static ArrayList<DTImport> buscarTodosCabal() throws BDException{
        return persistencia.persistencia.buscarTodosCabal();
    }
    
    public static ArrayList<DTImport> buscarTodosMasterYCabal() throws BDException{
        return persistencia.persistencia.buscarTodosMasterYCabal();
    }
    
    public static int insertarImport(DTImport dt) throws BDException{
        return persistencia.persistencia.InsertarImport(dt);
    }
    
    public static void modificarImport(DTImport dt) throws BDException{
        persistencia.persistencia.ModificarImport(dt);
    }

    public static void borrarTodosImports() throws BDException{
        persistencia.persistencia.borrarTodosImport();
    }
    
    public static void borrarImport(DTImport dt) throws BDException{
        persistencia.persistencia.borrarImport(dt);
    }
    
    //USUARIO
    public static String buscarUsuario(String nombrePc) throws BDException{
        try{
            wkUser = new workerUsuario("buscar", nombrePc);
            wkUser.execute();
            return wkUser.get();
        }catch (ExecutionException | InterruptedException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            System.out.println(e);
        }
        return null;
    }
    
    public static void insertarUsuario(String nombrePc, String usuario) throws BDException{
        wkUser = new workerUsuario("insertar", nombrePc, usuario);
        wkUser.execute();
    }
    
    public static void modificarUsuario(String nombrePc, String usuario) throws BDException{
        wkUser = new workerUsuario("modificar", nombrePc, usuario);
        wkUser.execute();
    }
    
    private static ArrayList<ramo> buscarRamos(String tipo) throws BDException{
        return persistencia.persistencia.buscarRamos(tipo);
    }
    
    public void cargarRamos() throws BDException{
        ArrayList<ramo> listaAnda = Controlador.buscarRamos("ANDA");
        ArrayList<ramo> listaMaster = Controlador.buscarRamos("MASTER");
        ArrayList<ramo> listaCabal = Controlador.buscarRamos("CABAL");
        
        for (ramo r: listaAnda){
            add((ramoAnda)r);
        }
        for (ramo r: listaMaster){
            add((ramoMaster)r);
        }
        for (ramo r: listaCabal){
            add((ramoCabal)r);
        }
    }
    
    public void modificarRamo(ramo r) throws BDException{
        persistencia.persistencia.modificarRamo(r);
    }
}
