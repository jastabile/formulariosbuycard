package controladores;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import datatypes.DTImport;
import excepciones.noHay12ColumnasException;
import excepciones.noHay28ColumnasException;
import excepciones.noHay9ColumnasException;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *
 * @author Javier
 */
public class ControladorExcel {
    
    private static final String MASTER = "MASTER";
    private static final String HISTORICO_MASTER = "HISTORICO_MASTER";
    private static final String CABAL = "CABAL";
    
    public static DefaultTableModel Importar(File archivo) throws IOException, InvalidFormatException {
        DefaultTableModel model = new DefaultTableModel();
        Workbook wb = WorkbookFactory.create(new FileInputStream(archivo));
        Sheet hoja = wb.getSheetAt(0);
        Iterator filaIterator = hoja.rowIterator();
        int indiceFila = -1;
        while (filaIterator.hasNext()) {
            indiceFila++;
            Row fila = (Row) filaIterator.next();
            Object[] listaColumna = new Object[100];
            Iterator columnaIterator = fila.cellIterator();
            int indiceColumna = -1;
            while (columnaIterator.hasNext()) {
                indiceColumna++;
                Cell celda = (Cell) columnaIterator.next();
                if (indiceFila == 0){
                    model.addColumn(celda.getStringCellValue());
                }else{
                    if (celda != null){
                        celda.setCellType(CellType.STRING);
                        switch(celda.getCellTypeEnum()){
                            case NUMERIC:
                                if (HSSFDateUtil.isCellDateFormatted(celda)) {
                                    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
                                    String fecha = formater.format(celda.getDateCellValue());
                                    listaColumna[indiceColumna] = fecha;
                                }else{
                                    listaColumna[indiceColumna] = celda.getNumericCellValue();
                                }
                                break;
                            case STRING:
                                listaColumna[indiceColumna] = celda.getStringCellValue();
                                break;
                            case BOOLEAN:
                                listaColumna[indiceColumna] = celda.getBooleanCellValue();
                                break;
                            default:
                                listaColumna[indiceColumna] = celda.getDateCellValue();
                                break;
                        }
                    }
                }
            }
            if (indiceFila != 0){
                model.addRow(listaColumna);
            }
        }
        return model;
    }
    
    public static ArrayList<DTImport> ImportarCabalOMaster(File archivo) throws IOException, InvalidFormatException, noHay28ColumnasException, noHay12ColumnasException, noHay9ColumnasException {
        ArrayList<DTImport> lista  = new ArrayList();
        Workbook wb = WorkbookFactory.create(new FileInputStream(archivo));
        Sheet hoja;
        Iterator filaIterator;
        int filas = 0;
        
        for (int j = 0; j < wb.getNumberOfSheets() ; j++){
            hoja = wb.getSheetAt(j);
            filaIterator = hoja.rowIterator();
            Row fila = null;
            if (filaIterator.hasNext())
                fila = (Row) filaIterator.next();            //Nunca recorro la primer fila porque es la de los titulos
            filas++;
            
            String tipo = MASTER;    
            if (fila != null && fila.getLastCellNum() == 28){
                tipo = CABAL;
            }else if (fila != null && fila.getLastCellNum() == 14){
                tipo = HISTORICO_MASTER;
            }
            while (filaIterator.hasNext()) {            //Itero en las filas
                    fila = (Row) filaIterator.next();
                    filas++;
                    String nombreFantasia = "";
                    String RUT = "";
                    String razonSocial = "";
                    String tipoCabal = "";
                    String calle = "";
                    String numero = "";
                    String apto = "";
                    boolean montevideo = false;
                    String codPostal = "";
                    String telefono = "";
                    String celular = "";
                    String pos = "";
                    String mail = "";
                    String representanteLegal = "";
                    String documento = "";
                    String pesos = "";
                    String dolares = "";
                    String observaciones = "";
                    String fecha = "";
                    String direccion = "";
                    String localidadString = "";
                    String bancoString = "";
                    String ramoString = "";
                    String comentarios1 = "";
                    String comentarios2 = "";
                    String estado = "";
                    int color = -1;
            
                if (tipo.equals(CABAL)){
                        try{
                            for (int i = 0; i < 28; i++) {
                                if (i != 14)
                                    fila.getCell(i).setCellType(CellType.STRING);
                            }
                        }catch(Exception e){
                            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                            throw new noHay28ColumnasException();
                        }

                        nombreFantasia = fila.getCell(6).getStringCellValue().trim().toUpperCase();
                        RUT = fila.getCell(8).getStringCellValue().trim().toUpperCase();
                        razonSocial = fila.getCell(7).getStringCellValue().trim().toUpperCase();
                        telefono = fila.getCell(9).getStringCellValue().trim().toUpperCase();
                        pos = fila.getCell(1).getStringCellValue().trim().toUpperCase();
                        mail = fila.getCell(27).getStringCellValue().trim().toUpperCase();
                        montevideo = fila.getCell(13).getStringCellValue().trim().toUpperCase().equals("MONTEVIDEO");
                        
                        //FECHA
                        if (fila.getCell(14).getCellTypeEnum() == CellType.STRING){
                            fecha = fila.getCell(14).getStringCellValue();
                        }else{
                            DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm");
                            fecha = df.format(fila.getCell(14).getDateCellValue());
                        }

                        //REGEXP DIRECCION
                        direccion = fila.getCell(10).getStringCellValue().trim().toUpperCase();
                        Matcher match3 = Pattern.compile("[0-9][0-9][0-9]").matcher(direccion);         //matcher para num de 3 cifras
                        Matcher match4 = Pattern.compile("[0-9][0-9][0-9][0-9]").matcher(direccion);    //matcher para num de 4 cifras 
                        Matcher matchApto = Pattern.compile("APTO.*|AP.*|APTO*|APT.*|APT*").matcher(direccion);
                        if (match4.find()){
                            calle = direccion.substring(0, match4.start()).trim();
                            numero = direccion.substring(match4.start(), match4.end()).trim();
                        }else if (match3.find()){
                            calle = direccion.substring(0, match3.start()).trim();
                            numero = direccion.substring(match3.start(), match3.end()).trim();
                        }else{
                            calle = direccion;
                        }
                        if (matchApto.find()){
                            apto = direccion.substring(matchApto.start(), matchApto.end()).trim();
                        }
                        
                }else if (tipo.equals(MASTER) || tipo.equals("LLAMADOS " + MASTER)){
                    if (hoja.getSheetName().toUpperCase().contains("LLAMADOS")){
                        try{
                            for (int i = 0; i < 12; i++) {
                                fila.getCell(i).setCellType(CellType.STRING);
                            }
                        }catch(Exception e){
                            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                            throw new noHay12ColumnasException();
                        }

                        tipo = "LLAMADOS " + MASTER;
                        nombreFantasia = fila.getCell(2).getStringCellValue().trim().toUpperCase();
                        RUT = fila.getCell(3).getStringCellValue().trim().toUpperCase();
                        telefono = fila.getCell(8).getStringCellValue().trim().toUpperCase();
                        mail = fila.getCell(9).getStringCellValue().trim().toUpperCase();
                        montevideo = fila.getCell(7).getStringCellValue().trim().toUpperCase().equals("MONTEVIDEO");
                        fecha = fila.getCell(1).getStringCellValue().trim().toUpperCase();
                        bancoString = fila.getCell(5).getStringCellValue().trim().toUpperCase();
                        localidadString = fila.getCell(7).getStringCellValue().trim().toUpperCase();ramoString = fila.getCell(4).getStringCellValue().trim().toUpperCase();
                        bancoString = fila.getCell(5).getStringCellValue().trim().toUpperCase();
                        localidadString = fila.getCell(7).getStringCellValue().trim().toUpperCase();

                        //REGEXP DIRECCION
                        direccion = fila.getCell(6).getStringCellValue().trim().toUpperCase();
                        Matcher match3 = Pattern.compile("[0-9][0-9][0-9]").matcher(direccion);         //matcher para num de 3 cifras
                        Matcher match4 = Pattern.compile("[0-9][0-9][0-9][0-9]").matcher(direccion);    //matcher para num de 4 cifras 
                        Matcher matchApto = Pattern.compile("APTO.*|AP.*|APTO*|APT.*|APT*").matcher(direccion);
                        if (match4.find()){
                            calle = direccion.substring(0, match4.start()).trim();
                            numero = direccion.substring(match4.start(), match4.end()).trim();
                        }else if (match3.find()){
                            calle = direccion.substring(0, match3.start()).trim();
                            numero = direccion.substring(match3.start(), match3.end()).trim();
                        }else{
                            calle = direccion;
                        }
                        if (matchApto.find()){
                            apto = direccion.substring(matchApto.start(), matchApto.end()).trim();
                        }

                    }else{      //PESTAÑA ALTAS
                        try{
                            for (int i = 0; i < 9; i++) {
                                if (i != 7)
                                    fila.getCell(i).setCellType(CellType.STRING);
                            }
                        }catch (Exception e){
                            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                            throw new noHay9ColumnasException();
                        }

                        tipo = MASTER;
                        nombreFantasia = fila.getCell(1).getStringCellValue().trim().toUpperCase();
                        RUT = fila.getCell(3).getStringCellValue().trim().toUpperCase();
                        razonSocial = fila.getCell(2).getStringCellValue().trim().toUpperCase();
                        telefono = fila.getCell(4).getStringCellValue().trim().toUpperCase();
                        pos = fila.getCell(0).getStringCellValue().trim().toUpperCase();
                        mail = fila.getCell(8).getStringCellValue().trim().toUpperCase();
                        localidadString = fila.getCell(6).getStringCellValue().trim().toUpperCase();
                        montevideo = true;
                        
                        if (fila.getCell(7).getCellTypeEnum() == CellType.STRING){
                            fecha = fila.getCell(7).getStringCellValue();
                        }else{
                            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                            fecha = df.format(fila.getCell(7).getDateCellValue());
                        }
                        
                        //REGEXP DIRECCION
                        direccion = fila.getCell(5).getStringCellValue().trim().toUpperCase();
                        Matcher match3 = Pattern.compile("[0-9][0-9][0-9]").matcher(direccion);         //matcher para num de 3 cifras
                        Matcher match4 = Pattern.compile("[0-9][0-9][0-9][0-9]").matcher(direccion);    //matcher para num de 4 cifras 
                        Matcher matchApto = Pattern.compile("APTO.*|AP.*|APTO*|APT.*|APT*").matcher(direccion);
                        if (match4.find()){
                            calle = direccion.substring(0, match4.start()).trim();
                            numero = direccion.substring(match4.start(), match4.end()).trim();
                        }else if (match3.find()){
                            calle = direccion.substring(0, match3.start()).trim();
                            numero = direccion.substring(match3.start(), match3.end()).trim();
                        }else{
                            calle = direccion;
                        }                    
                        if (matchApto.find()){
                            apto = direccion.substring(matchApto.start(), matchApto.end()).trim();
                        }
                    }
                }else if (tipo.equals(HISTORICO_MASTER) || tipo.equals("LLAMADOS " + HISTORICO_MASTER)){
                    if (hoja.getSheetName().toUpperCase().startsWith("LLAMADOS")){ // PESTAÑA LLAMADOS
                        for (int i = 1; i < 14; i++) {
                            if (fila.getCell(i)!= null)
                                fila.getCell(i).setCellType(CellType.STRING);
                        }
                        
                        tipo = "LLAMADOS " + HISTORICO_MASTER;
                        
                        //FECHA
                        if (fila.getCell(0) != null){
                            if (fila.getCell(0).getCellTypeEnum() == CellType.STRING){
                                fecha = fila.getCell(0).getStringCellValue();
                            }else{
                                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                                fecha = df.format(fila.getCell(0).getDateCellValue());
                            }
                        }
                        nombreFantasia = fila.getCell(2).getStringCellValue().trim().toUpperCase();
                        RUT = fila.getCell(3).getStringCellValue().trim().toUpperCase();
                        ramoString = fila.getCell(4).getStringCellValue().trim().toUpperCase();
                        bancoString =  fila.getCell(5).getStringCellValue().trim().toUpperCase();
                        localidadString = fila.getCell(7).getStringCellValue().trim().toUpperCase();
                        telefono = fila.getCell(8).getStringCellValue().trim().toUpperCase();
                        mail = fila.getCell(9).getStringCellValue().trim().toUpperCase();
                        comentarios1 = fila.getCell(10).getStringCellValue().trim().toUpperCase();
                        comentarios2 = fila.getCell(11).getStringCellValue().trim().toUpperCase();
                        observaciones = fila.getCell(12).getStringCellValue().trim().toUpperCase();                        
                        String col = fila.getCell(13).getStringCellValue();
                        if (col.equals("3"))
                            color = Color.RED.getRGB();
                        else if (col.equals("4")|| col.equals("50"))
                            color = Color.GREEN.getRGB();
                        else if (col.equals("6"))
                            color = Color.YELLOW.getRGB();
                        System.out.println(filas);

                        //REGEXP DIRECCION
                        direccion = fila.getCell(6).getStringCellValue().trim().toUpperCase();
                        Matcher match3 = Pattern.compile("[0-9][0-9][0-9]").matcher(direccion);         //matcher para num de 3 cifras
                        Matcher match4 = Pattern.compile("[0-9][0-9][0-9][0-9]").matcher(direccion);    //matcher para num de 4 cifras 
                        Matcher matchApto = Pattern.compile("APTO.*|AP.*|APTO*|APT.*|APT*").matcher(direccion);
                        if (match4.find()){
                            calle = direccion.substring(0, match4.start()).trim();
                            numero = direccion.substring(match4.start(), match4.end()).trim();
                        }else if (match3.find()){
                            calle = direccion.substring(0, match3.start()).trim();
                            numero = direccion.substring(match3.start(), match3.end()).trim();
                        }else{
                            calle = direccion;
                        }                    
                        if (matchApto.find()){
                            apto = direccion.substring(matchApto.start(), matchApto.end()).trim();
                        }
                    }else if (hoja.getSheetName().toUpperCase().startsWith("PEDIDOS")){ // PESTAÑA PEDIDOS
                        for (int i = 1; i < 14; i++) {
                            if (fila.getCell(i)!= null)
                                fila.getCell(i).setCellType(CellType.STRING);
                        }
                        tipo = HISTORICO_MASTER;
                        
                        //FECHA
                        if (fila.getCell(0) != null){
                            if (fila.getCell(0).getCellTypeEnum() == CellType.STRING){
                                fecha = fila.getCell(0).getStringCellValue();
                            }else{
                                DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm");
                                fecha = df.format(fila.getCell(0).getDateCellValue());
                            }
                        }
                        
                        pos = fila.getCell(1).getStringCellValue().trim().toUpperCase();
                        nombreFantasia = fila.getCell(2).getStringCellValue().trim().toUpperCase();
                        razonSocial = fila.getCell(3).getStringCellValue().trim().toUpperCase();
                        RUT = fila.getCell(4).getStringCellValue().trim().toUpperCase();
                        localidadString = fila.getCell(7).getStringCellValue().trim().toUpperCase();
                        telefono = fila.getCell(5).getStringCellValue().trim().toUpperCase();
                        mail = fila.getCell(9).getStringCellValue().trim().toUpperCase();
                        observaciones = fila.getCell(12).getStringCellValue().trim().toUpperCase();
                        estado = fila.getCell(13).getStringCellValue().trim().toUpperCase();
                        Double col = fila.getCell(14).getNumericCellValue();
                        if (col == 3)
                            color = Color.RED.getRGB();
                        else if (col == 4 || col == 50)
                            color = Color.GREEN.getRGB();
                        else if (col == 6)
                            color = Color.YELLOW.getRGB();

                        //REGEXP DIRECCION
                        direccion = fila.getCell(6).getStringCellValue().trim().toUpperCase();
                        Matcher match3 = Pattern.compile("[0-9][0-9][0-9]").matcher(direccion);         //matcher para num de 3 cifras
                        Matcher match4 = Pattern.compile("[0-9][0-9][0-9][0-9]").matcher(direccion);    //matcher para num de 4 cifras 
                        Matcher matchApto = Pattern.compile("APTO.*|AP.*|APTO*|APT.*|APT*").matcher(direccion);
                        if (match4.find()){
                            calle = direccion.substring(0, match4.start()).trim();
                            numero = direccion.substring(match4.start(), match4.end()).trim();
                        }else if (match3.find()){
                            calle = direccion.substring(0, match3.start()).trim();
                            numero = direccion.substring(match3.start(), match3.end()).trim();
                        }else{
                            calle = direccion;
                        }                    
                        if (matchApto.find()){
                            apto = direccion.substring(matchApto.start(), matchApto.end()).trim();
                        }
                    }
                }
                
                if (nombreFantasia == null)
                    nombreFantasia = "";
                if (RUT == null)
                    RUT = "";
                if (razonSocial == null)
                    razonSocial = "";
                if (tipoCabal == null)
                    tipoCabal = "";
                if (calle == null)
                    calle = "";
                if (numero == null)
                    numero = "";
                if (apto == null)
                    apto = "";
                if (localidadString == null)
                    localidadString = "";
                if (telefono == null)
                    telefono = "";
                if (pos == null)
                    pos = "";
                if (mail == null)
                    mail = "";
                if (bancoString == null)
                    bancoString = "";
                if (ramoString == null)
                    ramoString = "";
                if (estado == null)
                    estado = "";
                if (fecha == null)
                    fecha = "";  
                
                
                DTImport dt = new DTImport(-1, tipo, nombreFantasia, RUT, 
                    razonSocial, tipoCabal, calle, numero, apto, montevideo, 
                    codPostal, localidadString, telefono, celular, pos, mail, representanteLegal,
                    documento, pesos, dolares, bancoString, ramoString, color,
                    comentarios1, comentarios2,observaciones, estado, fecha);

                lista.add(dt);
            }
        }
        return lista;
    }
    
    private static boolean existe(String RUT, ArrayList<DTImport> lista){
        boolean existe = false;
        for(DTImport dt: lista){
            if (dt.getRUT() == RUT){
                existe = true;
                break;
            }
        }
        return existe;
    }
    
    public static void exportar(ArrayList<DTImport> lista, File archivo) throws FileNotFoundException, IOException{
        FileOutputStream out = new FileOutputStream(archivo);
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet worksheet = workbook.createSheet("Hoja");
        
        
//////////////////////////PRIMER FILA//////////////////////////
        HSSFRow row0 = worksheet.createRow((short) 0);
        HSSFCell cellA1 = row0.createCell((short) 0);
        HSSFCell cellB1 = row0.createCell((short) 1);
        HSSFCell cellC1 = row0.createCell((short) 2);
        HSSFCell cellD1 = row0.createCell((short) 3);
        HSSFCell cellE1 = row0.createCell((short) 4);
        HSSFCell cellF1 = row0.createCell((short) 5);
        HSSFCell cellG1 = row0.createCell((short) 6);
        HSSFCell cellH1 = row0.createCell((short) 7);
        HSSFCell cellI1 = row0.createCell((short) 8);
        HSSFCell cellJ1 = row0.createCell((short) 9);
        HSSFCell cellK1 = row0.createCell((short) 10);
        HSSFCell cellL1 = row0.createCell((short) 11);
        HSSFCell cellM1 = row0.createCell((short) 12);
        HSSFCell cellN1 = row0.createCell((short) 13);
        
        cellA1.setCellValue("FECHA");
        cellB1.setCellValue("RECLAMO");
        cellC1.setCellValue("NOMBRE");
        cellD1.setCellValue("RUT");
        cellE1.setCellValue("RUBRO");
        cellF1.setCellValue("BANCO");
        cellG1.setCellValue("DIRECCIÓN");
        cellH1.setCellValue("LOCALIDAD");
        cellI1.setCellValue("TELEFONO");
        cellJ1.setCellValue("EMAIL-OTROS");
        cellK1.setCellValue("");
        cellL1.setCellValue("");
        cellM1.setCellValue("COMENTARIO");
        cellN1.setCellValue("ESTADO");
        
        HSSFFont defaultFont= workbook.createFont();
        defaultFont.setFontHeightInPoints((short)10);
        defaultFont.setFontName("Arial");
        defaultFont.setColor(IndexedColors.BLACK.getIndex());
        defaultFont.setBold(true);
        defaultFont.setItalic(false);
        
        HSSFCellStyle style1 = workbook.createCellStyle();
        style1.setFillForegroundColor(HSSFColor.BLUE.index);
        style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style1.setBorderBottom(CellStyle.BORDER_THIN);
        style1.setBottomBorderColor(HSSFColor.BLACK.index);
        style1.setBorderLeft(CellStyle.BORDER_THIN);
        style1.setLeftBorderColor(HSSFColor.BLACK.index);
        style1.setBorderRight(CellStyle.BORDER_THIN);
        style1.setRightBorderColor(HSSFColor.BLACK.index);
        style1.setBorderTop(CellStyle.BORDER_THIN);
        style1.setTopBorderColor(HSSFColor.BLACK.index);
        style1.setFont(defaultFont);

        cellA1.setCellStyle(style1);
        cellB1.setCellStyle(style1);
        cellC1.setCellStyle(style1);
        cellD1.setCellStyle(style1);
        cellE1.setCellStyle(style1);
        cellF1.setCellStyle(style1);
        cellG1.setCellStyle(style1);
        cellH1.setCellStyle(style1);
        cellI1.setCellStyle(style1);
        cellJ1.setCellStyle(style1);
        cellK1.setCellStyle(style1);
        cellL1.setCellStyle(style1);
        cellM1.setCellStyle(style1);
        cellN1.setCellStyle(style1);
//////////////////////////////////////////////////////////////////////  

        HSSFCellStyle style = workbook.createCellStyle();
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(HSSFColor.BLACK.index);
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(HSSFColor.BLACK.index);
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(HSSFColor.BLACK.index);
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(HSSFColor.BLACK.index);
        
        // VERDE
        HSSFCellStyle styleVerde = workbook.createCellStyle();
        styleVerde.setFillForegroundColor(HSSFColor.BRIGHT_GREEN.index);
        styleVerde.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        styleVerde.setBorderBottom(CellStyle.BORDER_THIN);
        styleVerde.setBottomBorderColor(HSSFColor.BLACK.index);
        styleVerde.setBorderLeft(CellStyle.BORDER_THIN);
        styleVerde.setLeftBorderColor(HSSFColor.BLACK.index);
        styleVerde.setBorderRight(CellStyle.BORDER_THIN);
        styleVerde.setRightBorderColor(HSSFColor.BLACK.index);
        styleVerde.setBorderTop(CellStyle.BORDER_THIN);
        styleVerde.setTopBorderColor(HSSFColor.BLACK.index);
        
        // AMARILLO
        HSSFCellStyle styleAmarillo = workbook.createCellStyle();
        styleAmarillo.setFillForegroundColor(HSSFColor.YELLOW.index);
        styleAmarillo.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        styleAmarillo.setBorderBottom(CellStyle.BORDER_THIN);
        styleAmarillo.setBottomBorderColor(HSSFColor.BLACK.index);
        styleAmarillo.setBorderLeft(CellStyle.BORDER_THIN);
        styleAmarillo.setLeftBorderColor(HSSFColor.BLACK.index);
        styleAmarillo.setBorderRight(CellStyle.BORDER_THIN);
        styleAmarillo.setRightBorderColor(HSSFColor.BLACK.index);
        styleAmarillo.setBorderTop(CellStyle.BORDER_THIN);
        styleAmarillo.setTopBorderColor(HSSFColor.BLACK.index);
        
        // ROJO
        HSSFCellStyle styleRojo = workbook.createCellStyle();
        styleRojo.setFillForegroundColor(HSSFColor.RED.index);
        styleRojo.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        styleRojo.setBorderBottom(CellStyle.BORDER_THIN);
        styleRojo.setBottomBorderColor(HSSFColor.BLACK.index);
        styleRojo.setBorderLeft(CellStyle.BORDER_THIN);
        styleRojo.setLeftBorderColor(HSSFColor.BLACK.index);
        styleRojo.setBorderRight(CellStyle.BORDER_THIN);
        styleRojo.setRightBorderColor(HSSFColor.BLACK.index);
        styleRojo.setBorderTop(CellStyle.BORDER_THIN);
        styleRojo.setTopBorderColor(HSSFColor.BLACK.index);

        int i = 1;
        for (DTImport dt: lista){
            HSSFRow row = worksheet.createRow((short) i);
            
            HSSFCell A = row.createCell((short) 0);
            HSSFCell B = row.createCell((short) 1);
            HSSFCell C = row.createCell((short) 2);
            HSSFCell D = row.createCell((short) 3);
            HSSFCell E = row.createCell((short) 4);
            HSSFCell F = row.createCell((short) 5);
            HSSFCell G = row.createCell((short) 6);
            HSSFCell H = row.createCell((short) 7);
            HSSFCell I = row.createCell((short) 8);
            HSSFCell J = row.createCell((short) 9);
            HSSFCell K = row.createCell((short) 10);
            HSSFCell L = row.createCell((short) 11);
            HSSFCell M = row.createCell((short) 12);
            HSSFCell N = row.createCell((short) 13);
            
            String direccion = dt.getCalle() + " " + dt.getNumero();
            
            A.setCellValue(dt.getFecha());
            B.setCellValue("");
            C.setCellValue(dt.getNombreFantasia());
            D.setCellValue(dt.getRUT());
            E.setCellValue("");
            F.setCellValue("");
            G.setCellValue(direccion);
            H.setCellValue("");
            I.setCellValue(dt.getTelefono());
            J.setCellValue(dt.getMail());
            K.setCellValue("");
            L.setCellValue("");
            M.setCellValue(dt.getObservaciones());
            N.setCellValue("");
            
            int color = dt.getColor();
            if (color == Color.GREEN.getRGB()){
                A.setCellStyle(styleVerde);
                B.setCellStyle(styleVerde);
                C.setCellStyle(styleVerde);
                D.setCellStyle(styleVerde);
                E.setCellStyle(styleVerde);
                F.setCellStyle(styleVerde);
                G.setCellStyle(styleVerde);
                H.setCellStyle(styleVerde);
                I.setCellStyle(styleVerde);
                J.setCellStyle(styleVerde);
                K.setCellStyle(styleVerde);
                L.setCellStyle(styleVerde);
                M.setCellStyle(styleVerde);
                N.setCellStyle(styleVerde);
            }else if (color == Color.YELLOW.getRGB()){
                A.setCellStyle(styleAmarillo);
                B.setCellStyle(styleAmarillo);
                C.setCellStyle(styleAmarillo);
                D.setCellStyle(styleAmarillo);
                E.setCellStyle(styleAmarillo);
                F.setCellStyle(styleAmarillo);
                G.setCellStyle(styleAmarillo);
                H.setCellStyle(styleAmarillo);
                I.setCellStyle(styleAmarillo);
                J.setCellStyle(styleAmarillo);
                K.setCellStyle(styleAmarillo);
                L.setCellStyle(styleAmarillo);
                M.setCellStyle(styleAmarillo);
                N.setCellStyle(styleAmarillo);
            }else if (color == Color.RED.getRGB()){
                A.setCellStyle(styleRojo);
                B.setCellStyle(styleRojo);
                C.setCellStyle(styleRojo);
                D.setCellStyle(styleRojo);
                E.setCellStyle(styleRojo);
                F.setCellStyle(styleRojo);
                G.setCellStyle(styleRojo);
                H.setCellStyle(styleRojo);
                I.setCellStyle(styleRojo);
                J.setCellStyle(styleRojo);
                K.setCellStyle(styleRojo);
                L.setCellStyle(styleRojo);
                M.setCellStyle(styleRojo);
                N.setCellStyle(styleRojo);
            }else{
                A.setCellStyle(style);
                B.setCellStyle(style);
                C.setCellStyle(style);
                D.setCellStyle(style);
                E.setCellStyle(style);
                F.setCellStyle(style);
                G.setCellStyle(style);
                H.setCellStyle(style);
                I.setCellStyle(style);
                J.setCellStyle(style);
                K.setCellStyle(style);
                L.setCellStyle(style);
                M.setCellStyle(style);
                N.setCellStyle(style);
            }
            i++;
        }
        for (int j = 0; j < 15 ; j++)
            worksheet.autoSizeColumn(j);
        workbook.write(out);
        out.flush();
        out.close();
    }
    
}
