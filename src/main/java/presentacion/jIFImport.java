/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import components.ButtonTabComponent;
import controladores.Controlador;
import controladores.ControladorExcel;
import datatypes.DTBancoInterior;
import datatypes.DTBancoMontevideo;
import datatypes.DTCiudad;
import datatypes.DTImport;
import excepciones.BDException;
import excepciones.noSeleccionoElementoException;
import java.awt.Color;
import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import logica.ramoMaster;
import logica.workerActualizar;
import logica.workerBorrar;
import logica.workerExportar;
import logica.workerGuardar;
import logica.workerImportar;

/**
 *
 * @author Javier
 */
public class jIFImport extends javax.swing.JInternalFrame {
    
    private boolean primerCarga = true;
    private static jIFImport instance = null;
    private DefaultTableModel modelLlamados;
    private DefaultTableModel modelPedidos;
    private DefaultTableModel modelLlamadosYPedidos;
    private DefaultTableModel modelCabal;
    private DefaultTableModel modelMasterYCabal;
    private ArrayList<DTImport> listaLlamados;
    private ArrayList<DTImport> listaPedidos;
    private Map<String,ArrayList<DTImport>> mapLlamadosYPedidosUnoPorFila;
    private Map<String,ArrayList<DTImport>> mapMasterYCabalUnoPorFila;
    private ArrayList<DTImport> listaLlamadosYPedidos;
    private ArrayList<DTImport> listaLlamadosYPedidosVista;
    private ArrayList<DTImport> listaCabal;
    private ArrayList<DTImport> listaMasterYCabal;
    private ArrayList<DTImport> listaCambiosAGuardar;
    private JTabbedPane tabPane;
    private JButton botonBorrar;
    private TableRowSorter<DefaultTableModel> filterLlamados;
    private TableRowSorter<DefaultTableModel> filterPedidos;
    private TableRowSorter<DefaultTableModel> filterCabal;
    private final Color AZULSELECCION = new Color(0, 255, 215);
    private final String SEPARADOR = " ++ ";
    private final String TIPO_LLAMADOS_Y_PEDIDOS = "LLAMADOS Y PEDIDOS MASTER";
    private final String TIPO_MASTER_Y_CABAL = "MASTER Y CABAL";
    private final int SEPARADOR_SIZE = 3;
    ArrayList<String> listaBancosString;
    ArrayList<ramoMaster> listaRamosMaster;
    ArrayList<DTCiudad> listaCiudades;
    
    
    protected jIFImport(JTabbedPane tabPane, JButton botonBorrar) {
        // Exists only to defeat instantiation.
        initComponents();
        this.tabPane = tabPane;
        this.botonBorrar = botonBorrar;
        cargarPreferencias();
        preferenciasTablas();
        wkActualizarTablas(false);
    }
    
    public static jIFImport getInstance(JTabbedPane tabPane, JButton botonBorrar) {
      if(instance == null) {
         instance = new jIFImport(tabPane, botonBorrar);
      }
      return instance;
   }
    
    private void cargarRendererLlamados(){
        jTLlamados.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = super.getTableCellRendererComponent(table, value, false, hasFocus, row, column);
                try{
                    int indices[] = jTLlamados.getSelectedRows();
                    if (!isSelected){
                        int color = listaLlamadosYPedidosVista.get(jTLlamados.getRowSorter().convertRowIndexToModel(row)).getColor();
                        if (color == Color.GREEN.getRGB())
                            c.setBackground(Color.GREEN);
                        else if (color == Color.YELLOW.getRGB())
                            c.setBackground(Color.YELLOW);
                        else if (color == Color.RED.getRGB())
                            c.setBackground(Color.RED);
                        else
                            c.setBackground(Color.WHITE);
                    }else
                        c.setBackground(AZULSELECCION);
                }catch (Exception e){
                    controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                    System.out.println(e);
                }
                return c;
            }
        });
    }
    
    private void cargarRendererPedidos(){
        jTPedidos.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = super.getTableCellRendererComponent(table, value, false, hasFocus, row, column);
                try{
                    int indices[] = jTPedidos.getSelectedRows();
                    if (!isSelected){
                        int color = listaPedidos.get(jTPedidos.getRowSorter().convertRowIndexToModel(row)).getColor();
                        if (color == Color.GREEN.getRGB())
                            c.setBackground(Color.GREEN);
                        else if (color == Color.YELLOW.getRGB())
                            c.setBackground(Color.YELLOW);
                        else if (color == Color.RED.getRGB())
                            c.setBackground(Color.RED);
                        else
                            c.setBackground(Color.WHITE);
                    }else
                        c.setBackground(AZULSELECCION);
                }catch (Exception e){
                    controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                    System.out.println(e);
                }
                return c;
            }
        });
    }
    
    private void cargarRendererCabal(){
        jTCabal.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = super.getTableCellRendererComponent(table, value, false, hasFocus, row, column);
                try{
                    int indices[] = jTCabal.getSelectedRows();
                    if (!isSelected){
                        int color = listaCabal.get(jTCabal.getRowSorter().convertRowIndexToModel(row)).getColor();
                        if (color == Color.GREEN.getRGB())
                            c.setBackground(Color.GREEN);
                        else if (color == Color.YELLOW.getRGB())
                            c.setBackground(Color.YELLOW);
                        else if (color == Color.RED.getRGB())
                            c.setBackground(Color.RED);
                        else
                            c.setBackground(Color.WHITE);
                    }else
                        c.setBackground(AZULSELECCION);
                }catch (Exception e){
                    controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                    System.out.println(e);
                }
                return c;
            }
        });
    }
    
    private void preferenciasTablas(){
        preferenciasTablaCabal();
        preferenciasTablaLlamados();
        preferenciasTablaPedidos();
    }
    
    private void preferenciasTablaPedidos(){
        modelPedidos = new DefaultTableModel();

        filterPedidos = new TableRowSorter(modelPedidos);
        jTPedidos.setModel(modelPedidos);
        jTPedidos.setRowSorter(filterPedidos);
        jTPedidos.getTableHeader().setReorderingAllowed(false);

        modelPedidos.addColumn("Fecha");
        modelPedidos.addColumn("Terminal");
        modelPedidos.addColumn("Nombre Fantasía");
        modelPedidos.addColumn("Razón Social");
        modelPedidos.addColumn("RUT");
        modelPedidos.addColumn("Teléfono");
        modelPedidos.addColumn("Direccion");
        modelPedidos.addColumn("Localidad");
        modelPedidos.addColumn("Mail");
        modelPedidos.addColumn("Comentario");
        modelPedidos.addColumn("");
        modelPedidos.addColumn("");

        jTPedidos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        ///////////////////TAMAÑO COLUMNAS////////////////////////
        jTPedidos.getColumnModel().getColumn(0).setPreferredWidth(120);
//            jTable1.getColumnModel().getColumn(0).setMinWidth(130);
//            jTable1.getColumnModel().getColumn(0).setMaxWidth(130);

        jTPedidos.getColumnModel().getColumn(1).setPreferredWidth(100);
//            jTable1.getColumnModel().getColumn(1).setMinWidth(130);
//            jTable1.getColumnModel().getColumn(1).setMaxWidth(130);

        jTPedidos.getColumnModel().getColumn(2).setPreferredWidth(250);
//            jTable1.getColumnModel().getColumn(2).setMinWidth(300);
//            jTable1.getColumnModel().getColumn(2).setMaxWidth(300);

        jTPedidos.getColumnModel().getColumn(3).setPreferredWidth(250);
//            jTable1.getColumnModel().getColumn(3).setMinWidth(200);
//            jTable1.getColumnModel().getColumn(3).setMaxWidth(200);

        jTPedidos.getColumnModel().getColumn(4).setPreferredWidth(120);

        jTPedidos.getColumnModel().getColumn(5).setPreferredWidth(200);

        jTPedidos.getColumnModel().getColumn(6).setPreferredWidth(280);

        jTPedidos.getColumnModel().getColumn(7).setPreferredWidth(200);

        jTPedidos.getColumnModel().getColumn(8).setPreferredWidth(280);

        jTPedidos.getColumnModel().getColumn(9).setPreferredWidth(2000);
        //////////////////////////////////////////////////////////

        jScrollPane3.getVerticalScrollBar().setUnitIncrement(16);
        jScrollPane4.getVerticalScrollBar().setUnitIncrement(16);

        jTPedidos.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int col = jTPedidos.columnAtPoint(e.getPoint());
                if (col == 0){
                    jTPedidos.getRowSorter().setSortKeys(null);
                }
            }
        });
        cargarRendererPedidos();
        cambioCeldaEventPedidos();
    }
    
    private void preferenciasTablaLlamados(){
        modelLlamados = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                if (columnIndex == 0)
                    return false;
                return true;
            }
        };

        filterLlamados = new TableRowSorter(modelLlamados);
        jTLlamados.setModel(modelLlamados);
        jTLlamados.setRowSorter(filterLlamados);
        jTLlamados.getTableHeader().setReorderingAllowed(false);

        modelLlamados.addColumn("Tipo");
        modelLlamados.addColumn("Fecha");
        modelLlamados.addColumn("Nombre Fantasía");
        modelLlamados.addColumn("RUT");
        modelLlamados.addColumn("Rubro");
        modelLlamados.addColumn("Banco");
        modelLlamados.addColumn("Direccion");
        modelLlamados.addColumn("Localidad");
        modelLlamados.addColumn("Teléfono");
        modelLlamados.addColumn("Mail");
        modelLlamados.addColumn("");
        modelLlamados.addColumn("");
        modelLlamados.addColumn("Comentario");
        modelLlamados.addColumn("Estado");
        modelLlamados.addColumn("");

        jTLlamados.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        ///////////////////TAMAÑO COLUMNAS////////////////////////

        jTLlamados.getColumnModel().getColumn(0).setPreferredWidth(120)
                ;
        jTLlamados.getColumnModel().getColumn(2).setPreferredWidth(250);
//            jTable1.getColumnModel().getColumn(1).setMinWidth(130);
//            jTable1.getColumnModel().getColumn(1).setMaxWidth(130);

        jTLlamados.getColumnModel().getColumn(3).setPreferredWidth(120);
//            jTable1.getColumnModel().getColumn(2).setMinWidth(300);
//            jTable1.getColumnModel().getColumn(2).setMaxWidth(300);

        jTLlamados.getColumnModel().getColumn(4).setPreferredWidth(200);
//            jTable1.getColumnModel().getColumn(3).setMinWidth(200);
//            jTable1.getColumnModel().getColumn(3).setMaxWidth(200);

        jTLlamados.getColumnModel().getColumn(5).setPreferredWidth(200);

        jTLlamados.getColumnModel().getColumn(6).setPreferredWidth(200);

        jTLlamados.getColumnModel().getColumn(7).setPreferredWidth(200);

        jTLlamados.getColumnModel().getColumn(8).setPreferredWidth(200);

        jTLlamados.getColumnModel().getColumn(9).setPreferredWidth(250);

        jTLlamados.getColumnModel().getColumn(10).setPreferredWidth(220);

        jTLlamados.getColumnModel().getColumn(11).setPreferredWidth(220);
        
        jTLlamados.getColumnModel().getColumn(12).setPreferredWidth(2000);
        
        jTLlamados.getColumnModel().getColumn(13).setPreferredWidth(120);
        //////////////////////////////////////////////////////////
        
        jTLlamados.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int col = jTLlamados.columnAtPoint(e.getPoint());
                if (col == 1){
                    jTLlamados.getRowSorter().setSortKeys(null);
                }
            }
        });
        cargarRendererLlamados();
        cambioCeldaEventLlamados();

        jScrollPane1.getVerticalScrollBar().setUnitIncrement(16);
        jScrollPane2.getVerticalScrollBar().setUnitIncrement(16);
    }
    
    private void preferenciasTablaCabal(){
        modelCabal = new DefaultTableModel();
//            {
//                @Override
//                public boolean isCellEditable(int rowIndex, int columnIndex) {
//                    return false;
//                }
//            };

        filterCabal = new TableRowSorter(modelCabal);
        jTCabal.setModel(modelCabal);
        jTCabal.setRowSorter(filterCabal);
        jTCabal.getTableHeader().setReorderingAllowed(false);

        modelCabal.addColumn("Fecha");
        modelCabal.addColumn("Nombre Fantasía");
        modelCabal.addColumn("RUT");
        modelCabal.addColumn("Rubro");
        modelCabal.addColumn("Banco");
        modelCabal.addColumn("Direccion");
        modelCabal.addColumn("Localidad");
        modelCabal.addColumn("Teléfono");
        modelCabal.addColumn("Mail");
        modelCabal.addColumn("");
        modelCabal.addColumn("");
        modelCabal.addColumn("Comentario");
        modelCabal.addColumn("Estado");
        modelCabal.addColumn("");

        jTCabal.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        ///////////////////TAMAÑO COLUMNAS////////////////////////

        jTCabal.getColumnModel().getColumn(1).setPreferredWidth(250);
//            jTable1.getColumnModel().getColumn(1).setMinWidth(130);
//            jTable1.getColumnModel().getColumn(1).setMaxWidth(130);

        jTCabal.getColumnModel().getColumn(2).setPreferredWidth(120);
//            jTable1.getColumnModel().getColumn(2).setMinWidth(300);
//            jTable1.getColumnModel().getColumn(2).setMaxWidth(300);

        jTCabal.getColumnModel().getColumn(3).setPreferredWidth(200);
//            jTable1.getColumnModel().getColumn(3).setMinWidth(200);
//            jTable1.getColumnModel().getColumn(3).setMaxWidth(200);

        jTCabal.getColumnModel().getColumn(4).setPreferredWidth(200);

        jTCabal.getColumnModel().getColumn(5).setPreferredWidth(200);

        jTCabal.getColumnModel().getColumn(6).setPreferredWidth(200);

        jTCabal.getColumnModel().getColumn(7).setPreferredWidth(200);

        jTCabal.getColumnModel().getColumn(8).setPreferredWidth(250);

        jTCabal.getColumnModel().getColumn(9).setPreferredWidth(220);

        jTCabal.getColumnModel().getColumn(10).setPreferredWidth(220);
        
        jTCabal.getColumnModel().getColumn(11).setPreferredWidth(2000);
        
        jTCabal.getColumnModel().getColumn(12).setPreferredWidth(120);
        //////////////////////////////////////////////////////////
        
        jTCabal.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int col = jTCabal.columnAtPoint(e.getPoint());
                if (col == 0){
                    jTCabal.getRowSorter().setSortKeys(null);
                }
            }
        });
        cargarRendererCabal();
        cambioCeldaEventCabal();

        jScrollPane7.getVerticalScrollBar().setUnitIncrement(16);
        jScrollPane8.getVerticalScrollBar().setUnitIncrement(16);
    }
    
    private void cambioCeldaEventPedidos(){
        CellEditorListener ChangeNotification = new CellEditorListener() {
            public void editingCanceled(ChangeEvent e) {
                System.out.println("The user canceled editing.");
            }

            public void editingStopped(ChangeEvent e) {
                int row = jTPedidos.getRowSorter().convertRowIndexToModel(jTPedidos.getSelectedRow());
                int col = jTPedidos.getSelectedColumn();
                DTImport dt = listaPedidos.get(row);
                boolean hayCambios = false;
                String valor = (String)modelPedidos.getValueAt(row, col);
                switch (col){
                    case 0 : 
                        if (!dt.getFecha().equals(valor)){
                            dt.setFecha(valor);
                            hayCambios = true;
                        }
                        break;
                    case 1: 
                        if (!dt.getPos().equals(valor)){
                            dt.setPos(valor);
                            hayCambios = true;
                        }
                        break;
                    case 2: 
                        if (!dt.getNombreFantasia().equals(valor)){
                            dt.setNombreFantasia(valor);
                            hayCambios = true;
                        }
                        break;
                    case 3: 
                        if (!dt.getRazonSocial().equals(valor)){
                            dt.setRazonSocial(valor);
                            hayCambios = true;
                        }
                        break;
                    case 4: 
                        if (!dt.getRUT().equals(valor)){
                            dt.setRUT(valor);
                            hayCambios = true;
                        }
                        break;
                    case 5: 
                        if (!dt.getTelefono().equals(valor)){
                            dt.setTelefono(valor);
                            hayCambios = true;
                        }
                        break;
                    case 6: 
                        if (!dt.getCalle().equals(valor)){
                        }
                        break;
                    case 7: 
                        if (!dt.getLocalidadString().equals(valor)){
                            dt.setLocalidadString(valor);
                            hayCambios = true;
                        }
                        break;
                    case 8: 
                        if (!dt.getMail().equals(valor)){
                            dt.setMail(valor);
                            hayCambios = true;
                        }
                        break;
                    case 9: 
                        if (!dt.getObservaciones().equals(valor)){
                            dt.setObservaciones(valor);
                            hayCambios = true;
                        }
                        break;
                }
                if (hayCambios){
                    listaCambiosAGuardar.add(dt);
                    wkGuardar(null);
                }
                System.out.println(row+ " "+ col );
            }
        };

        jTPedidos.getDefaultEditor(String.class).addCellEditorListener(ChangeNotification);
    }
    
    private void cambioCeldaEventLlamados(){
        CellEditorListener ChangeNotification = new CellEditorListener() {
            public void editingCanceled(ChangeEvent e) {
                System.out.println("The user canceled editing.");
            }

            public void editingStopped(ChangeEvent e) {
                int row = jTLlamados.getRowSorter().convertRowIndexToModel(jTLlamados.getSelectedRow());
                int col = jTLlamados.getSelectedColumn();
                DTImport dt = listaLlamadosYPedidosVista.get(row);
                boolean hayCambios = false;
                String valor = (String)modelLlamados.getValueAt(row, col);
                switch (col){
                    case 1 : 
                        if (!dt.getFecha().equals(valor)){
                            dt.setFecha(valor);
                            hayCambios = true;
                        }
                        break;
                    case 2: 
                        if (!dt.getNombreFantasia().equals(valor)){
                            dt.setNombreFantasia(valor);
                            hayCambios = true;
                        }
                        break;
                    case 3: 
                        if (!dt.getRUT().equals(valor)){
                            dt.setRUT(valor);
                            hayCambios = true;
                        }
                        break;
                    case 4: 
                        if (!dt.getRamoString().equals(valor)){
                            dt.setRamoString(valor);
                            hayCambios = true;
                        }
                        break;
                    case 5: 
                        if (!dt.getBancoString().equals(valor)){
                            dt.setBancoString(valor);
                            hayCambios = true;
                        }
                        break;
                    case 6: 
                        if (!dt.getCalle().equals(valor)){
                            
                        }
                        break;
                    case 7: 
                        if (!dt.getLocalidadString().equals(valor)){
                            dt.setLocalidadString(valor);
                            hayCambios = true;
                        }
                        break;
                    case 8: 
                        if (!dt.getTelefono().equals(valor)){
                            dt.setTelefono(valor);
                            hayCambios = true;
                        }
                        break;
                    case 9: 
                        if (!dt.getMail().equals(valor)){
                            dt.setMail(valor);
                            hayCambios = true;
                        }
                        break;
                    case 10: 
                        if (!dt.getComentarios1().equals(valor)){
                            dt.setComentarios1(valor);
                            hayCambios = true;
                        }
                        break;
                    case 11: 
                        if (!dt.getComentarios2().equals(valor)){
                            dt.setComentarios2(valor);
                            hayCambios = true;
                        }
                        break;
                    case 12: 
                        if (!dt.getObservaciones().equals(valor)){
                            dt.setObservaciones(valor);
                            hayCambios = true;
                        }
                        break;
                    case 13: 
                        if (!dt.getEstado().equals(valor)){
                            dt.setEstado(valor);
                            hayCambios = true;
                        }
                        break;
                }
                if (hayCambios){
                    listaCambiosAGuardar.add(dt);
                    wkGuardar(null);
                }
                System.out.println(row+ " "+ col );
            }
        };

        jTLlamados.getDefaultEditor(String.class).addCellEditorListener(ChangeNotification);
    }
    
    private void cambioCeldaEventCabal(){
        CellEditorListener ChangeNotification = new CellEditorListener() {
            public void editingCanceled(ChangeEvent e) {
                System.out.println("The user canceled editing.");
            }

            public void editingStopped(ChangeEvent e) {
                int row = jTCabal.getRowSorter().convertRowIndexToModel(jTCabal.getSelectedRow());
                int col = jTCabal.getSelectedColumn();
                DTImport dt = listaCabal.get(row);
                boolean hayCambios = false;
                String valor = (String)modelCabal.getValueAt(row, col);
                switch (col){
                    case 0 : 
                        if (!dt.getFecha().equals(valor)){
                            dt.setFecha(valor);
                            hayCambios = true;
                        }
                        break;
                    case 1: 
                        if (!dt.getNombreFantasia().equals(valor)){
                            dt.setNombreFantasia(valor);
                            hayCambios = true;
                        }
                        break;
                    case 2: 
                        if (!dt.getRUT().equals(valor)){
                            dt.setRUT(valor);
                            hayCambios = true;
                        }
                        break;
                    case 3: 
                        if (!dt.getRamoString().equals(valor)){
                            dt.setRamoString(valor);
                            hayCambios = true;
                        }
                        break;
                    case 4: 
                        if (!dt.getBancoString().equals(valor)){
                            dt.setBancoString(valor);
                            hayCambios = true;
                        }
                        break;
                    case 5: 
                        if (!dt.getCalle().equals(valor)){
                            
                        }
                        break;
                    case 6: 
                        if (!dt.getLocalidadString().equals(valor)){
                            dt.setLocalidadString(valor);
                            hayCambios = true;
                        }
                        break;
                    case 7: 
                        if (!dt.getTelefono().equals(valor)){
                            dt.setTelefono(valor);
                            hayCambios = true;
                        }
                        break;
                    case 8: 
                        if (!dt.getMail().equals(valor)){
                            dt.setMail(valor);
                            hayCambios = true;
                        }
                        break;
                    case 9: 
                        if (!dt.getComentarios1().equals(valor)){
                            dt.setComentarios1(valor);
                            hayCambios = true;
                        }
                        break;
                    case 10: 
                        if (!dt.getComentarios2().equals(valor)){
                            dt.setComentarios2(valor);
                            hayCambios = true;
                        }
                        break;
                    case 11: 
                        if (!dt.getObservaciones().equals(valor)){
                            dt.setObservaciones(valor);
                            hayCambios = true;
                        }
                        break;
                    case 12: 
                        if (!dt.getEstado().equals(valor)){
                            dt.setEstado(valor);
                            hayCambios = true;
                        }
                        break;
                }
                if (hayCambios){
                    listaCambiosAGuardar.add(dt);
                    wkGuardar(null);
                }
                System.out.println(row+ " "+ col );
            }
        };

        jTCabal.getDefaultEditor(String.class).addCellEditorListener(ChangeNotification);
    }
    
    public void wkActualizarTablas(boolean importando){
        jDCargando jdc = new jDCargando(null, true);
        workerActualizar wk = new workerActualizar(jdc, this, importando);
        wk.execute();
        jdc.setVisible(true);
    }
    
    public void actualizarTablas(boolean importando){
        try{
            Controlador ctrl = Controlador.getInstance();
            if (jPLlamados.isShowing() || primerCarga || importando){
                listaLlamados.clear();
                listaLlamadosYPedidos.clear();
                mapLlamadosYPedidosUnoPorFila.clear();
                mapMasterYCabalUnoPorFila.clear();
                listaLlamadosYPedidosVista.clear();
                listaMasterYCabal.clear();
                listaLlamados = ctrl.buscarTodosLlamados();
                listaLlamadosYPedidos = ctrl.buscarTodosLlamadosYPedidos();
                listaMasterYCabal = ctrl.buscarTodosMasterYCabal();
                mapLlamadosYPedidosUnoPorFila = agruparPorRutLlamadosYPedidos();
                mapMasterYCabalUnoPorFila = agruparPorRutMasterYCabal();
            }
            if (jPPedidos.isShowing() || importando){
                listaPedidos.clear();
                listaPedidos = ctrl.buscarTodosPedidos();
            }
            if (jPCabal.isShowing() || importando){
                listaCabal.clear();
                listaCabal = ctrl.buscarTodosCabal();
            }
            recargarTablas(importando);
//            autoResizeColumnWidth(jTPedidos);
//            autoResizeColumnWidth(jTLlamados);
//            autoResizeColumnWidth(jTLlamadosYPedidos);
        }catch(BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(null, e.toString());
        }
    }
    
    private Map<String,ArrayList<DTImport>> agruparPorRutLlamadosYPedidos(){
        Map<String,ArrayList<DTImport>> map = new LinkedHashMap();
        for (DTImport dt: listaLlamadosYPedidos){
            if (map.containsKey(dt.getRUT()))
                map.get(dt.getRUT()).add(dt);
            else{
                ArrayList<DTImport> nueva = new ArrayList();
                nueva.add(dt);
                map.put(dt.getRUT(), nueva);
            }
        }
        return map;
    }
    
    private Map<String,ArrayList<DTImport>> agruparPorRutMasterYCabal(){
        Map<String,ArrayList<DTImport>> map = new LinkedHashMap();
        for (DTImport dt: listaMasterYCabal){
            if (map.containsKey(dt.getRUT()))
                map.get(dt.getRUT()).add(dt);
            else{
                ArrayList<DTImport> nueva = new ArrayList();
                nueva.add(dt);
                map.put(dt.getRUT(), nueva);
            }
        }
        return map;
    }
    
    private void insertInOrderById(ArrayList<DTImport> lista, DTImport dt){
        boolean inserte = false;
        for (int i = 0; i < lista.size(); i++){
            if (lista.get(i).getId() < dt.getId()){
                lista.add(i, dt);
                inserte = true;
                break;
            }
        }
        if (!inserte)
            lista.add(dt);
    }
    
    private void autoResizeColumnWidth(JTable table) {
        final TableColumnModel columnModel = table.getColumnModel();
        int column;
        for (column = 0; column < table.getColumnCount() - 1; column++) {
            int width = 15; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width +1 , width) + 2;
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
        //ULTIMA COLUMNA
        columnModel.getColumn(column).sizeWidthToFit();
    }
    
    private void recargarTablaPedidos(){
        try{
            modelPedidos.setRowCount(0);
            for (DTImport dt : listaPedidos){
                try{
                    String direccion = dt.getCalle() + " " + dt.getNumero();
                    if (!dt.getApto().equals(""))
                        direccion = direccion + " Apto. " + dt.getApto();
                    String localidad = "*****";
                    if (dt.getLocalidad() != -1)
                        localidad = listaCiudades.get(dt.getLocalidad()).toString();
                    else if (!dt.getCodPostal().equals(""))
                        localidad = "Montevideo";
                    else if (!dt.getLocalidadString().equals(""))
                        localidad = dt.getLocalidadString();
                    Object[] fila = {dt.getFecha(), dt.getPos() ,dt.getNombreFantasia(),
                        dt.getRazonSocial(), dt.getRUT(), dt.getTelefono(),
                        direccion, localidad,dt.getMail(),dt.getObservaciones()};
                    modelPedidos.addRow(fila);
                }catch(IndexOutOfBoundsException e){
                    controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                    JOptionPane.showMessageDialog(null, e.toString());
                }
            }
//            autoResizeColumnWidth(jTable1);
        }catch(HeadlessException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(this, e.toString());
        }
    }
    
    private void mezclaDeTodosMismoRut(){
        //                String[] fila = {"", "", 
//                    "", "",
//                    "", "", 
//                    "", "", 
//                    "", "", "", 
//                    "", "",
//                    "",""};
//                
//                String direccion = dt.getCalle() + " " + dt.getNumero();
//                String banco = "*****";
//                String rubro = "*****";
//                if (dt.getBanco() != -1)
//                    banco = listaBancosString.get(dt.getBanco());
//                else if (!dt.getBancoString().equals(""))
//                    banco = dt.getBancoString();
//                if (dt.getRamoMaster()!= -1)
//                    rubro = listaRamosMaster.get(dt.getRamoMaster()).toString();
//                else if (!dt.getRamoString().equals(""))
//                    rubro = dt.getRamoString();
//                if (!dt.getApto().equals(""))
//                    direccion = direccion + " Apto. " + dt.getApto();
//                String localidad = "*****";
//                if (dt.getLocalidad() != -1)
//                    localidad = listaCiudades.get(dt.getLocalidad()).toString();
//                else if (!dt.getCodPostal().equals(""))
//                    localidad = "Montevideo";
//                else if (!dt.getLocalidadString().equals(""))
//                    localidad = dt.getLocalidadString();
//                
//                String fecha = dt.getFecha();
//                String terminal = dt.getPos();
//                String nombreFantasia = dt.getNombreFantasia();
//                String razonSocial = dt.getRazonSocial();
//                String RUT = dt.getRUT();
//                String telefono = dt.getTelefono();
//                String mail = dt.getMail();
//                String estado = dt.getEstado();
//                
//                fila[0] = fecha;
//                fila[1] = terminal;
//                fila[2] = nombreFantasia;
//                fila[3] = razonSocial;
//                fila[4] = RUT;
//                fila[5] = rubro;
//                fila[6] = banco;
//                fila[7] = direccion;
//                fila[8] = direccion;
//                fila[9] = localidad;
//                fila[10] = telefono;
//                fila[11] = mail;
//                fila[14] = estado;
//                for ( dt2: listaInterna){
//                    try{
//                        String comentarios1 = dt2.getComentarios1();
//                        String comentarios2 = dt2.getComentarios2();
//                        String observaciones = dt2.getObservaciones();
//                        
////                        agregarSiNoEsta(fila, 1, terminal);
////                        agregarSiNoEsta(fila, 2, nombreFantasia);
////                        agregarSiNoEsta(fila, 3, razonSocial);
////                        agregarSiNoEsta(fila, 5, rubro);
////                        agregarSiNoEsta(fila, 6, banco);
////                        agregarSiNoEsta(fila, 7, direccion);
////                        agregarSiNoEsta(fila, 8, localidad);
////                        agregarSiNoEsta(fila, 9, telefono);
////                        agregarSiNoEsta(fila, 10, mail);
//                        agregarSiNoEsta(fila, 11, comentarios1);
//                        agregarSiNoEsta(fila, 12, comentarios2);
//                        agregarSiNoEsta(fila, 13, observaciones);
////                        agregarSiNoEsta(fila, 14, estado);
//                        
//                    }catch(IndexOutOfBoundsException e){
//                        controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
//                        JOptionPane.showMessageDialog(null, e.toString());
//                    }
//                }
//                fila = formatFila(fila);
    }
    
    private void recargarTablaLlamados(boolean base){
        try{
            modelLlamados.setRowCount(0);
            if (base){
                for (Map.Entry<String, ArrayList<DTImport>> entry : mapLlamadosYPedidosUnoPorFila.entrySet()){
                    ArrayList<DTImport> listaInterna = entry.getValue();
                    DTImport dt = getOrCreateTipoLlamadosYPedidos(listaInterna);
                    insertInOrderById(listaLlamadosYPedidosVista, dt);

                }
                for (Map.Entry<String, ArrayList<DTImport>> entry : mapMasterYCabalUnoPorFila.entrySet()){
                    ArrayList<DTImport> listaInterna = entry.getValue();
                    DTImport dt = getOrCreateTipoMasterYCabal(listaInterna);
                    insertInOrderById(listaLlamadosYPedidosVista, dt);
                }
                for (DTImport dt : listaLlamados){
                    insertInOrderById(listaLlamadosYPedidosVista, dt);
                }
            }
            for (DTImport dt: listaLlamadosYPedidosVista){
                System.out.println(dt.getId());
                try{
                    String direccion = dt.getCalle() + " " + dt.getNumero();
                    String banco = "*****";
                    String rubro = "*****";
                    if (dt.getBanco() != -1)
                        banco = listaBancosString.get(dt.getBanco());
                    else if (!dt.getBancoString().equals(""))
                        banco = dt.getBancoString();
                    if (dt.getRamoMaster()!= -1)
                        rubro = listaRamosMaster.get(dt.getRamoMaster()).toString();
                    else if (!dt.getRamoString().equals(""))
                        rubro = dt.getRamoString();
                    if (!dt.getApto().equals(""))
                        direccion = direccion + " Apto. " + dt.getApto();
                    String localidad = "*****";
                    if (dt.getLocalidad() != -1)
                        localidad = listaCiudades.get(dt.getLocalidad()).toString();
                    else if (!dt.getCodPostal().equals(""))
                        localidad = "Montevideo";
                    else if (!dt.getLocalidadString().equals(""))
                        localidad = dt.getLocalidadString();
                    String tipo = "LLAMADO";
                    if (dt.getTipo().equals(TIPO_LLAMADOS_Y_PEDIDOS))
                        tipo = "LLAMADO Y PEDIDO";
                    else if (dt.getTipo().equals(TIPO_MASTER_Y_CABAL))
                        tipo = "MASTER Y CABAL";
                    
                    Object[] fila = {tipo, dt.getFecha().trim(), dt.getNombreFantasia().trim(),
                        dt.getRUT().trim(), rubro.trim(), banco.trim(), direccion.trim(), 
                        localidad.trim(), dt.getTelefono().trim(), dt.getMail().trim(), 
                        dt.getComentarios1().trim(), dt.getComentarios2().trim(),
                        dt.getObservaciones().trim(),dt.getEstado().trim()};
                    
                    modelLlamados.addRow(fila);
                }catch(IndexOutOfBoundsException e){
                    controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                    JOptionPane.showMessageDialog(null, e.toString());
                }
            }
            guardarCambios();
//            autoResizeColumnWidth(jTable1);
        }catch(HeadlessException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(this, e.toString());
        }catch(Exception e){
            throw e;
        }
    }
    
    private DTImport getOrCreateTipoLlamadosYPedidos(ArrayList<DTImport> lista){
        int i = containsTipoLlamadosYPedidos(lista);
        if (i != -1){
            return lista.get(i);
        }else{
            int idTemp = lista.get(0).getId();
            DTImport dt = lista.get(indiceUltimoLlamado(lista));
            dt.setId(idTemp);
            dt.setTipo(TIPO_LLAMADOS_Y_PEDIDOS);
            
            String comentarios1 = "";
            String comentarios2 = "";
            String observaciones = "";
            for (DTImport dt2: lista){
                comentarios1 = concatComentario(dt2.getComentarios1(), comentarios1);
                comentarios2 = concatComentario(dt2.getComentarios2(), comentarios2);
                observaciones = concatComentario(dt2.getObservaciones(), observaciones);
            }
            dt.setComentarios1(comentarios1);
            dt.setComentarios2(comentarios2);
            dt.setObservaciones(observaciones);
            listaCambiosAGuardar.add(dt);
            return dt;
        }
    }
    
    private DTImport getOrCreateTipoMasterYCabal(ArrayList<DTImport> lista){
        int i = containsTipoMasterYCabal(lista);
        if (i != -1){
            return lista.get(i);
        }else{
            int idTemp = lista.get(0).getId();
            DTImport dt = lista.get(indiceUltimoLlamado(lista));
            dt.setId(idTemp);
            dt.setTipo(TIPO_MASTER_Y_CABAL);
            
            String comentarios1 = "";
            String comentarios2 = "";
            String observaciones = "";
            for (DTImport dt2: lista){
                comentarios1 = concatComentario(dt2.getComentarios1(), comentarios1);
                comentarios2 = concatComentario(dt2.getComentarios2(), comentarios2);
                observaciones = concatComentario(dt2.getObservaciones(), observaciones);
            }
            dt.setComentarios1(comentarios1);
            dt.setComentarios2(comentarios2);
            dt.setObservaciones(observaciones);
            listaCambiosAGuardar.add(dt);
            return dt;
        }
    }
    
    private int containsTipoLlamadosYPedidos(ArrayList<DTImport> lista){
        int i = -1;
        for (DTImport dt: lista){
            i++;
            if (dt.getTipo().equals(TIPO_LLAMADOS_Y_PEDIDOS))
                return i;
        }
        return -1;
    }
    
    private int containsTipoMasterYCabal(ArrayList<DTImport> lista){
        int i = -1;
        for (DTImport dt: lista){
            i++;
            if (dt.getTipo().equals(TIPO_MASTER_Y_CABAL))
                return i;
        }
        return -1;
    }
    
    private String concatComentario (String a, String b){
        if (!a.equals("") && !b.equals(""))
            a = a + SEPARADOR + b; 
        else if (!b.equals("")){
            a = b;
        }
        return a;
    }
            
    private String[] formatFila(String[] fila){
        if (fila[1].endsWith(SEPARADOR))
            fila[1] = fila[1].substring(0,fila[1].length() - SEPARADOR_SIZE);
        if (fila[2].endsWith(SEPARADOR))
            fila[2] = fila[2].substring(0,fila[2].length() - SEPARADOR_SIZE);
        if (fila[3].endsWith(SEPARADOR))
            fila[3] = fila[3].substring(0,fila[3].length() - SEPARADOR_SIZE);
        if (fila[4].endsWith(SEPARADOR))
            fila[4] = fila[4].substring(0,fila[4].length() - SEPARADOR_SIZE);
        if (fila[5].endsWith(SEPARADOR))
            fila[5] = fila[5].substring(0,fila[5].length() - SEPARADOR_SIZE);
        if (fila[6].endsWith(SEPARADOR))
            fila[6] = fila[6].substring(0,fila[6].length() - SEPARADOR_SIZE);
        if (fila[7].endsWith(SEPARADOR))
            fila[7] = fila[7].substring(0,fila[7].length() - SEPARADOR_SIZE);
        if (fila[8].endsWith(SEPARADOR))
            fila[8] = fila[8].substring(0,fila[8].length() - SEPARADOR_SIZE);
        if (fila[9].endsWith(SEPARADOR))
            fila[9] = fila[9].substring(0,fila[9].length() - SEPARADOR_SIZE);
        if (fila[10].endsWith(SEPARADOR))
            fila[10] = fila[10].substring(0,fila[10].length() - SEPARADOR_SIZE);
        if (fila[11].endsWith(SEPARADOR))
            fila[11] = fila[11].substring(0,fila[11].length() - SEPARADOR_SIZE);
        if (fila[12].endsWith(SEPARADOR))
            fila[12] = fila[12].substring(0,fila[12].length() - SEPARADOR_SIZE);
        if (fila[13].endsWith(SEPARADOR))
            fila[13] = fila[13].substring(0,fila[13].length() - SEPARADOR_SIZE);
        if (fila[14].endsWith(SEPARADOR))
            fila[14] = fila[14].substring(0,fila[14].length() - SEPARADOR_SIZE);
        return fila;
    }
    
    private void recargarTablaCabal(){
        try{
            modelCabal.setRowCount(0);
            for (DTImport dt : listaCabal){
                try{
                    String direccion = dt.getCalle() + " " + dt.getNumero();
                    String banco = "*****";
                    String rubro = "*****";
                    if (dt.getBanco() != -1)
                        banco = listaBancosString.get(dt.getBanco());
                    else if (!dt.getBancoString().equals(""))
                        banco = dt.getBancoString();
                    if (dt.getRamoMaster()!= -1)
                        rubro = listaRamosMaster.get(dt.getRamoMaster()).toString();
                    else if (!dt.getRamoString().equals(""))
                        rubro = dt.getRamoString();
                    if (!dt.getApto().equals(""))
                        direccion = direccion + " Apto. " + dt.getApto();
                    String localidad = "*****";
                    if (dt.getLocalidad() != -1)
                        localidad = listaCiudades.get(dt.getLocalidad()).toString();
                    else if (!dt.getCodPostal().equals(""))
                        localidad = "Montevideo";
                    else if (!dt.getLocalidadString().equals(""))
                        localidad = dt.getLocalidadString();
                    Object[] fila = {dt.getFecha().trim(), dt.getNombreFantasia().trim(),
                        dt.getRUT().trim(), rubro.trim(), banco.trim(), direccion.trim(), 
                        localidad.trim(), dt.getTelefono().trim(), dt.getMail().trim(),
                        dt.getComentarios1().trim(), dt.getComentarios2().trim(),
                        dt.getObservaciones().trim(),dt.getEstado().trim()};
                    modelCabal.addRow(fila);
                }catch(IndexOutOfBoundsException e){
                    controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                    JOptionPane.showMessageDialog(null, e.toString());
                }
            }
//            autoResizeColumnWidth(jTable1);
        }catch(HeadlessException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(this, e.toString());
        }
    }
    
    private void recargarTablaMasterYCabal(){
        try{
            modelMasterYCabal.setRowCount(0);
            for (DTImport dt : listaMasterYCabal){
                try{
                    String direccion = dt.getCalle() + " " + dt.getNumero();
                    String banco = "*****";
                    String rubro = "*****";
                    if (dt.getBanco() != -1)
                        banco = listaBancosString.get(dt.getBanco());
                    else if (!dt.getBancoString().equals(""))
                        banco = dt.getBancoString();
                    if (dt.getRamoMaster()!= -1)
                        rubro = listaRamosMaster.get(dt.getRamoMaster()).toString();
                    else if (!dt.getRamoString().equals(""))
                        rubro = dt.getRamoString();
                    if (!dt.getApto().equals(""))
                        direccion = direccion + " Apto. " + dt.getApto();
                    String localidad = "*****";
                    if (dt.getLocalidad() != -1)
                        localidad = listaCiudades.get(dt.getLocalidad()).toString();
                    else if (!dt.getCodPostal().equals(""))
                        localidad = "Montevideo";
                    else if (!dt.getLocalidadString().equals(""))
                        localidad = dt.getLocalidadString();
                    Object[] fila = {dt.getFecha().trim(), dt.getNombreFantasia().trim(),
                        dt.getRUT().trim(), rubro.trim(), banco.trim(), direccion.trim(), 
                        localidad.trim(), dt.getTelefono().trim(), dt.getMail().trim(),
                        dt.getObservaciones().trim(),dt.getEstado().trim()};
                    modelMasterYCabal.addRow(fila);
                }catch(IndexOutOfBoundsException e){
                    controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                    JOptionPane.showMessageDialog(null, e.toString());
                }
            }
//            autoResizeColumnWidth(jTable1);
        }catch(HeadlessException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(this, e.toString());
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="recargarTablaLlamadosYPedidos">
//    private void recargarTablaLlamadosYPedidos(){
//        try{
//            modelLlamadosYPedidos.setRowCount(0);
//            for ( dt: listaLlamadosYPedidos){
//                String direccion = dt.getCalle() + " " + dt.getNumero();
//                String banco = "*****";
//                String rubro = "*****";
//                if (dt.getBanco() != -1)
//                    banco = listaBancosString.get(dt.getBanco());
//                else if (!dt.getBancoString().equals(""))
//                    banco = dt.getBancoString();
//                if (dt.getRamoMaster()!= -1)
//                    rubro = listaRamosMaster.get(dt.getRamoMaster()).toString();
//                else if (!dt.getRamoString().equals(""))
//                    rubro = dt.getRamoString();
//                if (!dt.getApto().equals(""))
//                    direccion = direccion + " Apto. " + dt.getApto();
//                String localidad = "*****";
//                if (dt.getLocalidad() != -1)
//                    localidad = listaCiudades.get(dt.getLocalidad()).toString();
//                else if (!dt.getCodPostal().equals(""))
//                    localidad = "Montevideo";
//                else if (!dt.getLocalidadString().equals(""))
//                    localidad = dt.getLocalidadString();
//                String nombreFantasia = dt.getNombreFantasia();
//                String terminal = dt.getPos();
//                String razonSocial = dt.getRazonSocial();
//                String telefono = dt.getTelefono();
//                String mail = dt.getMail();
//                String comentarios1 = dt.getComentarios1();
//                String comentarios2 = dt.getComentarios2();
//                String observaciones = dt.getObservaciones();
//                String estado = dt.getEstado();
//
//                String[] fila = {dt.getFecha(), terminal, 
//                    nombreFantasia, razonSocial,
//                    dt.getRUT(), rubro, banco, direccion, 
//                    localidad, telefono, mail, comentarios1, 
//                    comentarios2,observaciones,estado};
//                modelLlamadosYPedidos.addRow(fila);
//            }
////            autoResizeColumnWidth(jTable1);
//        }catch(HeadlessException e){
//            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
//            JOptionPane.showMessageDialog(this, e.toString());
//        }
//    }
    //</editor-fold>    
    
    private void recargarTablaLlamadosYPedidosUnoPorFila(){
        try{
            modelLlamadosYPedidos.setRowCount(0);
            for (Map.Entry<String, ArrayList<DTImport>> entry : mapLlamadosYPedidosUnoPorFila.entrySet()){
                ArrayList<DTImport> listaInterna = entry.getValue();
                int i = indiceUltimoLlamado(listaInterna);
                DTImport dt = listaInterna.get(i);
                
                String[] fila = {"", "", 
                    "", "",
                    "", "", 
                    "", "", 
                    "", "", "", 
                    "", "",
                    "",""};
                
                String direccion = dt.getCalle() + " " + dt.getNumero();
                String banco = "*****";
                String rubro = "*****";
                if (dt.getBanco() != -1)
                    banco = listaBancosString.get(dt.getBanco());
                else if (!dt.getBancoString().equals(""))
                    banco = dt.getBancoString();
                if (dt.getRamoMaster()!= -1)
                    rubro = listaRamosMaster.get(dt.getRamoMaster()).toString();
                else if (!dt.getRamoString().equals(""))
                    rubro = dt.getRamoString();
                if (!dt.getApto().equals(""))
                    direccion = direccion + " Apto. " + dt.getApto();
                String localidad = "*****";
                if (dt.getLocalidad() != -1)
                    localidad = listaCiudades.get(dt.getLocalidad()).toString();
                else if (!dt.getCodPostal().equals(""))
                    localidad = "Montevideo";
                else if (!dt.getLocalidadString().equals(""))
                    localidad = dt.getLocalidadString();
                
                String fecha = dt.getFecha();
                String terminal = dt.getPos();
                String nombreFantasia = dt.getNombreFantasia();
                String razonSocial = dt.getRazonSocial();
                String RUT = dt.getRUT();
                String telefono = dt.getTelefono();
                String mail = dt.getMail();
                String estado = dt.getEstado();
                
                fila[0] = fecha;
                fila[1] = terminal;
                fila[2] = nombreFantasia;
                fila[3] = razonSocial;
                fila[4] = RUT;
                fila[5] = rubro;
                fila[6] = banco;
                fila[7] = direccion;
                fila[8] = direccion;
                fila[9] = localidad;
                fila[10] = telefono;
                fila[11] = mail;
                fila[14] = estado;
                for (DTImport dt2: listaInterna){
                    try{
                        String comentarios1 = dt2.getComentarios1();
                        String comentarios2 = dt2.getComentarios2();
                        String observaciones = dt2.getObservaciones();
                        
//                        agregarSiNoEsta(fila, 1, terminal);
//                        agregarSiNoEsta(fila, 2, nombreFantasia);
//                        agregarSiNoEsta(fila, 3, razonSocial);
//                        agregarSiNoEsta(fila, 5, rubro);
//                        agregarSiNoEsta(fila, 6, banco);
//                        agregarSiNoEsta(fila, 7, direccion);
//                        agregarSiNoEsta(fila, 8, localidad);
//                        agregarSiNoEsta(fila, 9, telefono);
//                        agregarSiNoEsta(fila, 10, mail);
                        agregarSiNoEsta(fila, 11, comentarios1);
                        agregarSiNoEsta(fila, 12, comentarios2);
                        agregarSiNoEsta(fila, 13, observaciones);
//                        agregarSiNoEsta(fila, 14, estado);
                        
                    }catch(IndexOutOfBoundsException e){
                        controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                        JOptionPane.showMessageDialog(null, e.toString());
                    }
                }
                fila = formatFila(fila);
                modelLlamadosYPedidos.addRow(fila);
            }
//            autoResizeColumnWidth(jTable1);
        }catch(HeadlessException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(this, e.toString());
        }
    }
    
    private int indiceUltimoLlamado(ArrayList<DTImport> lista){
        int i = 0;
        for (int j = 0; j < lista.size(); j++){
            if (lista.get(j).getTipo().toUpperCase().startsWith("LLAMADOS")){
                i = j;
                break;
            }
        }
        return i;
    }
    
    private void agregarSiNoEsta(String[] fila, int row, String elem){
        if (!fila[row].contains(elem) && !elem.equals("*****"))
            fila[row] = fila[row] + elem + SEPARADOR;
    }
    
    public void export(){
        try{
            String userDir = System.getProperty("user.home");
            JFileChooser chooser = new JFileChooser(userDir +"/Desktop");
            if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION){
                String path = chooser.getSelectedFile().getAbsolutePath();
                if (path.endsWith(".xlsx"))
                    path = path.substring(0, path.length() - 1) ;
                else if (!path.endsWith(".xls"))
                    path = path + ".xls";
                File archivo = new File(path);
                if (jPLlamados.isShowing())
                    ControladorExcel.exportar(listaLlamadosYPedidosVista, archivo);
                else if (jPPedidos.isShowing())
                    ControladorExcel.exportar(listaPedidos, archivo);
                else if (jPCabal.isShowing())
                    ControladorExcel.exportar(listaCabal, archivo);
            }
        }catch (IOException | HeadlessException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void recargarTablas(boolean importando){
        try{
            if (jPLlamados.isShowing() || primerCarga || importando){
                primerCarga = false;
                recargarTablaLlamados(true);
            }
            if (jPPedidos.isShowing() || importando)
                recargarTablaPedidos();
            if (jPCabal.isShowing() || importando)
                recargarTablaCabal();
        }catch(Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
        }
    }
    
    public void wkAgregarFilas(){
        try{
            String userDir = System.getProperty("user.home");
            JFileChooser chooser = new JFileChooser(userDir +"/Desktop");
            if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION && ((chooser.getSelectedFile().getPath().endsWith(".xlsx")) || chooser.getSelectedFile().getPath().endsWith(".xls"))){
                //ESTO SE HACE EN UN WORKER MIENTRAS APARECE EL CARTEL DE CARGANDO
                jDCargando jdc = new jDCargando(null, true);
                workerImportar wk = new workerImportar(chooser.getSelectedFile().getPath(), jdc, new ArrayList());
                wk.execute();
                jdc.setVisible(true);
                wkActualizarTablas(true);
            }
        }catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    public void wkExportar(JButton boton){
        workerExportar wk = new workerExportar(this, boton);
        wk.execute();
    }
    
    private void cargarPreferencias(){
        ((BasicInternalFrameUI)this.getUI()).setNorthPane(null);
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(16);
        this.listaCambiosAGuardar = new ArrayList();
        
        Controlador ctrl = Controlador.getInstance();
        listaBancosString = new ArrayList();
        ArrayList<DTBancoInterior> listaInterior = ctrl.listarBancosInterior();
        ArrayList<DTBancoMontevideo> listaMontevideo = ctrl.listarBancosMontevideo();
        listaRamosMaster = ctrl.listarRamosMaster();
        for (DTBancoMontevideo dt: listaMontevideo){
            listaBancosString.add(dt.toString());
        }
        for (DTBancoInterior dt: listaInterior){
            listaBancosString.add(dt.toString());
        }
        listaCiudades = ctrl.listarCiudades();
        listaLlamados = new ArrayList();
        listaPedidos = new ArrayList();
        listaLlamadosYPedidos = new ArrayList();
        listaLlamadosYPedidosVista = new ArrayList();
        mapLlamadosYPedidosUnoPorFila = new LinkedHashMap();
        mapMasterYCabalUnoPorFila = new LinkedHashMap();
        listaCabal = new ArrayList();
        listaMasterYCabal = new ArrayList();
    }
    
    public void guardarCambios(){
        try{
            for (DTImport dt: listaCambiosAGuardar){
                if (dt.getId() == -1){
                    int id = Controlador.insertarImport(dt);
                    dt.setId(id);
                }else{
                    Controlador.modificarImport(dt);
                }
            }
            listaCambiosAGuardar.clear();
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    public void cargarGenerador() throws noSeleccionoElementoException{
        try{
            if (jPLlamados.isShowing()){
                DTImport dt = listaLlamadosYPedidosVista.get(indiceSeleccionado());
                jIFGenerarForm jif = new jIFGenerarForm(tabPane, dt);
                agregarTab("Nuevo formulario", jif);
            }else if (jPPedidos.isShowing()){
                DTImport dt = listaPedidos.get(indiceSeleccionado());
                jIFGenerarForm jif = new jIFGenerarForm(tabPane, dt);
                agregarTab("Nuevo formulario", jif);
            }else if (jPCabal.isShowing()){
                DTImport dt = listaCabal.get(indiceSeleccionado());
                jIFGenerarForm jif = new jIFGenerarForm(tabPane, dt);
                agregarTab("Nuevo formulario", jif);
            }
        }catch (Exception e){
            throw new noSeleccionoElementoException();
        }
    }
    
    public void pintarFilaSeleccionada(Color c) throws noSeleccionoElementoException{
        try{
            if (jPLlamados.isShowing()){
                int i = indiceSeleccionado();
                listaLlamadosYPedidosVista.get(i).setColor(c.getRGB());
                listaCambiosAGuardar.add(listaLlamadosYPedidosVista.get(i));
                wkGuardar(null);
                recargarTablaLlamados(false);
            }else if (jPPedidos.isShowing()){
                int i = indiceSeleccionado();
                listaPedidos.get(i).setColor(c.getRGB());
                listaCambiosAGuardar.add(listaPedidos.get(i));
                wkGuardar(null);
                recargarTablaPedidos();
            }else if (jPCabal.isShowing()){
                int i = indiceSeleccionado();
                listaCabal.get(i).setColor(c.getRGB());
                listaCambiosAGuardar.add(listaCabal.get(i));
                wkGuardar(null);
                recargarTablaCabal();
            }
        }catch(ArrayIndexOutOfBoundsException e){
            throw new noSeleccionoElementoException();
        }
    }
    
    private void agregarTab(String titulo, JInternalFrame ji) {
        tabPane.add(titulo, ji);
        tabPane.setTabComponentAt(tabPane.getTabCount()- 1,
                 new ButtonTabComponent(tabPane));
        tabPane.setSelectedComponent(ji);
    }
    
    public void agregarObservacionesFilaSeleccionada(String obs) throws noSeleccionoElementoException{
        try{
            if (jPLlamados.isShowing())
                listaLlamadosYPedidosVista.get(indiceSeleccionado()).setObservaciones(obs);
            else if (jPPedidos.isShowing())
                listaPedidos.get(indiceSeleccionado()).setObservaciones(obs);
            else if (jPCabal.isShowing())
                listaCabal.get(indiceSeleccionado()).setObservaciones(obs);
        }catch(ArrayIndexOutOfBoundsException e){
            throw new noSeleccionoElementoException();
        }
    }
    
    public String getObservacionesFilaSeleccionada() throws noSeleccionoElementoException{
        if (jPLlamados.isShowing())
            return listaLlamadosYPedidosVista.get(indiceSeleccionado()).getObservaciones();
        else if (jPPedidos.isShowing())
            return listaPedidos.get(indiceSeleccionado()).getObservaciones();
        else if (jPCabal.isShowing())
            return listaCabal.get(indiceSeleccionado()).getObservaciones();
        return "";
    } 
    
    public void wkBorrarSeleccionados(JButton boton){
//        workerBorrar wk = new workerBorrar(this, boton);
//        wk.execute();
    }
    
    public void wkGuardar(JButton boton){
        workerGuardar wk = new workerGuardar(this, boton);
        wk.execute();
    }
    
    public void borrarSeleccionados(){
        try{
            if (jPLlamados.isShowing()){
                if (jTLlamados.getSelectedRowCount()!= 0){
                    if (JOptionPane.showConfirmDialog(this, "¿Está seguro que desea borrar el elemento seleccionado?","",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                        int[] indices = jTLlamados.getSelectedRows();
                        //Recorro el arreglo desde el final 
                        for (int i = jTLlamados.getSelectedRowCount() - 1; i >= 0 ; i--){
                            //Lo convierto al indice correspondiente al filtrado
                            int iFiltrado = jTLlamados.getRowSorter().convertRowIndexToModel(indices[i]);
                            DTImport dtImp = listaLlamados.get(iFiltrado);
                            Controlador.borrarImport(dtImp);
                        }
                        wkActualizarTablas(false);
                    }
                }
            }else if (jPPedidos.isShowing()){
                if (jTPedidos.getSelectedRowCount()!= 0){
                    if (JOptionPane.showConfirmDialog(this, "¿Está seguro que desea borrar el elemento seleccionado?","",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                        int[] indices = jTPedidos.getSelectedRows();
                        //Recorro el arreglo desde el final 
                        for (int i = jTPedidos.getSelectedRowCount() - 1; i >= 0 ; i--){
                            //Lo convierto al indice correspondiente al filtrado
                            int iFiltrado = jTPedidos.getRowSorter().convertRowIndexToModel(indices[i]);
                            DTImport dtImp = listaPedidos.get(iFiltrado);
                            Controlador.borrarImport(dtImp);
                        }
                        wkActualizarTablas(false);
                    }
                }
            }
//            else if (jPLlamadosYpedidos.isShowing()){
//                if (jTLlamadosYPedidos.getSelectedRowCount()!= 0){
//                    if (JOptionPane.showConfirmDialog(this, "¿Está seguro que desea borrar el elemento seleccionado?","",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
//                        int[] indices = jTLlamadosYPedidos.getSelectedRows();
//                        //Recorro el arreglo desde el final 
//                        for (int i = jTLlamadosYPedidos.getSelectedRowCount() - 1; i >= 0 ; i--){
//                            //Lo convierto al indice correspondiente al filtrado
//                            int iFiltrado = jTLlamadosYPedidos.getRowSorter().convertRowIndexToModel(indices[i]);
//                             dtImp = listaLlamadosYPedidos.get(iFiltrado);
//                            Controlador.borrarImport(dtImp);
//                        }
//                        wkActualizarTablas();
//                    }
//                }
//            }
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(this, e.getMessage());
        }catch(Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(this, e.getMessage());}
    }
    
    private int indiceSeleccionado() throws noSeleccionoElementoException{
        try{
            if (jPLlamados.isShowing())
                return jTLlamados.getRowSorter().convertRowIndexToModel(jTLlamados.getSelectedRow());
            else if (jPPedidos.isShowing())
                return jTPedidos.getRowSorter().convertRowIndexToModel(jTPedidos.getSelectedRow());
            else if (jPCabal.isShowing())
                return jTCabal.getRowSorter().convertRowIndexToModel(jTCabal.getSelectedRow());
            return -1;
        }catch(Exception e){
            throw new noSeleccionoElementoException();
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPLlamados = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTLlamados = new javax.swing.JTable();
        jTFiltrarLlamados = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTCeldaLlamados = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPPedidos = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTPedidos = new javax.swing.JTable();
        jTFiltrarPedidos = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTCeldaPedidos = new javax.swing.JTextField();
        jScrollPane7 = new javax.swing.JScrollPane();
        jPCabal = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTCabal = new javax.swing.JTable();
        jTFiltrarCabal = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTCeldaCabal = new javax.swing.JTextField();

        setBorder(null);

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jTLlamados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTLlamados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTLlamadosMouseReleased(evt);
            }
        });
        jTLlamados.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTLlamadosKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(jTLlamados);

        jTFiltrarLlamados.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFiltrarLlamadosKeyReleased(evt);
            }
        });

        jLabel3.setText("Filtrar:");

        jTCeldaLlamados.setEditable(false);

        javax.swing.GroupLayout jPLlamadosLayout = new javax.swing.GroupLayout(jPLlamados);
        jPLlamados.setLayout(jPLlamadosLayout);
        jPLlamadosLayout.setHorizontalGroup(
            jPLlamadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPLlamadosLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTFiltrarLlamados))
            .addGroup(jPLlamadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTCeldaLlamados)
                .addContainerGap())
        );
        jPLlamadosLayout.setVerticalGroup(
            jPLlamadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPLlamadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTCeldaLlamados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPLlamadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTFiltrarLlamados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPLlamados);

        jTabbedPane1.addTab("Llamados y reclamos", jScrollPane1);

        jTPedidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTPedidos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTPedidosMouseReleased(evt);
            }
        });
        jTPedidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTPedidosKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(jTPedidos);

        jTFiltrarPedidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFiltrarPedidosKeyReleased(evt);
            }
        });

        jLabel4.setText("Filtrar:");

        jTCeldaPedidos.setEditable(false);

        javax.swing.GroupLayout jPPedidosLayout = new javax.swing.GroupLayout(jPPedidos);
        jPPedidos.setLayout(jPPedidosLayout);
        jPPedidosLayout.setHorizontalGroup(
            jPPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPPedidosLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTFiltrarPedidos))
            .addGroup(jPPedidosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTCeldaPedidos)
                .addContainerGap())
        );
        jPPedidosLayout.setVerticalGroup(
            jPPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPPedidosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTCeldaPedidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPPedidosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTFiltrarPedidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE))
        );

        jScrollPane3.setViewportView(jPPedidos);

        jTabbedPane1.addTab("Pedidos Master", jScrollPane3);

        jTCabal.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTCabal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTCabalMouseReleased(evt);
            }
        });
        jTCabal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTCabalKeyReleased(evt);
            }
        });
        jScrollPane8.setViewportView(jTCabal);

        jTFiltrarCabal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFiltrarCabalKeyReleased(evt);
            }
        });

        jLabel6.setText("Filtrar:");

        jTCeldaCabal.setEditable(false);

        javax.swing.GroupLayout jPCabalLayout = new javax.swing.GroupLayout(jPCabal);
        jPCabal.setLayout(jPCabalLayout);
        jPCabalLayout.setHorizontalGroup(
            jPCabalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPCabalLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTFiltrarCabal))
            .addGroup(jPCabalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTCeldaCabal)
                .addContainerGap())
        );
        jPCabalLayout.setVerticalGroup(
            jPCabalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPCabalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTCeldaCabal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPCabalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTFiltrarCabal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE))
        );

        jScrollPane7.setViewportView(jPCabal);

        jTabbedPane1.addTab("Cabal", jScrollPane7);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 627, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTFiltrarLlamadosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFiltrarLlamadosKeyReleased
        try{
            filterLlamados.setRowFilter(javax.swing.RowFilter.regexFilter("(?i)" + jTFiltrarLlamados.getText()));
        }catch(Exception e){}
    }//GEN-LAST:event_jTFiltrarLlamadosKeyReleased

    private void jTFiltrarPedidosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFiltrarPedidosKeyReleased
        try{
            filterPedidos.setRowFilter(javax.swing.RowFilter.regexFilter("(?i)" + jTFiltrarPedidos.getText()));
        }catch(Exception e){}
    }//GEN-LAST:event_jTFiltrarPedidosKeyReleased

    private void jTLlamadosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTLlamadosKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DELETE){
            wkBorrarSeleccionados(botonBorrar);
        }else if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_RIGHT || evt.getKeyCode() == KeyEvent.VK_LEFT || evt.getKeyCode() == KeyEvent.VK_DOWN){
            try {
                int row = indiceSeleccionado();
                int col = jTLlamados.getSelectedColumn();
                jTCeldaLlamados.setText((String)modelLlamados.getValueAt(row, col));
            } catch (noSeleccionoElementoException e) {
                Logger.getLogger(jIFImport.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }//GEN-LAST:event_jTLlamadosKeyReleased

    private void jTLlamadosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTLlamadosMouseReleased
        try {
            int row = indiceSeleccionado();
            int col = jTLlamados.getSelectedColumn();
            jTCeldaLlamados.setText((String)modelLlamados.getValueAt(row, col));
            System.out.println(listaLlamadosYPedidosVista.get(row).getId());
        } catch (noSeleccionoElementoException ex) {
            Logger.getLogger(jIFImport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jTLlamadosMouseReleased

    private void jTPedidosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTPedidosKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DELETE){
            wkBorrarSeleccionados(botonBorrar);
        }else if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_RIGHT || evt.getKeyCode() == KeyEvent.VK_LEFT || evt.getKeyCode() == KeyEvent.VK_DOWN){
            try {
                int row = indiceSeleccionado();
                int col = jTPedidos.getSelectedColumn();
                jTCeldaPedidos.setText((String)modelPedidos.getValueAt(row, col));
            } catch (noSeleccionoElementoException ex) {
                Logger.getLogger(jIFImport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jTPedidosKeyReleased

    private void jTPedidosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTPedidosMouseReleased
        try {
            int row = indiceSeleccionado();
            int col = jTPedidos.getSelectedColumn();
            jTCeldaPedidos.setText((String)modelPedidos.getValueAt(row, col));
        } catch (noSeleccionoElementoException ex) {
            Logger.getLogger(jIFImport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jTPedidosMouseReleased

    private void jTCabalMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTCabalMouseReleased
        try {
            int row = indiceSeleccionado();
            int col = jTCabal.getSelectedColumn();
            jTCeldaCabal.setText((String)modelCabal.getValueAt(row, col));
        } catch (noSeleccionoElementoException ex) {
            Logger.getLogger(jIFImport.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jTCabalMouseReleased

    private void jTCabalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTCabalKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DELETE){
            wkBorrarSeleccionados(botonBorrar);
        }else if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_RIGHT || evt.getKeyCode() == KeyEvent.VK_LEFT || evt.getKeyCode() == KeyEvent.VK_DOWN){
            try {
                int row = indiceSeleccionado();
                int col = jTCabal.getSelectedColumn();
                jTCeldaCabal.setText((String)modelCabal.getValueAt(row, col));
            } catch (noSeleccionoElementoException ex) {
                Logger.getLogger(jIFImport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jTCabalKeyReleased

    private void jTFiltrarCabalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFiltrarCabalKeyReleased
        try{
            filterCabal.setRowFilter(javax.swing.RowFilter.regexFilter("(?i)" + jTFiltrarCabal.getText()));
        }catch(Exception e){}
    }//GEN-LAST:event_jTFiltrarCabalKeyReleased

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        if (jPLlamados.isShowing()){
            if (listaLlamadosYPedidosVista.isEmpty())
                wkActualizarTablas(false);
        }else if (jPPedidos.isShowing()){
            if (listaPedidos.isEmpty())
                wkActualizarTablas(false);
        }else if (jPCabal.isShowing()){
            if (listaCabal.isEmpty())
                wkActualizarTablas(false);
        }
    }//GEN-LAST:event_jTabbedPane1StateChanged

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPCabal;
    private javax.swing.JPanel jPLlamados;
    private javax.swing.JPanel jPPedidos;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTable jTCabal;
    private javax.swing.JTextField jTCeldaCabal;
    private javax.swing.JTextField jTCeldaLlamados;
    private javax.swing.JTextField jTCeldaPedidos;
    private javax.swing.JTextField jTFiltrarCabal;
    private javax.swing.JTextField jTFiltrarLlamados;
    private javax.swing.JTextField jTFiltrarPedidos;
    private javax.swing.JTable jTLlamados;
    private javax.swing.JTable jTPedidos;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables
}
