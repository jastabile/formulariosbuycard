/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import components.ButtonTabComponent;
import controladores.Controlador;
import datatypes.DTFormularioAnda;
import datatypes.DTFormularioCabal;
import datatypes.DTFormularioMaster;
import excepciones.BDException;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import logica.workerActualizar;
import logica.workerBorrar;

/**
 *
 * @author Javier
 */
public class jIFOpen extends javax.swing.JInternalFrame {
    
    private DefaultTableModel modelCabal;
    private DefaultTableModel modelMaster;
    private DefaultTableModel modelAnda;
    private TableRowSorter<DefaultTableModel> filterCabal;
    private TableRowSorter<DefaultTableModel> filterMaster;
    private TableRowSorter<DefaultTableModel> filterAnda;
    private ArrayList<DTFormularioCabal> listaCabal;
    private ArrayList<DTFormularioMaster> listaMaster;
    private ArrayList<DTFormularioAnda> listaAnda;
    private JTabbedPane tabPane;
    
    public jIFOpen(JTabbedPane tabPane) {
        initComponents();
        ((BasicInternalFrameUI)this.getUI()).setNorthPane(null);
        this.tabPane = tabPane;
        preferenciasTablas();
        wkRecargar();
    }

    private void preferenciasTablas(){
        modelCabal = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };
        modelMaster = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };
        
        modelAnda = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };

        filterCabal = new TableRowSorter(modelCabal);
        jTableFormsCabal.setModel(modelCabal);
        jTableFormsCabal.setRowSorter(filterCabal);
        jTableFormsCabal.getTableHeader().setReorderingAllowed(false);

        filterMaster = new TableRowSorter(modelMaster);
        jTableFormsMaster.setModel(modelMaster);
        jTableFormsMaster.setRowSorter(filterMaster);
        jTableFormsMaster.getTableHeader().setReorderingAllowed(false);
        
        filterAnda = new TableRowSorter(modelAnda);
        jTableFormsAnda.setModel(modelAnda);
        jTableFormsAnda.setRowSorter(filterAnda);
        jTableFormsAnda.getTableHeader().setReorderingAllowed(false);

        listaCabal = null;
        listaMaster = null;
        listaAnda = null;

        modelCabal.addColumn("RUT");
        modelCabal.addColumn("Nombre Fantasía");
        modelCabal.addColumn("Departamento");
        modelCabal.addColumn("Guardado");

        modelMaster.addColumn("RUT");
        modelMaster.addColumn("Nombre Fantasía");
        modelMaster.addColumn("Departamento");
        modelMaster.addColumn("Guardado");
        
        modelAnda.addColumn("RUT");
        modelAnda.addColumn("Nombre Fantasía");
        modelAnda.addColumn("Departamento");
        modelAnda.addColumn("Guardado");

        //Scroll vertical mas rapido
        jScrollPane4.getVerticalScrollBar().setUnitIncrement(16);
        jScrollPane6.getVerticalScrollBar().setUnitIncrement(16);
        jScrollPane7.getVerticalScrollBar().setUnitIncrement(16);
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPCabal = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jTFiltrarCabal = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTableFormsCabal = new javax.swing.JTable();
        jPMaster = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jTFiltrarMaster = new javax.swing.JTextField();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTableFormsMaster = new javax.swing.JTable();
        jPAnda = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jTFiltrarAnda = new javax.swing.JTextField();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTableFormsAnda = new javax.swing.JTable();

        setBackground(java.awt.Color.lightGray);
        setBorder(null);

        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseReleased(evt);
            }
        });

        jLabel3.setText("Filtrar:");

        jTFiltrarCabal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFiltrarCabalKeyReleased(evt);
            }
        });

        jTableFormsCabal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableFormsCabalMouseClicked(evt);
            }
        });
        jTableFormsCabal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableFormsCabalKeyReleased(evt);
            }
        });
        jScrollPane4.setViewportView(jTableFormsCabal);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 760, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTFiltrarCabal)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTFiltrarCabal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPCabalLayout = new javax.swing.GroupLayout(jPCabal);
        jPCabal.setLayout(jPCabalLayout);
        jPCabalLayout.setHorizontalGroup(
            jPCabalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPCabalLayout.setVerticalGroup(
            jPCabalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Cabal", jPCabal);

        jLabel4.setText("Filtrar:");

        jTFiltrarMaster.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFiltrarMasterKeyReleased(evt);
            }
        });

        jTableFormsMaster.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableFormsMaster.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableFormsMasterMouseClicked(evt);
            }
        });
        jTableFormsMaster.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableFormsMasterKeyReleased(evt);
            }
        });
        jScrollPane6.setViewportView(jTableFormsMaster);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 760, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTFiltrarMaster)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTFiltrarMaster, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPMasterLayout = new javax.swing.GroupLayout(jPMaster);
        jPMaster.setLayout(jPMasterLayout);
        jPMasterLayout.setHorizontalGroup(
            jPMasterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPMasterLayout.setVerticalGroup(
            jPMasterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Master", jPMaster);

        jLabel6.setText("Filtrar:");

        jTFiltrarAnda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFiltrarAndaKeyReleased(evt);
            }
        });

        jTableFormsAnda.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableFormsAnda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableFormsAndaMouseClicked(evt);
            }
        });
        jTableFormsAnda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTableFormsAndaKeyReleased(evt);
            }
        });
        jScrollPane7.setViewportView(jTableFormsAnda);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 760, Short.MAX_VALUE)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTFiltrarAnda)))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTFiltrarAnda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPAndaLayout = new javax.swing.GroupLayout(jPAnda);
        jPAnda.setLayout(jPAndaLayout);
        jPAndaLayout.setHorizontalGroup(
            jPAndaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPAndaLayout.setVerticalGroup(
            jPAndaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Anda", jPAnda);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void modificar(){
        if (jPCabal.isShowing()){
            if (jTableFormsCabal.getSelectedRow() != -1)
                cargarFormCabal();
            else
                JOptionPane.showMessageDialog(null, "Debe seleccionar un elemento primero");
        }else if (jPMaster.isShowing()){
            if (jTableFormsMaster.getSelectedRow() != -1)
                cargarFormMaster();
            else
                JOptionPane.showMessageDialog(null, "Debe seleccionar un elemento primero");
        }else{
            if (jTableFormsAnda.getSelectedRow() != -1)
                cargarFormAnda();
            else
                JOptionPane.showMessageDialog(null, "Debe seleccionar un elemento primero");
        }
    }
    
    public void wkEliminar(JButton boton){
        workerBorrar wk = new workerBorrar(this, boton);
        wk.execute();
    }
    
    public void eliminar(){
        try{
            if (jPCabal.isShowing()){
                if (jTableFormsCabal.getSelectedRow() != -1){
                    DTFormularioCabal dt = listaCabal.get(jTableFormsCabal.getRowSorter().convertRowIndexToModel(jTableFormsCabal.getSelectedRow()));
                    if (JOptionPane.showConfirmDialog(this, "¿Está seguro que desea eliminar el/los formulario/s seleccionado/s?", "¿Desea continuar?", JOptionPane.YES_NO_OPTION) == 0){
                        int[] indices = jTableFormsCabal.getSelectedRows();
                        //Recorro el arreglo desde el final 
                        for (int i = jTableFormsCabal.getSelectedRowCount() - 1; i >= 0 ; i--){
                            //Lo convierto al indice correspondiente al filtrado
                            int iFiltrado = jTableFormsCabal.getRowSorter().convertRowIndexToModel(indices[i]);
                            Controlador.borrarCabal(listaCabal.get(iFiltrado).getId());
                        }
                        recargarTablaCabal();
                    }
                }else
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un elemento primero");
            }else if (jPMaster.isShowing()){
                if (jTableFormsMaster.getSelectedRow() != -1){
                    DTFormularioMaster dt = listaMaster.get(jTableFormsMaster.getRowSorter().convertRowIndexToModel(jTableFormsMaster.getSelectedRow()));
                    if (JOptionPane.showConfirmDialog(this, "¿Está seguro que desea eliminar el formulario correspondiente al RUT " + dt.getRUT() + "?", "¿Desea continuar?", JOptionPane.YES_NO_OPTION) == 0){
                        int[] indices = jTableFormsMaster.getSelectedRows();
                        //Recorro el arreglo desde el final 
                        for (int i = jTableFormsMaster.getSelectedRowCount() - 1; i >= 0 ; i--){
                            //Lo convierto al indice correspondiente al filtrado
                            int iFiltrado = jTableFormsMaster.getRowSorter().convertRowIndexToModel(indices[i]);
                            Controlador.borrarMaster(listaMaster.get(iFiltrado).getId());
                        }
                        recargarTablaMaster();
                    }
                }else
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un elemento primero");
            }else{
                if (jTableFormsAnda.getSelectedRow() != -1){
                    DTFormularioAnda dt = listaAnda.get(jTableFormsAnda.getRowSorter().convertRowIndexToModel(jTableFormsAnda.getSelectedRow()));
                    if (JOptionPane.showConfirmDialog(this, "¿Está seguro que desea eliminar el formulario correspondiente al RUT " + dt.getRUT() + "?", "¿Desea continuar?", JOptionPane.YES_NO_OPTION) == 0){
                        int[] indices = jTableFormsAnda.getSelectedRows();
                        //Recorro el arreglo desde el final 
                        for (int i = jTableFormsAnda.getSelectedRowCount() - 1; i >= 0 ; i--){
                            //Lo convierto al indice correspondiente al filtrado
                            int iFiltrado = jTableFormsAnda.getRowSorter().convertRowIndexToModel(indices[i]);
                            Controlador.borrarAnda(listaAnda.get(iFiltrado).getId());
                        }
                        recargarTablaAnda();
                    }
                }else
                    JOptionPane.showMessageDialog(null, "Debe seleccionar un elemento primero");
            }
        }catch(BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    public void wkRecargar(){
        jDCargando jdc = new jDCargando(null, true);
        workerActualizar wk = new workerActualizar(jdc, this);
        wk.execute();
        jdc.setVisible(true);
    }
    
    public void recargar(){
        if (jPCabal.isShowing()){
            recargarTablaCabal();
        }else if (jPMaster.isShowing()){
            recargarTablaMaster();
        }else if (jPAnda.isShowing()){
            recargarTablaAnda();
        }else{
            recargarTablaCabal();
        }
    }
    
    private void jTFiltrarCabalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFiltrarCabalKeyReleased
        filterCabal.setRowFilter(javax.swing.RowFilter.regexFilter("(?i)" + jTFiltrarCabal.getText()));
    }//GEN-LAST:event_jTFiltrarCabalKeyReleased

    private void jTableFormsCabalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableFormsCabalMouseClicked
        if (evt.getClickCount() == 2){
            cargarFormCabal();
        }
    }//GEN-LAST:event_jTableFormsCabalMouseClicked

    private void jTFiltrarMasterKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFiltrarMasterKeyReleased
        filterMaster.setRowFilter(javax.swing.RowFilter.regexFilter("(?i)" + jTFiltrarMaster.getText()));
    }//GEN-LAST:event_jTFiltrarMasterKeyReleased

    private void jTableFormsMasterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableFormsMasterMouseClicked
        if (evt.getClickCount() == 2){
            cargarFormMaster();
        }
    }//GEN-LAST:event_jTableFormsMasterMouseClicked

    private void jTabbedPane1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseReleased
        if (jPCabal.isShowing() && listaCabal == null){
            wkRecargar();
        }else if (jPMaster.isShowing() && listaMaster == null){
            wkRecargar();
        }else if (jPAnda.isShowing() && listaAnda == null){
            wkRecargar();
        }
    }//GEN-LAST:event_jTabbedPane1MouseReleased

    private void jTableFormsCabalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableFormsCabalKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DELETE){
            wkEliminar(null);
        }
    }//GEN-LAST:event_jTableFormsCabalKeyReleased

    private void jTableFormsMasterKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableFormsMasterKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DELETE){
            eliminar();
        }
    }//GEN-LAST:event_jTableFormsMasterKeyReleased

    private void jTFiltrarAndaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFiltrarAndaKeyReleased
        filterAnda.setRowFilter(javax.swing.RowFilter.regexFilter("(?i)" + jTFiltrarAnda.getText()));
    }//GEN-LAST:event_jTFiltrarAndaKeyReleased

    private void jTableFormsAndaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableFormsAndaMouseClicked
        if (evt.getClickCount() == 2){
            cargarFormAnda();
        }
    }//GEN-LAST:event_jTableFormsAndaMouseClicked

    private void jTableFormsAndaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTableFormsAndaKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DELETE){
            eliminar();
        }
    }//GEN-LAST:event_jTableFormsAndaKeyReleased

    private void agregarTab(String titulo, JInternalFrame ji) {
        tabPane.add(titulo, ji);
        tabPane.setTabComponentAt(tabPane.getTabCount()- 1,
                 new ButtonTabComponent(tabPane));
    }
    
    private void cargarFormMaster(){
        DTFormularioMaster dt = listaMaster.get(jTableFormsMaster.getRowSorter().convertRowIndexToModel(jTableFormsMaster.getSelectedRow()));
        boolean val = dt.isVal();
        jIFVistaFormFirstData ji = new jIFVistaFormFirstData(dt, val);
        String name = "Master - ";
        if (val)
            name = "Master + VAL - ";
        agregarTab(name + dt.getRUT(), ji);
        tabPane.setSelectedIndex(tabPane.getTabCount() - 1);
    }
    
    private void cargarFormCabal(){
        DTFormularioCabal dt = listaCabal.get(jTableFormsCabal.getRowSorter().convertRowIndexToModel(jTableFormsCabal.getSelectedRow()));
        jIFFormCabal ji = new jIFFormCabal(dt);
        agregarTab("Cabal - " + dt.getRUT(), ji);
        tabPane.setSelectedIndex(tabPane.getTabCount() - 1);
    }
    
    private void cargarFormAnda(){
        DTFormularioAnda dt = listaAnda.get(jTableFormsAnda.getRowSorter().convertRowIndexToModel(jTableFormsAnda.getSelectedRow()));
        jIFFormAnda ji = new jIFFormAnda(dt);
        agregarTab("Anda - " + dt.getRUT(), ji);
        tabPane.setSelectedIndex(tabPane.getTabCount() - 1);
    }
    
    
    private void recargarTablaCabal(){
        try{
            modelCabal.setRowCount(0);
            listaCabal = Controlador.buscarCabal();
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String codigoPostalDepartamento;
            for (DTFormularioCabal dt : listaCabal){
                codigoPostalDepartamento = dt.getCodPostalDepartamento();
                String[] partes = codigoPostalDepartamento.split("-");
                try{
                    Object[] fila = {dt.getRUT(), dt.getNombreFantasia(), partes[1], format.format(dt.getHoraGuardado())};
                    modelCabal.addRow(fila);
                }catch(IndexOutOfBoundsException io){
                    //partes[1] error
                    Object[] fila = {dt.getRUT(), dt.getNombreFantasia(), codigoPostalDepartamento, format.format(dt.getHoraGuardado())};
                    modelCabal.addRow(fila);
                }
            }
        }catch(BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(null, e.getMessage());
        }catch(Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(null, e.toString());
        }
    }
    
    private void recargarTablaMaster(){
        try{
            modelMaster.setRowCount(0);
            listaMaster = Controlador.buscarMaster();
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            for (DTFormularioMaster dt : listaMaster){
                Object[] fila = {dt.getRUT(), dt.getNombreFantasia(), dt.getLocalidad(), format.format(dt.getHoraGuardado())};
                modelMaster.addRow(fila);
            }
        }catch(BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    private void recargarTablaAnda(){
        try{
            modelAnda.setRowCount(0);
            listaAnda = Controlador.buscarAnda();
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            for (DTFormularioAnda dt : listaAnda){
                Object[] fila = {dt.getRUT(), dt.getNombreComercial(), dt.getDepartamento(), format.format(dt.getHoraGuardado())};
                modelAnda.addRow(fila);
            }
        }catch(BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPAnda;
    private javax.swing.JPanel jPCabal;
    private javax.swing.JPanel jPMaster;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTextField jTFiltrarAnda;
    private javax.swing.JTextField jTFiltrarCabal;
    private javax.swing.JTextField jTFiltrarMaster;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableFormsAnda;
    private javax.swing.JTable jTableFormsCabal;
    private javax.swing.JTable jTableFormsMaster;
    // End of variables declaration//GEN-END:variables
}
