/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import components.ButtonTabComponent;
import controladores.Controlador;
import controladores.ControladorExceptions;
import datatypes.DTBanco;
import datatypes.DTBancoInterior;
import datatypes.DTBancoMontevideo;
import datatypes.DTCiudad;
import datatypes.DTFormularioAnda;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import datatypes.DTFormularioCabal;
import datatypes.DTFormularioMaster;
import datatypes.DTImport;
import excepciones.BDException;
import excepciones.noSeleccionoBancoException;
import excepciones.noSeleccionoCiudadInteriorException;
import excepciones.noSeleccionoRamoCabalException;
import excepciones.noSeleccionoRamoMasterException;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JTabbedPane;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import logica.ramoAnda;
import logica.ramoCabal;
import logica.ramoMaster;
import logica.workerGuardar;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author Javier
 */
public class jIFGenerarForm extends javax.swing.JInternalFrame {

    private JTabbedPane tabPane;
    private DefaultComboBoxModel modelComboBancos;
    private DefaultComboBoxModel modelComboRamosMaster;
    private DefaultComboBoxModel modelComboRamosCabal;
    private DefaultComboBoxModel modelComboCiudades;
    private ArrayList<ramoAnda> listaRamosAnda;
    private DTImport dt;
    
    private final String TIPO_LLAMADOS_Y_PEDIDOS = "LLAMADOS Y PEDIDOS MASTER";
    private final String TIPO_MASTER_Y_CABAL = "MASTER Y CABAL";
            
    public jIFGenerarForm(JTabbedPane tabPane, DTImport dt) {
        initComponents();
        this.tabPane = tabPane;
        cargarPreferencias();
        cargarCombos();
        tecla_tab();
        
        this.dt = dt;
        if (dt != null){
            cargarDatos();
        }
    }
    
    public void wkGuardar(JButton boton){
        workerGuardar wk = new workerGuardar(this, boton);
        wk.execute();
    }
    
    public void actualizarTablaImports(){
        jIFImport.getInstance(tabPane, null).actualizarTablas(false);
    }
    
    public void guardarCambios(){
        try{
            DTImport dtGuardar = getDatosAGuardar();
            if (dtGuardar.getId() == -1){
                int id = Controlador.insertarImport(dtGuardar);
                dtGuardar.setId(id);
                dt = dtGuardar;
            }
            else
                Controlador.modificarImport(dtGuardar);
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void cargarDatos(){
        if (dt.getTipo().equals(TIPO_LLAMADOS_Y_PEDIDOS))
            jLTipoLlamado.setText("LLAMADO Y PEDIDO");
        else if ((dt.getTipo().equals(TIPO_MASTER_Y_CABAL)))
            jLTipoLlamado.setText("MASTER Y CABAL");
        jTNombreFantasia.setText(dt.getNombreFantasia());
        jTRUT.setText(dt.getRUT());
        jTRazonSocial.setText(dt.getRazonSocial());
        jTTipoRUT.setText(dt.getTipoCabal());
        jTCalle.setText(dt.getCalle());
        jTNumeroPuerta.setText(dt.getNumero());
        jTApto.setText(dt.getApto());
        if (dt.isMontevideo()){
            jRBMontevideo.setSelected(true);
            jRBInterior.setSelected(false);
            jTCodigoPostal.setText(dt.getCodPostal());
        }else{
            jRBMontevideo.setSelected(false);
            jRBInterior.setSelected(true);
            jComboCiudades.setSelectedIndex(dt.getLocalidad());
        }
        jTTelefono.setText(dt.getTelefono());
        jTPos.setText(dt.getPos());
        jTMail.setText(dt.getMail());
        jTRepresentanteLegal.setText(dt.getRepresentanteLegal());
        jTDocumento.setText(dt.getDocumento());
        jRBPesos.setSelected(false);
        jRBDolares.setSelected(false);
        if (dt.isPesosSelected()){
            jRBPesos.setSelected(true);
            jTCuentaNumero.setText(dt.getPesos());
        }
        if (dt.isDolaresSelected()){
            jRBDolares.setSelected(true);
            jTCuentaNumeroDolares.setEditable(true);
            jTCuentaNumeroDolares.setText(dt.getDolares());
        }
        if (!dt.isPesosSelected() && !dt.isDolaresSelected())
            jRBPesos.setSelected(true);
        jComboBancos.setSelectedIndex(dt.getBanco());
        jComboCabal.setSelectedIndex(dt.getRamoCabal());
        jComboMaster.setSelectedIndex(dt.getRamoMaster());
            
    }
    
    private void cargarPreferencias(){
        ((BasicInternalFrameUI)this.getUI()).setNorthPane(null);
        //Scroll vertical mas rapido
        this.getRootPane().setDefaultButton(jBGenerar);
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(16);
        
        //Oculto cosas
        jPanelDistintoDom.setVisible(false);
        jLLocalidad.setVisible(false);
        jComboCiudades.setVisible(false);
        jBBuscarCiudad.setVisible(false);
        
        //montevideo selected
        jRBMontevideo.setSelected(true);
    }
      
    private void tecla_tab(){
        jTCuentaNumero.setFocusTraversalKeysEnabled(false);
        jTApto.setFocusTraversalKeysEnabled(false);
        jTCodigoPostal.setFocusTraversalKeysEnabled(false);
        jComboCiudades.setFocusTraversalKeysEnabled(false);
        jTTipoRUT.setFocusTraversalKeysEnabled(false);
        jTDocumento.setFocusTraversalKeysEnabled(false);
        jTCuentaNumeroDolares.setFocusTraversalKeysEnabled(false);
    }

    private void cargarCombos(){
        modelComboBancos = new DefaultComboBoxModel();
        modelComboRamosCabal = new DefaultComboBoxModel();
        modelComboRamosMaster = new DefaultComboBoxModel();
        modelComboCiudades = new DefaultComboBoxModel();
        jComboBancos.setModel(modelComboBancos);
        jComboCabal.setModel(modelComboRamosCabal);
        jComboMaster.setModel(modelComboRamosMaster);
        jComboCiudades.setModel(modelComboCiudades);
        listaRamosAnda = Controlador.getInstance().listarRamosAnda();
//        AutoCompleteDecorator.decorate(jComboBancos);
//        AutoCompleteDecorator.decorate(jComboCabal);
//        AutoCompleteDecorator.decorate(jComboMaster);
        AutoCompleteDecorator.decorate(jComboCiudades);
        Controlador ctrl = Controlador.getInstance();
        
        //carga combo bancos
        ArrayList<DTBancoInterior> listaInterior = ctrl.listarBancosInterior();
        ArrayList<DTBancoMontevideo> listaMontevideo = ctrl.listarBancosMontevideo();
        for (DTBancoMontevideo dt: listaMontevideo){
            modelComboBancos.addElement(dt);
        }
        for (DTBancoInterior dt: listaInterior){
            modelComboBancos.addElement(dt);
        }
        
        //carga combo Cabal
        ArrayList<ramoCabal> listaRamosCabal = ctrl.listarRamosCabal();
        for (ramoCabal dt: listaRamosCabal){
            modelComboRamosCabal.addElement(dt);
        }
        
        //carga combo Master
        ArrayList<ramoMaster> listaRamosMaster = ctrl.listarRamosMaster();
        for (ramoMaster dt: listaRamosMaster){
            modelComboRamosMaster.addElement(dt);
        }
        
        //carga combo Ciudades
        ArrayList<DTCiudad> listaCiudades = ctrl.listarCiudades();
        for (DTCiudad dt: listaCiudades){
            modelComboCiudades.addElement(dt);
        }
        jComboBancos.setSelectedIndex(-1);
        jComboCabal.setSelectedIndex(-1);
        jComboMaster.setSelectedIndex(-1);
    }
    
    private DTImport getDatosAGuardar(){
        String tipo = this.dt.getTipo();
        String nombreFantasia = jTNombreFantasia.getText();
        String RUT = jTRUT.getText();
        String razonSocial = jTRazonSocial.getText();
        String tipoCabal = jTTipoRUT.getText();
        String calle = jTCalle.getText();
        String numero = jTNumeroPuerta.getText();
        String apto = jTApto.getText();
        boolean montevideo = jRBMontevideo.isSelected();
        String codigoPostal = "";
        int localidad = -1;
        String localidadString = this.dt.getLocalidadString();
        if (montevideo)
            codigoPostal = jTCodigoPostal.getText();
        else
            localidad = jComboCiudades.getSelectedIndex();
        String telefono = jTTelefono.getText();
        String pos = jTPos.getText();
        String mail = jTMail.getText();
        String representanteLegal = jTRepresentanteLegal.getText();
        String documento = jTDocumento.getText();
        boolean cajaAhorro = jRBCA.isSelected();
        boolean pesosSelected = jRBPesos.isSelected();
        boolean dolaresSelected = jRBDolares.isSelected();
        String pesos = "";
        String dolares = "";
        if (pesosSelected)
            pesos = jTCuentaNumero.getText();
        if (dolaresSelected)
            dolares = jTCuentaNumeroDolares.getText();
        int banco = jComboBancos.getSelectedIndex();
        String bancoString = this.dt.getBancoString();
        int ramoCabal = jComboCabal.getSelectedIndex();
        int ramoMaster = jComboMaster.getSelectedIndex();
        String ramoString = this.dt.getRamoString();
        int id = -1;
        int color = 0;
        String comentarios1 = this.dt.getComentarios1();
        String comentarios2 = this.dt.getComentarios2();
        String observaciones = "";
        String estado = this.dt.getEstado();
        String fecha = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        if (dt != null){
            id = dt.getId();
            color = dt.getColor();
            observaciones = dt.getObservaciones();
            fecha = dt.getFecha();
        }
        if (localidad != -1)
            localidadString = ((DTCiudad)jComboCiudades.getSelectedItem()).toString();
        if (banco != -1)
            bancoString = ((DTBanco)jComboBancos.getSelectedItem()).toString();
        if (ramoMaster != -1)
            ramoString = ((ramoMaster)jComboMaster.getSelectedItem()).toString();
        DTImport ret = new DTImport(id, tipo, nombreFantasia, 
                RUT, razonSocial, tipoCabal, calle, numero, apto, montevideo, 
                codigoPostal, localidad, localidadString, telefono, "", 
                pos, mail, representanteLegal, documento, cajaAhorro, 
                pesosSelected, dolaresSelected, pesos, dolares, banco, 
                bancoString, ramoCabal, ramoMaster, ramoString, color, 
                comentarios1, comentarios2, observaciones, estado, fecha);
        return ret;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel8 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jTNombreFantasia = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTRazonSocial = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTRUT = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jTTipoRUT = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        jPanelDistintoDom = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jTCodigoPostal1 = new javax.swing.JTextField();
        jTDepartamento1 = new javax.swing.JTextField();
        jTNumeroPuerta1 = new javax.swing.JTextField();
        jTCalle1 = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jTApto1 = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jRBInterior = new javax.swing.JRadioButton();
        jPanelMontevideo = new javax.swing.JPanel();
        jTCodigoPostal = new javax.swing.JTextField();
        jLCodPostal = new javax.swing.JLabel();
        jLLocalidad = new javax.swing.JLabel();
        jComboCiudades = new org.jdesktop.swingx.JXComboBox();
        jBBuscarCiudad = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTApto = new javax.swing.JTextField();
        jTNumeroPuerta = new javax.swing.JTextField();
        jRBMontevideo = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jTCalle = new javax.swing.JTextField();
        jRBDistintoDomic = new javax.swing.JRadioButton();
        jPanel4 = new javax.swing.JPanel();
        jButton4 = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jTPos = new javax.swing.JTextField();
        jComboMaster = new org.jdesktop.swingx.JXComboBox();
        jLabel13 = new javax.swing.JLabel();
        jTCuentaNumeroDolares = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jComboCabal = new org.jdesktop.swingx.JXComboBox();
        jRBCA = new javax.swing.JRadioButton();
        jTCuentaNumero = new javax.swing.JTextField();
        jTRepresentanteLegal = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTDocumento = new javax.swing.JTextField();
        jComboBancos = new org.jdesktop.swingx.JXComboBox();
        jLabel20 = new javax.swing.JLabel();
        jRBCC = new javax.swing.JRadioButton();
        jRBPesos = new javax.swing.JRadioButton();
        jTTelefono = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jRBDolares = new javax.swing.JRadioButton();
        jTMail = new javax.swing.JTextField();
        jTTitularCuenta = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jBGenerar = new javax.swing.JButton();
        jRBMaster = new javax.swing.JRadioButton();
        jRBCabal = new javax.swing.JRadioButton();
        jRBAnda = new javax.swing.JRadioButton();
        jRBMasterMasVal = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        jRBModif = new javax.swing.JRadioButton();
        jRBAlta = new javax.swing.JRadioButton();
        jLTipoLlamado = new javax.swing.JLabel();

        setBorder(null);
        setClosable(true);
        setTitle("Nuevo formulario");

        jPanel8.setBackground(java.awt.Color.lightGray);

        jScrollPane1.setPreferredSize(new java.awt.Dimension(1090, 650));

        jPanel1.setPreferredSize(new java.awt.Dimension(1120, 680));

        jLabel2.setText("NOMBRE FANTASÍA");

        jLabel3.setText("RAZÓN SOCIAL");

        jLabel4.setText("RUT");

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/dgilogo.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel21.setText("TIPO CABAL:");

        jTTipoRUT.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTTipoRUTKeyPressed(evt);
            }
        });

        jLabel24.setText("ALT. PTA - APTO");

        jLabel23.setText("N° DE PUERTA");

        jLabel22.setText("CALLE");

        jLabel26.setText("DEPARTAMENTO");

        jLabel25.setText("CÓDIGO POSTAL");

        javax.swing.GroupLayout jPanelDistintoDomLayout = new javax.swing.GroupLayout(jPanelDistintoDom);
        jPanelDistintoDom.setLayout(jPanelDistintoDomLayout);
        jPanelDistintoDomLayout.setHorizontalGroup(
            jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelDistintoDomLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTCalle1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22))
                .addGap(18, 18, 18)
                .addGroup(jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTNumeroPuerta1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23))
                .addGap(18, 18, 18)
                .addGroup(jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel24)
                    .addComponent(jTApto1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTCodigoPostal1, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                    .addComponent(jLabel25))
                .addGap(18, 18, 18)
                .addGroup(jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTDepartamento1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel26))
                .addGap(0, 0, 0))
        );
        jPanelDistintoDomLayout.setVerticalGroup(
            jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelDistintoDomLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanelDistintoDomLayout.createSequentialGroup()
                        .addGroup(jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel25)
                            .addComponent(jLabel26))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTCodigoPostal1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTDepartamento1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanelDistintoDomLayout.createSequentialGroup()
                        .addGroup(jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(jLabel23)
                            .addComponent(jLabel24))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelDistintoDomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTCalle1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTNumeroPuerta1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTApto1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jRBInterior.setText("Interior");
        jRBInterior.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jRBInteriorItemStateChanged(evt);
            }
        });

        jTCodigoPostal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTCodigoPostalKeyPressed(evt);
            }
        });

        jLCodPostal.setText("CÓDIGO POSTAL");

        jLLocalidad.setText("LOCALIDAD");

        jBBuscarCiudad.setText("Buscar");
        jBBuscarCiudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBuscarCiudadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelMontevideoLayout = new javax.swing.GroupLayout(jPanelMontevideo);
        jPanelMontevideo.setLayout(jPanelMontevideoLayout);
        jPanelMontevideoLayout.setHorizontalGroup(
            jPanelMontevideoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMontevideoLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanelMontevideoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelMontevideoLayout.createSequentialGroup()
                        .addComponent(jComboCiudades, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jBBuscarCiudad))
                    .addComponent(jLCodPostal)
                    .addComponent(jLLocalidad)
                    .addComponent(jTCodigoPostal, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanelMontevideoLayout.setVerticalGroup(
            jPanelMontevideoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelMontevideoLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLCodPostal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTCodigoPostal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLLocalidad)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelMontevideoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboCiudades, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBBuscarCiudad))
                .addGap(0, 0, 0))
        );

        jLabel7.setText("N° DE PUERTA");

        jLabel8.setText("ALT. PTA - APTO");

        jTApto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTAptoKeyPressed(evt);
            }
        });

        jRBMontevideo.setSelected(true);
        jRBMontevideo.setText("Montevideo");
        jRBMontevideo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jRBMontevideoItemStateChanged(evt);
            }
        });

        jLabel6.setText("CALLE");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTCalle, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTNumeroPuerta, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jTApto, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jRBMontevideo)
                    .addComponent(jRBInterior))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanelMontevideo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelMontevideo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jRBMontevideo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTNumeroPuerta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTApto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTCalle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jRBInterior))))
                .addGap(0, 0, 0))
        );

        jRBDistintoDomic.setText("Paga en distinto domicilio");
        jRBDistintoDomic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRBDistintoDomicActionPerformed(evt);
            }
        });

        jButton4.setText("Busqueda avanzada");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel15.setText("REPRESENTANTE LEGAL");

        jLabel13.setText("CORREO ELECTRÓNICO");

        jTCuentaNumeroDolares.setEditable(false);
        jTCuentaNumeroDolares.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTCuentaNumeroDolaresKeyPressed(evt);
            }
        });

        jButton5.setText("Busqueda avanzada");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jLabel11.setText("TELÉFONO");

        jLabel19.setText("BANCO");

        jButton3.setText("Busqueda avanzada");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jRBCA.setSelected(true);
        jRBCA.setText("Caja Ahorro");
        jRBCA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRBCAActionPerformed(evt);
            }
        });

        jTCuentaNumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTCuentaNumeroKeyPressed(evt);
            }
        });

        jLabel12.setText("TERMINAL DE POS");

        jTDocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTDocumentoKeyPressed(evt);
            }
        });

        jLabel20.setText("RAMO CABAL Y ANDA");

        jRBCC.setText("Cuenta Corr.");
        jRBCC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRBCCActionPerformed(evt);
            }
        });

        jRBPesos.setSelected(true);
        jRBPesos.setText("Pesos");
        jRBPesos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRBPesosActionPerformed(evt);
            }
        });

        jTTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTTelefonoKeyPressed(evt);
            }
        });

        jLabel17.setText("RAMO MASTER");

        jLabel18.setText("DOCUMENTO");

        jRBDolares.setText("Dolares");
        jRBDolares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRBDolaresActionPerformed(evt);
            }
        });

        jLabel9.setText("Titular cuenta:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jComboCabal, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton4)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addGap(123, 123, 123))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jComboMaster, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton5))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(jComboBancos, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton3))
                                    .addComponent(jLabel19))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jRBCA)
                                    .addComponent(jRBCC)))
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addComponent(jRBPesos)
                                .addGap(155, 155, 155)
                                .addComponent(jRBDolares)
                                .addGap(0, 163, Short.MAX_VALUE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTTitularCuenta)
                                    .addComponent(jTCuentaNumero, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addComponent(jTCuentaNumeroDolares, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(jTPos, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addComponent(jTMail, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jTRepresentanteLegal, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel18)
                            .addComponent(jTDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(0, 0, 0))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTPos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTRepresentanteLegal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addGap(26, 26, 26)))
                .addGap(31, 31, 31)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jRBCA)
                                .addComponent(jRBPesos))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jRBCC, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jRBDolares)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTCuentaNumeroDolares, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTCuentaNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jComboBancos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton3))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTTitularCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(5, 5, 5)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(jLabel20))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton4)
                        .addComponent(jComboCabal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jComboMaster, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanelDistintoDom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jRBDistintoDomic, javax.swing.GroupLayout.PREFERRED_SIZE, 616, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 34, Short.MAX_VALUE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jRBDistintoDomic)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelDistintoDom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jTNombreFantasia, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jTRUT, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jButton2)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jTRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTTipoRUT, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel3Layout.createSequentialGroup()
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTNombreFantasia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel3Layout.createSequentialGroup()
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTRUT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTTipoRUT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jBGenerar.setText("GENERAR FORMULARIO");
        jBGenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGenerarActionPerformed(evt);
            }
        });

        jRBMaster.setSelected(true);
        jRBMaster.setText("MASTER");
        jRBMaster.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRBMasterActionPerformed(evt);
            }
        });

        jRBCabal.setSelected(true);
        jRBCabal.setText("CABAL");

        jRBAnda.setText("ANDA");
        jRBAnda.setEnabled(false);

        jRBMasterMasVal.setText("MASTER + VAL");
        jRBMasterMasVal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRBMasterMasValActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jBGenerar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jRBCabal)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jRBMasterMasVal)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jRBMaster)
                        .addGap(18, 18, 18)
                        .addComponent(jRBAnda)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRBMaster)
                    .addComponent(jRBCabal)
                    .addComponent(jRBAnda))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jRBMasterMasVal)
                .addGap(7, 7, 7)
                .addComponent(jBGenerar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPanel1);

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jRBModif.setText("Modificación");
        jRBModif.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRBModifActionPerformed(evt);
            }
        });

        jRBAlta.setSelected(true);
        jRBAlta.setText("Alta");
        jRBAlta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRBAltaActionPerformed(evt);
            }
        });

        jLTipoLlamado.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLTipoLlamado.setForeground(new java.awt.Color(255, 0, 0));
        jLTipoLlamado.setText(" ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jRBAlta)
                .addGap(18, 18, 18)
                .addComponent(jRBModif)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLTipoLlamado)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRBAlta)
                    .addComponent(jRBModif)
                    .addComponent(jLTipoLlamado))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap(170, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(115, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jRBModifActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRBModifActionPerformed
        if (!jRBAlta.isSelected())
            jRBModif.setSelected(true);
        jRBAlta.setSelected(false);
    }//GEN-LAST:event_jRBModifActionPerformed

    private void jRBAltaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRBAltaActionPerformed
        if (!jRBModif.isSelected())
            jRBAlta.setSelected(true);
        jRBModif.setSelected(false);
    }//GEN-LAST:event_jRBAltaActionPerformed

    private void jBGenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGenerarActionPerformed
        if (!jRBCabal.isSelected() && !jRBMaster.isSelected() && !jRBAnda.isSelected() && !jRBMasterMasVal.isSelected())
            JOptionPane.showMessageDialog(this, "Debe seleccionar al menos un formulario para el cual generar datos");
        else{
            try{
                String altaOModificacion = "ALTA";
                if (jRBModif.isSelected())
                    altaOModificacion = "MODIFICACION";
                String nombreFantasia = jTNombreFantasia.getText();
                String razonSocial = jTRazonSocial.getText();
                String RUT = jTRUT.getText();
                String tipoRUT = jTTipoRUT.getText();
                String tipoRUTMaster = "";
                if (tipoRUT.equals("70"))
                    tipoRUTMaster = "CAT 0";
                else if (tipoRUT.equals("75"))
                    tipoRUTMaster = "CAT 1";
                else if (tipoRUT.equals("78") || tipoRUT.equals("71"))
                    tipoRUTMaster = "CAT 4";
                String cuentaNumeroPesos = jTCuentaNumero.getText();
                String cuentaNumeroDolares = "";
                String siDolares = "NO";
                if (jRBDolares.isSelected()){
                    cuentaNumeroDolares = jTCuentaNumeroDolares.getText();
                    siDolares = "SI";
                }
                String moneda = "PESOS";
                if (jRBPesos.isSelected() && jRBDolares.isSelected())
                    moneda = "PESOS Y DOLARES";
                else if (jRBDolares.isSelected())
                    moneda = "DOLARES";
                String tipoDePago = "ACREDITA EN CUENTA";
                String CACCMaster = "4";
                String CACCCabal = "CC - ";
                if (jRBCA.isSelected()){
                    CACCMaster = "5";
                    CACCCabal = "CA - ";
                }
                if (cuentaNumeroPesos.equals("")){
                    CACCCabal = "";
                    tipoDePago = "PERSONALIZADO";
                }

                String titularCuenta = jTTitularCuenta.getText();
                String entidadPagadora = "";
                String calle = jTCalle.getText();
                String numPuerta = jTNumeroPuerta.getText();
                String apto = jTApto.getText();
                String codCiudad = "1256";

                String codigoPostal = jTCodigoPostal.getText();
                String ciudad = "MONTEVIDEO";
                String departamento = "MONTEVIDEO";
                String localidadYDepartamento = "MONTEVIDEO";
                if (jRBInterior.isSelected()){
                    DTCiudad city = (DTCiudad)jComboCiudades.getSelectedItem();
                    if (city == null)
                        throw new noSeleccionoCiudadInteriorException();
                    codigoPostal = city.getCodigoPostal();
                    ciudad = city.getLocalidad();
                    departamento = city.getDepartamento();
                    localidadYDepartamento = ciudad + " (" + departamento + ")";
                    codCiudad = city.getCodigoCiudad();
                }
                String codPostalDepartamento = codigoPostal + " - "  + localidadYDepartamento;
                String grupo = "";
                String subgrupo = "";
                String telefono = jTTelefono.getText();
                String pos = jTPos.getText();
                String mail = jTMail.getText();
                String representanteLegal = jTRepresentanteLegal.getText();
                String personaContacto = representanteLegal;
                String documento = jTDocumento.getText();
                String callePago = "";
                String numPuertaPago = "";
                String aptoPago = "";
                String codPostalDepartamentoPago = "";
                if (jRBDistintoDomic.isSelected()){
                    callePago = jTCalle1.getText();
                    numPuertaPago = jTNumeroPuerta1.getText();
                    aptoPago = jTApto1.getText();
                    codPostalDepartamentoPago = jTCodigoPostal1.getText();
                }
                Calendar cal = Calendar.getInstance();
                String anio = String.valueOf(cal.get(Calendar.YEAR)).substring(2);
                String mes = String.valueOf(cal.get(Calendar.MONTH) + 1);
                String dia = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
                Date fecha = new Date();
                
                DTBanco dtB = (DTBanco)jComboBancos.getSelectedItem();
                if (dtB == null)
                    throw new noSeleccionoBancoException();
                String nombreBanco = dtB.getNombre();
                String entidadSucursal = dtB.getEntidadCabal() + " - " + nombreBanco;
                String sucursal = dtB.getSucursal();
                String entidad = dtB.getEntidadMaster();
                String descuentoCuota = "1.3";
                String entidadDolares = "";
                String sucursalDolares = "";
                String CACCMasterDolares = "";
                if (jRBDolares.isSelected()){
                     entidadDolares = entidad;
                     sucursalDolares = sucursal;
                     CACCMasterDolares = CACCMaster;
                }
                
                boolean val = jRBMasterMasVal.isSelected();
                
                //ANDA
                SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
                String fechaString = f.format(fecha);
                String usuario = jFMain.jBUsuario.getText();
                String cuentaCorrienteAnda = "";
                String cajaAhorroAnda = "";
                String banco = dtB.getNombre();
                String localidadBanco = "MONTEVIDEO";
                if (dtB instanceof DTBancoInterior)
                    localidadBanco = ((DTBancoInterior) dtB).getCiudad();
                if (jRBCC.isSelected())
                    cuentaCorrienteAnda = cuentaNumeroPesos;
                else
                    cajaAhorroAnda = cuentaNumeroPesos;
                /////
                
                String[] split = usuario.split(" ");
                String usuarioCabalYMaster = split[0];
                try{
                    usuarioCabalYMaster = split[0].substring(0,1);
                }catch(Exception e){}
                
                for (int i = 1; i < split.length; i++){
                    usuarioCabalYMaster = usuarioCabalYMaster + split[i].substring(0,1);
                }
                
                ramoCabal ramoCabal = (ramoCabal) jComboCabal.getSelectedItem();
                ramoMaster ramoMaster = (ramoMaster) jComboMaster.getSelectedItem();
                ramoAnda ramoAnda = null;
                if (jRBAnda.isSelected())
                    ramoAnda = listaRamosAnda.get(jComboCabal.getSelectedIndex());
                
                if (jRBCabal.isSelected()){
                    if (ramoCabal == null)
                        throw new noSeleccionoRamoCabalException();
                    String ramoPpal = ramoCabal.getNombre().toUpperCase();
                    String numEstablecimiento = ramoCabal.getCodigo().toUpperCase();
                    
                    DTFormularioCabal dtFormCabal = new DTFormularioCabal(-1, fecha, altaOModificacion, numEstablecimiento, 
                            nombreFantasia, razonSocial, RUT, tipoRUT, moneda, "SI", tipoDePago, entidadSucursal, sucursal,
                            CACCCabal + cuentaNumeroPesos, CACCCabal + cuentaNumeroDolares, calle, numPuerta, apto, codCiudad,
                            codPostalDepartamento, telefono, pos, representanteLegal, ramoPpal, personaContacto,
                            mail, callePago, numPuertaPago, aptoPago, "", codPostalDepartamentoPago, grupo, subgrupo,
                            "", ramoCabal.getDescuento().toUpperCase(), ramoCabal.getDescuentoCuota().toUpperCase(), usuarioCabalYMaster);
                    jIFFormCabal jifCabal = new jIFFormCabal(dtFormCabal);
                    agregarTab("Cabal - " + RUT, jifCabal);
                }
                if (jRBMaster.isSelected() || jRBMasterMasVal.isSelected()){
                    if (ramoMaster == null)
                        throw new noSeleccionoRamoMasterException();
                    DTFormularioMaster dtFormMaster = new DTFormularioMaster(-1, pos, entidad, sucursal, nombreBanco, "X", "X", "", dia, mes,
                            anio, nombreFantasia, razonSocial, calle, numPuerta, apto, codigoPostal, localidadYDepartamento, "BC - " + usuarioCabalYMaster ,
                            telefono, tipoRUTMaster, RUT, representanteLegal, "CI", documento, ramoMaster.getCodigo().toUpperCase(),
                            ramoMaster.getNombre().toUpperCase(), mail, CACCMaster, cuentaNumeroPesos, entidad, sucursal, "1", descuentoCuota, "S0001",
                            "Z0001", CACCMaster, cuentaNumeroPesos, entidad, sucursal, siDolares, CACCMasterDolares, cuentaNumeroDolares, entidadDolares, sucursalDolares, "PP", "15H", "CUOTAS",
                            ramoMaster.getDescuento().toUpperCase(), ramoMaster.getDescuentoCuota(), val);
                    jIFVistaFormFirstData jifMaster = new jIFVistaFormFirstData(dtFormMaster, val);
                    String name = "Master - ";
                    if (val)
                        name = "Master + VAL - ";
                    agregarTab(name + RUT, jifMaster);
                }
                if (jRBAnda.isSelected()){
                    if (ramoCabal == null)
                        throw new noSeleccionoRamoCabalException();
                    String ramoPpal = ramoCabal.getNombre().toUpperCase();
                    
                    DTFormularioAnda dtFormAnda = new DTFormularioAnda(-1, razonSocial, 
                            nombreFantasia, RUT, representanteLegal,documento, "DUEÑO", 
                            telefono, calle, numPuerta, apto, ciudad, departamento, 
                            "", mail, "IDEM", "", "", "", "", "", "", ramoPpal, "", pos,
                            fechaString, cuentaCorrienteAnda, cajaAhorroAnda, titularCuenta, banco, localidadBanco, sucursal, ciudad, 
                            usuario, ramoAnda.getDescuento(), ramoAnda.getDescuentoCuota());
                    jIFFormAnda jifAnda = new jIFFormAnda(dtFormAnda);
                    agregarTab("Anda - " + RUT, jifAnda);
                }
                if (dt != null)
                    wkGuardar(null);
                //DISPOSE
                tabPane.remove(this);
            }catch (Exception e){
                if (!(e instanceof noSeleccionoBancoException || 
                        e instanceof noSeleccionoCiudadInteriorException || 
                        e instanceof noSeleccionoRamoCabalException ||
                        e instanceof noSeleccionoRamoMasterException))
                    controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                JOptionPane.showMessageDialog(this, e.getMessage());
            }
        }
    }//GEN-LAST:event_jBGenerarActionPerformed

    private void agregarTab(String titulo, JInternalFrame ji) {
        tabPane.add(titulo, ji);
        tabPane.setTabComponentAt(tabPane.getTabCount()- 1,
                 new ButtonTabComponent(tabPane));
        if (jRBCabal.isSelected() && jRBMaster.isSelected() && jRBAnda.isSelected())
            tabPane.setSelectedIndex(tabPane.getTabCount()- 3);
        else if ((jRBCabal.isSelected() && jRBMaster.isSelected()) || (jRBCabal.isSelected() && jRBAnda.isSelected()) || (jRBMaster.isSelected() && jRBAnda.isSelected()))
            tabPane.setSelectedIndex(tabPane.getTabCount()- 2);
        else
            tabPane.setSelectedIndex(tabPane.getTabCount()- 1);
    }
    
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        jTRUT.setSelectionStart(0);
        jTRUT.setSelectionEnd(jTRUT.getText().length());
        jTRUT.copy();
        Desktop enlace=Desktop.getDesktop();
        try {
                enlace.browse(new URI("https://servicios.dgi.gub.uy/ServiciosEnLinea/dgi--servicios-en-linea--consulta-de-certifcado-unico"));
        } catch (IOException | URISyntaxException e) {
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jRBCAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRBCAActionPerformed
        jRBCC.setSelected(false);
    }//GEN-LAST:event_jRBCAActionPerformed

    private void jRBCCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRBCCActionPerformed
        jRBCA.setSelected(false);
    }//GEN-LAST:event_jRBCCActionPerformed

    private void jRBDistintoDomicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRBDistintoDomicActionPerformed
        if (jRBDistintoDomic.isSelected()){
            jPanelDistintoDom.setVisible(true);
        }else{
            jPanelDistintoDom.setVisible(false);
        }
    }//GEN-LAST:event_jRBDistintoDomicActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        jDBuscarBanco jd = new jDBuscarBanco(null, true);
        jd.setVisible(true);
        int i = jd.getIndice();
        if (i != -1)
            jComboBancos.setSelectedIndex(i);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        jDBuscarRamoCabal jd = new jDBuscarRamoCabal(null, true);
        jd.setVisible(true);
        int i = jd.getIndice();
        if (i != -1)
            jComboCabal.setSelectedIndex(i);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        jDBuscarRamoMaster jd = new jDBuscarRamoMaster(null, true);
        jd.setVisible(true);
        int i = jd.getIndice();
        if (i != -1)
            jComboMaster.setSelectedIndex(i);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jBBuscarCiudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBuscarCiudadActionPerformed
        jDBuscarCiudad jd = new jDBuscarCiudad(null, true);
        jd.setVisible(true);
        int i = jd.getIndice();
        if (i != -1)
            jComboCiudades.setSelectedIndex(i);
    }//GEN-LAST:event_jBBuscarCiudadActionPerformed

    private void jRBDolaresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRBDolaresActionPerformed
        if (!jRBPesos.isSelected())
            jRBDolares.setSelected(true);
        if (jRBDolares.isSelected())
            jTCuentaNumeroDolares.setEditable(true);
        else
            jTCuentaNumeroDolares.setEditable(false);
    }//GEN-LAST:event_jRBDolaresActionPerformed

    private void jRBPesosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRBPesosActionPerformed
        if (!jRBDolares.isSelected())
            jRBPesos.setSelected(true);
        if (jRBPesos.isSelected())
            jTCuentaNumero.setEditable(true);
        else
            jTCuentaNumero.setEditable(false);
    }//GEN-LAST:event_jRBPesosActionPerformed

    private void jTCuentaNumeroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTCuentaNumeroKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB)
            if (jRBDolares.isSelected())
                jTCuentaNumeroDolares.requestFocus();
            else
                jTTitularCuenta.requestFocus();
    }//GEN-LAST:event_jTCuentaNumeroKeyPressed

    private void jTAptoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTAptoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB){
            if (jRBMontevideo.isSelected())
                jTCodigoPostal.requestFocus();
            else if (jRBDistintoDomic.isSelected())
                jTCalle1.requestFocus();
            else
                jTTelefono.requestFocus();
        }
    }//GEN-LAST:event_jTAptoKeyPressed

    private void jTTelefonoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTTelefonoKeyPressed
        
    }//GEN-LAST:event_jTTelefonoKeyPressed

    private void jTCodigoPostalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTCodigoPostalKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB)
            if (jRBDistintoDomic.isSelected())
                jTCalle1.requestFocus();
            else
                jTTelefono.requestFocus();
    }//GEN-LAST:event_jTCodigoPostalKeyPressed

    private void jTTipoRUTKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTTipoRUTKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB)
                jTCalle.requestFocus();
    }//GEN-LAST:event_jTTipoRUTKeyPressed

    private void jTDocumentoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTDocumentoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB)
            jTCuentaNumero.requestFocus();
    }//GEN-LAST:event_jTDocumentoKeyPressed

    private void jRBInteriorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jRBInteriorItemStateChanged
        if (!jRBMontevideo.isSelected())
            jRBInterior.setSelected(true);
        jRBMontevideo.setSelected(false);
        jLLocalidad.setVisible(true);
        jComboCiudades.setVisible(true);
        jBBuscarCiudad.setVisible(true);
        jLCodPostal.setVisible(false);
        jTCodigoPostal.setVisible(false);
    }//GEN-LAST:event_jRBInteriorItemStateChanged

    private void jRBMontevideoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jRBMontevideoItemStateChanged
        if (!jRBInterior.isSelected())
            jRBMontevideo.setSelected(true);
        jRBInterior.setSelected(false);
        jLLocalidad.setVisible(false);
        jComboCiudades.setVisible(false);
        jBBuscarCiudad.setVisible(false);
        jLCodPostal.setVisible(true);
        jTCodigoPostal.setVisible(true);
    }//GEN-LAST:event_jRBMontevideoItemStateChanged

    private void jTCuentaNumeroDolaresKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTCuentaNumeroDolaresKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB)
            jTTitularCuenta.requestFocus();
    }//GEN-LAST:event_jTCuentaNumeroDolaresKeyPressed

    private void jRBMasterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRBMasterActionPerformed
        jRBMasterMasVal.setSelected(false);
    }//GEN-LAST:event_jRBMasterActionPerformed

    private void jRBMasterMasValActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRBMasterMasValActionPerformed
        jRBMaster.setSelected(false);
    }//GEN-LAST:event_jRBMasterMasValActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBBuscarCiudad;
    private javax.swing.JButton jBGenerar;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private org.jdesktop.swingx.JXComboBox jComboBancos;
    private org.jdesktop.swingx.JXComboBox jComboCabal;
    private org.jdesktop.swingx.JXComboBox jComboCiudades;
    private org.jdesktop.swingx.JXComboBox jComboMaster;
    private javax.swing.JLabel jLCodPostal;
    private javax.swing.JLabel jLLocalidad;
    private javax.swing.JLabel jLTipoLlamado;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanelDistintoDom;
    private javax.swing.JPanel jPanelMontevideo;
    private javax.swing.JRadioButton jRBAlta;
    private javax.swing.JRadioButton jRBAnda;
    private javax.swing.JRadioButton jRBCA;
    private javax.swing.JRadioButton jRBCC;
    private javax.swing.JRadioButton jRBCabal;
    private javax.swing.JRadioButton jRBDistintoDomic;
    private javax.swing.JRadioButton jRBDolares;
    private javax.swing.JRadioButton jRBInterior;
    private javax.swing.JRadioButton jRBMaster;
    private javax.swing.JRadioButton jRBMasterMasVal;
    private javax.swing.JRadioButton jRBModif;
    private javax.swing.JRadioButton jRBMontevideo;
    private javax.swing.JRadioButton jRBPesos;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTApto;
    private javax.swing.JTextField jTApto1;
    private javax.swing.JTextField jTCalle;
    private javax.swing.JTextField jTCalle1;
    private javax.swing.JTextField jTCodigoPostal;
    private javax.swing.JTextField jTCodigoPostal1;
    private javax.swing.JTextField jTCuentaNumero;
    private javax.swing.JTextField jTCuentaNumeroDolares;
    private javax.swing.JTextField jTDepartamento1;
    private javax.swing.JTextField jTDocumento;
    private javax.swing.JTextField jTMail;
    private javax.swing.JTextField jTNombreFantasia;
    private javax.swing.JTextField jTNumeroPuerta;
    private javax.swing.JTextField jTNumeroPuerta1;
    private javax.swing.JTextField jTPos;
    private javax.swing.JTextField jTRUT;
    private javax.swing.JTextField jTRazonSocial;
    private javax.swing.JTextField jTRepresentanteLegal;
    private javax.swing.JTextField jTTelefono;
    private javax.swing.JTextField jTTipoRUT;
    private javax.swing.JTextField jTTitularCuenta;
    // End of variables declaration//GEN-END:variables
}
