/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import javax.swing.JButton;

/**
 *
 * @author Javier
 */
public interface Formulario {
    public void listarRamos();
    public void listarSucursales();
    public void imprimir();
    public boolean guardar(); // Devuelve true si guarda correctamente, false si no 
    public void exportPDF();
    public void wkGuardar(JButton b);
    public void wkExportPDF(JButton b);
}
