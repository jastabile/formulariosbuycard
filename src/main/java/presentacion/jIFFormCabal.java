/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import controladores.Controlador;
import datatypes.DTBanco;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import datatypes.DTFormularioCabal;
import excepciones.BDException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import logica.ramoCabal;
import logica.workerExportar;
import logica.workerGuardar;

/**
 *
 * @author Javier
 */
public class jIFFormCabal extends javax.swing.JInternalFrame implements Printable, Formulario{

    private DTFormularioCabal dt;
    private DefaultTableModel modelPesos = new DefaultTableModel();
    private DefaultTableModel modelDolares = new DefaultTableModel();
    private ArrayList<ramoCabal> listaRamosCabal;
    private ArrayList<DTBanco> listaBancos;
    
    public jIFFormCabal(DTFormularioCabal dt) {
        initComponents();
        
        this.dt = dt;
        this.listaRamosCabal = null;
        this.listaBancos = null;
        
        cargarPreferencias();
        formatoTablaPesos();
        formatoTablaDolares();
        cargarDatos();
    }
    
    private void cargarPreferencias(){
        ((BasicInternalFrameUI)this.getUI()).setNorthPane(null);
        //Scroll vertical mas rapido
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(16);
    }
    
    private void cargarDatos(){
        //Fecha
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String fecha =  format.format(dt.getFecha());
        jTFecha.setText(fecha);
        jTABM.setText(dt.getAltaOModificacion());
        jTNumEstablecimiento.setText(dt.getNumEstablecimiento());
        jTNomFantasia.setText(dt.getNombreFantasia());
        jTRazonSocial.setText(dt.getRazonSocial());
        jTRUT.setText(dt.getRUT());
        jTTipoRUT.setText(dt.getTipoRUT());
        jTMoneda.setText(dt.getMoneda());
        jTUnifica.setText(dt.getUnificaCheques());
        jTTipoPago.setText(dt.getTipoDePago());
        jTEntidadSucursal.setText(dt.getEntidadSucursal());
        jTSucursal.setText(dt.getSucursal());
        jTCuentaNumero.setText(dt.getCuentaNumero());
        jTEntidadPagadora.setText(dt.getEntidadPagadora());
        jTCalle.setText(dt.getCalle());
        jTNumPuerta.setText(dt.getNumPuerta());
        jTApto.setText(dt.getApto());
        jTCiudad.setText(dt.getCiudad());
        jTCodigoPostalDepartamento.setText(dt.getCodPostalDepartamento());
        jTTelefono.setText(dt.getTelefono());
        jTPOS.setText(dt.getPos());
        jTRepresentanteLegal.setText(dt.getRepresentanteLegal());
        jTRamoPpal.setText(dt.getRamoPpal());
        jTPersonaContacto.setText(dt.getPersonaContacto());
        jTCorreo.setText(dt.getMail());
        jTCallePago.setText(dt.getCallePago());
        jTNumPuertaPago.setText(dt.getNumPuertaPago());
        jTAptoPago.setText(dt.getAptoPago());
        jTCiudadPago.setText(dt.getCiudadPago());
        jTCodigoPostalDepartamentoPago.setText(dt.getCodigoPostalDepartamentoPago());
        jTObservaciones.setText(dt.getObservaciones());
        jLUsuario.setText("<html>POR CABAL URUGUAY S.A. <br> 030 - " + dt.getUsuario() + "</html>");
    }

    private void formatoTablaPesos(){
        modelPesos.addColumn("");
        modelPesos.addColumn("");
        modelPesos.addColumn("");
        modelPesos.addColumn("");
        jTPesos.setModel(modelPesos);

        //Tamaño columnas
        jTPesos.getColumnModel().getColumn(0).setResizable(false);
        jTPesos.getColumnModel().getColumn(0).setPreferredWidth(190);
        jTPesos.getColumnModel().getColumn(1).setResizable(false);
        jTPesos.getColumnModel().getColumn(1).setPreferredWidth(192);
        jTPesos.getColumnModel().getColumn(2).setResizable(false);
        jTPesos.getColumnModel().getColumn(2).setPreferredWidth(193);
        jTPesos.getColumnModel().getColumn(3).setResizable(false);
        jTPesos.getColumnModel().getColumn(3).setPreferredWidth(55);
        
        //CENTRAR
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( (int)CENTER_ALIGNMENT );
        jTPesos.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
        jTPesos.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
        jTPesos.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
        jTPesos.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
        
        //MOSTRAR LINEAS HORIZONTALES Y VERTICALES
        jTPesos.setShowGrid(true);
        jTPesos.setShowVerticalLines(true);
        jTPesos.setShowGrid(true);
        jTPesos.setShowHorizontalLines(true);

        Object fila1[] = {"CONTADO", dt.getDescuento() + "% MÁS IVA","3 CICLOS (SEMANAS)","1"};
        Object fila2[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","5 CICLOS (SEMANAS)","2"};
        Object fila3[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","7 CICLOS (SEMANAS)","3"};
        Object fila4[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","9 CICLOS (SEMANAS)","4"};
        Object fila5[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","11 CICLOS (SEMANAS)","5"};
        Object fila6[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","13 CICLOS (SEMANAS)","6"};
        modelPesos.addRow(fila1);
        modelPesos.addRow(fila2);
        modelPesos.addRow(fila3);
        modelPesos.addRow(fila4);
        modelPesos.addRow(fila5);
        modelPesos.addRow(fila6);
    }
    
    private void formatoTablaDolares(){
        modelDolares.addColumn("");
        modelDolares.addColumn("");
        modelDolares.addColumn("");
        modelDolares.addColumn("");
        jTDolares.setModel(modelDolares);

        //Tamaño columnas
        jTDolares.getColumnModel().getColumn(0).setResizable(false);
        jTDolares.getColumnModel().getColumn(0).setPreferredWidth(190);
        jTDolares.getColumnModel().getColumn(1).setResizable(false);
        jTDolares.getColumnModel().getColumn(1).setPreferredWidth(192);
        jTDolares.getColumnModel().getColumn(2).setResizable(false);
        jTDolares.getColumnModel().getColumn(2).setPreferredWidth(193);
        jTDolares.getColumnModel().getColumn(3).setResizable(false);
        jTDolares.getColumnModel().getColumn(3).setPreferredWidth(55);
        
        //CENTRAR
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( (int)CENTER_ALIGNMENT );
        jTDolares.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
        jTDolares.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
        jTDolares.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
        jTDolares.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
        
        //MOSTRAR LINEAS HORIZONTALES Y VERTICALES
        jTDolares.setShowGrid(true);
        jTDolares.setShowVerticalLines(true);
        jTDolares.setShowGrid(true);
        jTDolares.setShowHorizontalLines(true);

    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jRadioButton1 = new javax.swing.JRadioButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPScroll = new javax.swing.JPanel();
        jPImpresion = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTFecha = new javax.swing.JTextField();
        jTABM = new javax.swing.JTextField();
        jTNumEstablecimiento = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTNomFantasia = new javax.swing.JTextField();
        jTRazonSocial = new javax.swing.JTextField();
        jTRUT = new javax.swing.JTextField();
        jTTipoRUT = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jTTipoPago = new javax.swing.JTextField();
        jTUnifica = new javax.swing.JTextField();
        jTMoneda = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTEntidadSucursal = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jTCuentaNumero = new javax.swing.JTextField();
        jTEntidadPagadora = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        jTSucursal = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jTNumPuerta = new javax.swing.JTextField();
        jTApto = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jTTelefono = new javax.swing.JTextField();
        jTPOS = new javax.swing.JTextField();
        jTCiudad = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jTCorreo = new javax.swing.JTextField();
        jTRamoPpal = new javax.swing.JTextField();
        jTPersonaContacto = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jTRepresentanteLegal = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        jTCalle = new javax.swing.JTextField();
        jTCodigoPostalDepartamento = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jTCallePago = new javax.swing.JTextField();
        jTNumPuertaPago = new javax.swing.JTextField();
        jTAptoPago = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jTCiudadPago = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jTGrupo = new javax.swing.JTextField();
        jTCodigoPostalDepartamentoPago = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jTSubgrupo = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jBAgregarPesos = new javax.swing.JButton();
        jBBorrarPesos = new javax.swing.JButton();
        jTPesos = new javax.swing.JTable();
        jTDolares = new javax.swing.JTable();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jBAgregarDolares = new javax.swing.JButton();
        jBBorrarDolares = new javax.swing.JButton();
        jLabel41 = new javax.swing.JLabel();
        jTObservaciones = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        jLabel43 = new javax.swing.JLabel();
        jLUsuario = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();

        jRadioButton1.setText("jRadioButton1");

        setBorder(null);
        setClosable(true);
        setIconifiable(true);
        setTitle("Cabal");

        jPScroll.setBackground(new java.awt.Color(255, 255, 255));

        jPImpresion.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/logo_cabal.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1)
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("DATOS GENERALES DEL COMERCIO"));

        jLabel2.setBackground(new java.awt.Color(23, 55, 93));
        jLabel2.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("FECHA");
        jLabel2.setOpaque(true);

        jTFecha.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTFecha.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTABM.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTABM.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTABM.setText("ALTA");

        jTNumEstablecimiento.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTNumEstablecimiento.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel3.setBackground(new java.awt.Color(23, 55, 93));
        jLabel3.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("ALTA - BAJA - MODIFICACIÓN");
        jLabel3.setOpaque(true);

        jLabel4.setBackground(new java.awt.Color(23, 55, 93));
        jLabel4.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("N° ESTABLECIMIENTO");
        jLabel4.setOpaque(true);

        jLabel5.setBackground(new java.awt.Color(23, 55, 93));
        jLabel5.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("NOMBRE FANTASÍA");
        jLabel5.setOpaque(true);

        jLabel6.setBackground(new java.awt.Color(23, 55, 93));
        jLabel6.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("RAZÓN SOCIAL");
        jLabel6.setOpaque(true);

        jLabel7.setBackground(new java.awt.Color(23, 55, 93));
        jLabel7.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("RUT");
        jLabel7.setOpaque(true);

        jLabel8.setBackground(new java.awt.Color(23, 55, 93));
        jLabel8.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("TIPO");
        jLabel8.setOpaque(true);

        jTNomFantasia.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTNomFantasia.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTRazonSocial.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTRazonSocial.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTRUT.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTRUT.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTTipoRUT.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTTipoRUT.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel9.setBackground(new java.awt.Color(23, 55, 93));
        jLabel9.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("MONEDA");
        jLabel9.setOpaque(true);

        jLabel10.setBackground(new java.awt.Color(23, 55, 93));
        jLabel10.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("UNIFICA CHEQUES");
        jLabel10.setOpaque(true);

        jLabel11.setBackground(new java.awt.Color(23, 55, 93));
        jLabel11.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("TIPO DE PAGO");
        jLabel11.setOpaque(true);

        jTTipoPago.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTTipoPago.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTUnifica.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTUnifica.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTMoneda.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTMoneda.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel12.setBackground(new java.awt.Color(23, 55, 93));
        jLabel12.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("ENTIDAD - SUCURSAL");
        jLabel12.setOpaque(true);

        jTEntidadSucursal.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTEntidadSucursal.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel16.setBackground(new java.awt.Color(23, 55, 93));
        jLabel16.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("CUENTA NÚMERO $");
        jLabel16.setOpaque(true);

        jLabel17.setBackground(new java.awt.Color(23, 55, 93));
        jLabel17.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("CUENTA NÚMERO U$S");
        jLabel17.setOpaque(true);

        jTCuentaNumero.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTCuentaNumero.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTEntidadPagadora.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTEntidadPagadora.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel48.setBackground(new java.awt.Color(23, 55, 93));
        jLabel48.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel48.setForeground(new java.awt.Color(255, 255, 255));
        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel48.setText("SUC");
        jLabel48.setOpaque(true);

        jTSucursal.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTSucursal.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTEntidadSucursal, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel48, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                            .addComponent(jTSucursal))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTCuentaNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTEntidadPagadora, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTNomFantasia, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTRUT, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jTTipoRUT, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTFecha, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTABM, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jTNumEstablecimiento, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTMoneda, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(6, 6, 6)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTUnifica, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTTipoPago, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTABM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTNumEstablecimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTNomFantasia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTRUT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTTipoRUT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTMoneda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTUnifica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTTipoPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17)
                    .addComponent(jLabel48))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTEntidadSucursal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTCuentaNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTEntidadPagadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTSucursal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("DATOS PARTICULARES DEL COMERCIO"));

        jLabel14.setBackground(new java.awt.Color(23, 55, 93));
        jLabel14.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("N° DE PUERTA");
        jLabel14.setOpaque(true);

        jLabel15.setBackground(new java.awt.Color(23, 55, 93));
        jLabel15.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("ALT. PTA - APTO.");
        jLabel15.setOpaque(true);

        jTNumPuerta.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTNumPuerta.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTApto.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTApto.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel19.setBackground(new java.awt.Color(23, 55, 93));
        jLabel19.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("TELÉFONO");
        jLabel19.setOpaque(true);

        jLabel20.setBackground(new java.awt.Color(23, 55, 93));
        jLabel20.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("TERMINAL DE POS");
        jLabel20.setOpaque(true);

        jLabel21.setBackground(new java.awt.Color(23, 55, 93));
        jLabel21.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("CIUDAD");
        jLabel21.setOpaque(true);

        jTTelefono.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTTelefono.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTPOS.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTPOS.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTCiudad.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTCiudad.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel22.setBackground(new java.awt.Color(23, 55, 93));
        jLabel22.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("CORREO ELECTRÓNICO");
        jLabel22.setOpaque(true);

        jLabel23.setBackground(new java.awt.Color(23, 55, 93));
        jLabel23.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("RAMO PRINCIPAL");
        jLabel23.setOpaque(true);

        jLabel24.setBackground(new java.awt.Color(23, 55, 93));
        jLabel24.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("PERSONA DE CONTÁCTO");
        jLabel24.setOpaque(true);

        jTCorreo.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTCorreo.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTRamoPpal.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTRamoPpal.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTPersonaContacto.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTPersonaContacto.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel25.setBackground(new java.awt.Color(23, 55, 93));
        jLabel25.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("REPRESENTANTE LEGAL");
        jLabel25.setOpaque(true);

        jTRepresentanteLegal.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTRepresentanteLegal.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel26.setBackground(new java.awt.Color(23, 55, 93));
        jLabel26.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel26.setText("CALLE");
        jLabel26.setOpaque(true);

        jTCalle.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTCalle.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTCodigoPostalDepartamento.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTCodigoPostalDepartamento.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel27.setBackground(new java.awt.Color(23, 55, 93));
        jLabel27.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("CODIGO POSTAL - DEPARTAMENTO");
        jLabel27.setOpaque(true);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTCodigoPostalDepartamento)
                                .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTTelefono)
                                .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                                .addComponent(jTPOS, javax.swing.GroupLayout.Alignment.LEADING)))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTCalle)
                                .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE))
                            .addGap(6, 6, 6)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTNumPuerta, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jTApto, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE)
                                .addComponent(jTRepresentanteLegal))
                            .addGap(6, 6, 6)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                                .addComponent(jTRamoPpal))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTPersonaContacto, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 858, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 858, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTCalle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(jLabel15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTNumPuerta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTApto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jLabel20)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTPOS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTCodigoPostalDepartamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTRepresentanteLegal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel24)
                            .addComponent(jLabel23))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTPersonaContacto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTRamoPpal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("DOMICILIO PARA EL PAGO AL COMERCIO"));

        jLabel13.setBackground(new java.awt.Color(23, 55, 93));
        jLabel13.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("CALLE");
        jLabel13.setOpaque(true);

        jLabel18.setBackground(new java.awt.Color(23, 55, 93));
        jLabel18.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setText("N° DE PUERTA");
        jLabel18.setOpaque(true);

        jLabel35.setBackground(new java.awt.Color(23, 55, 93));
        jLabel35.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(255, 255, 255));
        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel35.setText("ALT. PTA - APTO.");
        jLabel35.setOpaque(true);

        jTCallePago.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTCallePago.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTNumPuertaPago.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTNumPuertaPago.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTAptoPago.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTAptoPago.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel36.setBackground(new java.awt.Color(23, 55, 93));
        jLabel36.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(255, 255, 255));
        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel36.setText("CIUDAD");
        jLabel36.setOpaque(true);

        jTCiudadPago.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTCiudadPago.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel28.setBackground(new java.awt.Color(23, 55, 93));
        jLabel28.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("CODIGO POSTAL - DEPARTAMENTO");
        jLabel28.setOpaque(true);

        jLabel29.setBackground(new java.awt.Color(23, 55, 93));
        jLabel29.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(255, 255, 255));
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel29.setText("GRUPO");
        jLabel29.setOpaque(true);

        jTGrupo.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTGrupo.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jTCodigoPostalDepartamentoPago.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTCodigoPostalDepartamentoPago.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel30.setBackground(new java.awt.Color(23, 55, 93));
        jLabel30.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(255, 255, 255));
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel30.setText("SUBGRUPO");
        jLabel30.setOpaque(true);

        jTSubgrupo.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTSubgrupo.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTCodigoPostalDepartamentoPago, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                            .addComponent(jTGrupo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                            .addComponent(jTSubgrupo)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTCallePago)
                            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTNumPuertaPago, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel35, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTAptoPago, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTCiudadPago)
                            .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel18)
                    .addComponent(jLabel35)
                    .addComponent(jLabel36))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTCallePago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTNumPuertaPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTAptoPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTCiudadPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(jLabel28)
                    .addComponent(jLabel30))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTGrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTCodigoPostalDepartamentoPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTSubgrupo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("CONDICIONES GENERALES"));

        jLabel31.setBackground(new java.awt.Color(23, 55, 93));
        jLabel31.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(255, 255, 255));
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel31.setText("PLANES PESOS");
        jLabel31.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel31.setOpaque(true);

        jLabel32.setBackground(new java.awt.Color(23, 55, 93));
        jLabel32.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(255, 255, 255));
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel32.setText("ARANCEL DE DESCUENTO");
        jLabel32.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel32.setOpaque(true);

        jLabel33.setBackground(new java.awt.Color(23, 55, 93));
        jLabel33.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(255, 255, 255));
        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel33.setText("PLAZO DE PAGO");
        jLabel33.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel33.setOpaque(true);

        jLabel34.setBackground(new java.awt.Color(23, 55, 93));
        jLabel34.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(255, 255, 255));
        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel34.setText("CUOTAS");
        jLabel34.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel34.setOpaque(true);

        jBAgregarPesos.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jBAgregarPesos.setText("+");
        jBAgregarPesos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAgregarPesosActionPerformed(evt);
            }
        });

        jBBorrarPesos.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jBBorrarPesos.setText("-");
        jBBorrarPesos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBorrarPesosActionPerformed(evt);
            }
        });

        jTPesos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTPesos.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTPesos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTPesos.setOpaque(false);
        jTPesos.setRowHeight(20);

        jTDolares.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTDolares.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jTDolares.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTDolares.setOpaque(false);
        jTDolares.setRowHeight(20);

        jLabel37.setBackground(new java.awt.Color(23, 55, 93));
        jLabel37.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel37.setForeground(new java.awt.Color(255, 255, 255));
        jLabel37.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel37.setText("PLANES DOLARES");
        jLabel37.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel37.setOpaque(true);

        jLabel38.setBackground(new java.awt.Color(23, 55, 93));
        jLabel38.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel38.setForeground(new java.awt.Color(255, 255, 255));
        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel38.setText("ARANCEL DE DESCUENTO");
        jLabel38.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel38.setOpaque(true);

        jLabel39.setBackground(new java.awt.Color(23, 55, 93));
        jLabel39.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(255, 255, 255));
        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel39.setText("PLAZO DE PAGO");
        jLabel39.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel39.setOpaque(true);

        jLabel40.setBackground(new java.awt.Color(23, 55, 93));
        jLabel40.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(255, 255, 255));
        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel40.setText("CUOTAS");
        jLabel40.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel40.setOpaque(true);

        jBAgregarDolares.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jBAgregarDolares.setText("+");
        jBAgregarDolares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAgregarDolaresActionPerformed(evt);
            }
        });

        jBBorrarDolares.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jBBorrarDolares.setText("-");
        jBBorrarDolares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBorrarDolaresActionPerformed(evt);
            }
        });

        jLabel41.setBackground(new java.awt.Color(23, 55, 93));
        jLabel41.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel41.setForeground(new java.awt.Color(255, 255, 255));
        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel41.setText("OBSERVACIONES");
        jLabel41.setOpaque(true);

        jTObservaciones.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTDolares, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTObservaciones)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel40, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jBAgregarPesos, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBBorrarPesos, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jBAgregarDolares, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBBorrarDolares, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTPesos, javax.swing.GroupLayout.PREFERRED_SIZE, 858, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBAgregarPesos)
                    .addComponent(jBBorrarPesos))
                .addGap(9, 9, 9)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(jLabel32)
                    .addComponent(jLabel33)
                    .addComponent(jLabel34))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTPesos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBAgregarDolares)
                    .addComponent(jBBorrarDolares))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel37)
                    .addComponent(jLabel38)
                    .addComponent(jLabel39)
                    .addComponent(jLabel40))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTDolares, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel41)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTObservaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        if (jTPesos.getColumnModel().getColumnCount() > 0) {
            jTPesos.getColumnModel().getColumn(0).setResizable(false);
            jTPesos.getColumnModel().getColumn(0).setPreferredWidth(186);
            jTPesos.getColumnModel().getColumn(1).setResizable(false);
            jTPesos.getColumnModel().getColumn(1).setPreferredWidth(186);
            jTPesos.getColumnModel().getColumn(2).setResizable(false);
            jTPesos.getColumnModel().getColumn(2).setPreferredWidth(186);
            jTPesos.getColumnModel().getColumn(3).setResizable(false);
            jTPesos.getColumnModel().getColumn(3).setPreferredWidth(55);
        }
        if (jTDolares.getColumnModel().getColumnCount() > 0) {
            jTDolares.getColumnModel().getColumn(0).setResizable(false);
            jTDolares.getColumnModel().getColumn(0).setPreferredWidth(186);
            jTDolares.getColumnModel().getColumn(1).setResizable(false);
            jTDolares.getColumnModel().getColumn(1).setPreferredWidth(186);
            jTDolares.getColumnModel().getColumn(2).setResizable(false);
            jTDolares.getColumnModel().getColumn(2).setPreferredWidth(186);
            jTDolares.getColumnModel().getColumn(3).setResizable(false);
            jTDolares.getColumnModel().getColumn(3).setPreferredWidth(55);
        }

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        jLabel43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel43.setText("X.....................................................................");

        jLUsuario.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLUsuario.setText("<html>POR CABAL URUGUAY S.A. <br> 030 - </html>");

        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel42.setText("...................................................................................................");

        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel44.setText("...................................................................................................");

        jLabel46.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel46.setText("<html> FIRMA AUTORIZADA <br> POR EL COMERCIO </html>");

        jLabel47.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel47.setText("<html>ACLARACIÓN<br> Y CARGO</html>");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel47)
                    .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel42)
                    .addComponent(jLabel43)
                    .addComponent(jLabel44))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPImpresionLayout = new javax.swing.GroupLayout(jPImpresion);
        jPImpresion.setLayout(jPImpresionLayout);
        jPImpresionLayout.setHorizontalGroup(
            jPImpresionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPImpresionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPImpresionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPImpresionLayout.setVerticalGroup(
            jPImpresionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPImpresionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(42, 42, 42)
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPScrollLayout = new javax.swing.GroupLayout(jPScroll);
        jPScroll.setLayout(jPScrollLayout);
        jPScrollLayout.setHorizontalGroup(
            jPScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPScrollLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPImpresion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPScrollLayout.setVerticalGroup(
            jPScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPScrollLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jPImpresion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jScrollPane1.setViewportView(jPScroll);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 726, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 505, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    @Override
    public void listarRamos(){
        if (listaRamosCabal == null)
            listaRamosCabal = Controlador.getInstance().listarRamosCabal();
        jDBuscarRamoCabal jd = new jDBuscarRamoCabal(null, true);
        jd.setVisible(true);
        int i = jd.getIndice();
        if (i != -1){
            ramoCabal dt = listaRamosCabal.get(i);
            jTRamoPpal.setText(dt.getNombre().toUpperCase());
            jTNumEstablecimiento.setText(dt.getCodigo().toUpperCase());
            for (int j = 0; j < jTPesos.getRowCount(); j++) {
                if (jTPesos.getValueAt(j, 0).equals("CONTADO"))
                    jTPesos.setValueAt(dt.getDescuento() + "% MAS IVA", j, 1);
                else if (jTPesos.getValueAt(j, 0).equals("CUOTAS SIN RECARGO"))
                    jTPesos.setValueAt(dt.getDescuentoCuota()+ "% MAS IVA", j, 1);
            }
            for (int j = 0; j < jTDolares.getRowCount(); j++) {
                if (jTDolares.getValueAt(j, 0).equals("CONTADO"))
                    jTDolares.setValueAt(dt.getDescuento() + "% MAS IVA", j, 1);
                else if (jTDolares.getValueAt(j, 0).equals("CUOTAS SIN RECARGO"))
                    jTDolares.setValueAt(dt.getDescuentoCuota()+ "% MAS IVA", j, 1);
            }
            this.dt.setDescuento(dt.getDescuento());
            this.dt.setDescuentoCuota(dt.getDescuentoCuota());
        }
    }
    
    @Override
    public void listarSucursales(){
        if (listaBancos == null)
            listaBancos = Controlador.getInstance().listarBancos();
        jDBuscarBanco jd =  new jDBuscarBanco(null, true);
        jd.setVisible(true);
        int i = jd.getIndice();
        if (i != -1){
            DTBanco dt = listaBancos.get(i);
            jTEntidadSucursal.setText(dt.getEntidadCabal() + " - " + dt.getNombre());
            jTSucursal.setText(dt.getSucursal());
        }
    }
    
    private void ocultarBotonesTablas(){
        jBAgregarDolares.setVisible(false);
            jBAgregarPesos.setVisible(false);
            jBBorrarDolares.setVisible(false);
            jBBorrarPesos.setVisible(false);
    }
    
    private void mostrarBotonesTablas(){
        jBAgregarDolares.setVisible(true);
            jBAgregarPesos.setVisible(true);
            jBBorrarDolares.setVisible(true);
            jBBorrarPesos.setVisible(true);
    }
    
    @Override
    public void wkGuardar(JButton boton){
        workerGuardar wk = new workerGuardar(this, boton);
        wk.execute();
    }
    
    @Override
    public void wkExportPDF(JButton boton){
        workerExportar wk = new workerExportar(this, boton);
        wk.execute();
    }
    
    @Override
    public void imprimir(){
        try {
            ocultarBotonesTablas();
            PrinterJob job = PrinterJob.getPrinterJob();
//            PageFormat preformat = getMinimumMarginPageFormat(job);
            job.setPrintable(this);
            if (job.printDialog()){
                job.print();
            }
            mostrarBotonesTablas();
        } catch (PrinterException ex) { 
            System.out.println(ex.getMessage());
        }
    }
    
    private DTFormularioCabal obtenerDatosFormulario(){
        DTFormularioCabal dtRetorno = null;
        try{
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date fecha = format.parse(jTFecha.getText());
            String altaOModificacion =  jTABM.getText();
            String numEstablecimiento = jTNumEstablecimiento.getText();
            String nombreFantasia = jTNomFantasia.getText();
            String razonSocial = jTRazonSocial.getText();
            String RUT = jTRUT.getText();
            String tipoRUT = jTTipoRUT.getText();
            String moneda = jTMoneda.getText();
            String unificaCheques = jTUnifica.getText();
            String tipoDePago = jTTipoPago.getText();
            String entidadSucursal = jTEntidadSucursal.getText();
            String sucursal = jTSucursal.getText();
            String cuentaNumero = jTCuentaNumero.getText();
            String entidadPagadora = jTEntidadPagadora.getText();
            String calle = jTCalle.getText();
            String numPuerta = jTNumPuerta.getText();
            String apto = jTApto.getText();
            String ciudad = jTCiudad.getText();
            String codPostalDepartamento = jTCodigoPostalDepartamento.getText();
            String telefono = jTTelefono.getText();
            String pos = jTPOS.getText();
            String representanteLegal = jTRepresentanteLegal.getText();
            String ramoPpal = jTRamoPpal.getText();
            String personaContacto= jTPersonaContacto.getText();
            String mail = jTCorreo.getText();
            String callePago = jTCallePago.getText();
            String numPuertaPago = jTNumPuertaPago.getText();
            String aptoPago = jTAptoPago.getText();
            String ciudadPago = jTCiudadPago.getText();
            String codigoPostalDepartamentoPago = jTCodigoPostalDepartamentoPago.getText();
            String grupo = jTGrupo.getText();
            String subgrupo = jTSubgrupo.getText();
            String observaciones = jTObservaciones.getText();
            String descuento = this.dt.getDescuento();
            String descuentoCuota = this.dt.getDescuentoCuota();
            String usuario = jLUsuario.getText().substring(40);
            dtRetorno = new DTFormularioCabal(this.dt.getId(),fecha, altaOModificacion, numEstablecimiento, nombreFantasia, 
                    razonSocial, RUT, tipoRUT, moneda, unificaCheques, tipoDePago, entidadSucursal, sucursal, cuentaNumero, entidadPagadora, 
                    calle, numPuerta, apto, ciudad, codPostalDepartamento, telefono, pos, representanteLegal, ramoPpal, 
                    personaContacto, mail, callePago, numPuertaPago, aptoPago, ciudadPago, codigoPostalDepartamentoPago,
                    grupo, subgrupo, observaciones,descuento,descuentoCuota, usuario);
        }catch (ParseException e){
            JOptionPane.showMessageDialog(null, "El formato de la fecha debe ser DD/MM/AAAA");
        }
        return dtRetorno;
    }
    
    @Override
    public boolean guardar(){
        boolean exito = false;
        try{
            DTFormularioCabal dtGuardar = obtenerDatosFormulario();
            if (this.dt.getId() == -1){
                int id = Controlador.insertarFormCabal(dtGuardar);
                dtGuardar.setId(id);
                this.dt = dtGuardar;
            }else{
                Controlador.modificarFormCabal(dtGuardar);
            }
            exito = true;
        }catch(BDException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return exito;
    }
    
    @Override
    public void exportPDF(){
        try{
            ocultarBotonesTablas();
            BufferedImage img = createImage(jPImpresion);
            String userDir = System.getProperty("user.home");
            JFileChooser chooser = new JFileChooser(userDir +"/Desktop");
            String extension = ".pdf";
            if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION){
                if (chooser.getSelectedFile().getPath().endsWith(".pdf"))
                    extension = "";
                FileOutputStream outputfile = new FileOutputStream(chooser.getSelectedFile().getPath()+ extension);
                Document d = new Document(PageSize.A4);
                PdfWriter writer = PdfWriter.getInstance(d, outputfile);
                d.open();
                Image itextImage = Image.getInstance(writer,img,1);
                itextImage.scaleToFit(PageSize.A4);
                itextImage.setAbsolutePosition(5,0);
                d.add(itextImage);
                d.close();
            }
            mostrarBotonesTablas();
        }catch(IOException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }catch(DocumentException de){
            System.out.println(de.getMessage());
        }
    }
    
    private void jBAgregarPesosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAgregarPesosActionPerformed
        if (jTPesos.getRowCount() == 0){
            //Agrego 6 filas
            Object fila1[] = {"CONTADO",dt.getDescuento() + "% MAS IVA","3 CICLOS (SEMANAS)","1"};
            Object fila2[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","5 CICLOS (SEMANAS)","2"};
            Object fila3[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","7 CICLOS (SEMANAS)","3"};
            Object fila4[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","9 CICLOS (SEMANAS)","4"};
            Object fila5[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","11 CICLOS (SEMANAS)","5"};
            Object fila6[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","13 CICLOS (SEMANAS)","6"};
            modelPesos.addRow(fila1);
            modelPesos.addRow(fila2);
            modelPesos.addRow(fila3);
            modelPesos.addRow(fila4);
            modelPesos.addRow(fila5);
            modelPesos.addRow(fila6);
        }else{
            //Agrego de a una fila
            Object row[] = {"","","",""};
            modelPesos.addRow(row);
        }
    }//GEN-LAST:event_jBAgregarPesosActionPerformed

    private void jBBorrarPesosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBorrarPesosActionPerformed
        if (jTPesos.getRowCount() > 0){
            //Borro de a una fila
            int fila = jTPesos.getSelectedRow();
            if (fila != -1)                                        //Si no selecciona fila, borro la ultima
                modelPesos.removeRow(fila);
            else
                modelPesos.removeRow(modelPesos.getRowCount() - 1);
        }
    }//GEN-LAST:event_jBBorrarPesosActionPerformed

    private void jBAgregarDolaresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAgregarDolaresActionPerformed
        if (jTDolares.getRowCount() == 0){
            //Agrego 6 filas
            Object fila1[] = {"CONTADO",dt.getDescuentoCuota() + "% MÁS IVA","3 CICLOS (SEMANAS)","1"};
            Object fila2[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","5 CICLOS (SEMANAS)","2"};
            Object fila3[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","7 CICLOS (SEMANAS)","3"};
            Object fila4[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","9 CICLOS (SEMANAS)","4"};
            Object fila5[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","11 CICLOS (SEMANAS)","5"};
            Object fila6[] = {"CUOTAS SIN RECARGO",dt.getDescuentoCuota() + "% MÁS IVA","13 CICLOS (SEMANAS)","6"};
            modelDolares.addRow(fila1);
            modelDolares.addRow(fila2);
            modelDolares.addRow(fila3);
            modelDolares.addRow(fila4);
            modelDolares.addRow(fila5);
            modelDolares.addRow(fila6);
            
        }else{
            Object row[] = {"","","",""};
            modelDolares.addRow(row);
        }
    }//GEN-LAST:event_jBAgregarDolaresActionPerformed

    private void jBBorrarDolaresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBorrarDolaresActionPerformed
        if (jTDolares.getRowCount() > 0){
            int fila = jTDolares.getSelectedRow();
            if (fila != -1)
                modelDolares.removeRow(fila);
            else
                modelDolares.removeRow(modelDolares.getRowCount() - 1);
        }
    }//GEN-LAST:event_jBBorrarDolaresActionPerformed

    private BufferedImage createImage(JPanel panel){
        BufferedImage bi = new BufferedImage((int)(panel.getWidth()), (int)(panel.getHeight()), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bi.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
//        g2d.setTransform(AffineTransform.getScaleInstance(1, 1));
        panel.print(g2d);
        return bi;
    
    }
    
    
    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
//        if (pageIndex > 0) 
//            return NO_SUCH_PAGE;
//        
//        Graphics2D g2d = (Graphics2D)graphics;
//        g2d.scale(pageFormat.getImageableWidth()/jPImpresion.getWidth(),pageFormat.getImageableHeight()/jPImpresion.getHeight());
//        
//        jPImpresion.print(g2d);
//        return PAGE_EXISTS;
        
        BufferedImage img = createImage(jPImpresion);
        if (pageIndex == 0) {
            int pWidth = 0;
            int pHeight = 0;

            pHeight = (int) Math.min(pageFormat.getImageableWidth(), (double) img.getHeight());
            pHeight = pHeight - 25;
            pWidth = pHeight * img.getWidth() / img.getHeight();
            graphics.drawImage(img, (int) pageFormat.getImageableX() + 25, (int) pageFormat.getImageableY(), (int)(pWidth + pWidth * 0.37), (int)(pHeight + pHeight * 0.37), null);
            return PAGE_EXISTS;
        } else {
            return NO_SUCH_PAGE;
        }

        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBAgregarDolares;
    private javax.swing.JButton jBAgregarPesos;
    private javax.swing.JButton jBBorrarDolares;
    private javax.swing.JButton jBBorrarPesos;
    private javax.swing.JLabel jLUsuario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPImpresion;
    private javax.swing.JPanel jPScroll;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTABM;
    private javax.swing.JTextField jTApto;
    private javax.swing.JTextField jTAptoPago;
    private javax.swing.JTextField jTCalle;
    private javax.swing.JTextField jTCallePago;
    private javax.swing.JTextField jTCiudad;
    private javax.swing.JTextField jTCiudadPago;
    private javax.swing.JTextField jTCodigoPostalDepartamento;
    private javax.swing.JTextField jTCodigoPostalDepartamentoPago;
    private javax.swing.JTextField jTCorreo;
    private javax.swing.JTextField jTCuentaNumero;
    private javax.swing.JTable jTDolares;
    private javax.swing.JTextField jTEntidadPagadora;
    private javax.swing.JTextField jTEntidadSucursal;
    private javax.swing.JTextField jTFecha;
    private javax.swing.JTextField jTGrupo;
    private javax.swing.JTextField jTMoneda;
    private javax.swing.JTextField jTNomFantasia;
    private javax.swing.JTextField jTNumEstablecimiento;
    private javax.swing.JTextField jTNumPuerta;
    private javax.swing.JTextField jTNumPuertaPago;
    private javax.swing.JTextField jTObservaciones;
    private javax.swing.JTextField jTPOS;
    private javax.swing.JTextField jTPersonaContacto;
    private javax.swing.JTable jTPesos;
    private javax.swing.JTextField jTRUT;
    private javax.swing.JTextField jTRamoPpal;
    private javax.swing.JTextField jTRazonSocial;
    private javax.swing.JTextField jTRepresentanteLegal;
    private javax.swing.JTextField jTSubgrupo;
    private javax.swing.JTextField jTSucursal;
    private javax.swing.JTextField jTTelefono;
    private javax.swing.JTextField jTTipoPago;
    private javax.swing.JTextField jTTipoRUT;
    private javax.swing.JTextField jTUnifica;
    // End of variables declaration//GEN-END:variables


}
