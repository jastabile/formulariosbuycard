/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import controladores.Controlador;
import datatypes.DTBancoInterior;
import datatypes.DTBancoMontevideo;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Javier
 */
public class jDBuscarBanco extends javax.swing.JDialog {

    DefaultTableModel modelInterior;
    DefaultTableModel modelMontevideo;
    TableRowSorter<DefaultTableModel> filterInterior;
    TableRowSorter<DefaultTableModel> filterMontevideo;
    private int indice;
    Controlador ctrl;
    
    public jDBuscarBanco(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.ctrl = Controlador.getInstance();
        centrar();
        cargarTabla();
        indice = -1;
    }
    
    private void cargarTabla(){
        modelInterior = new DefaultTableModel(){
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };
        modelMontevideo = new DefaultTableModel(){
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };
        filterInterior = new TableRowSorter(modelInterior);
        filterMontevideo = new TableRowSorter(modelMontevideo);
        jTableInterior.setModel(modelInterior);
        jTableMontevideo.setModel(modelMontevideo);
        jTableInterior.setRowSorter(filterInterior);
        jTableMontevideo.setRowSorter(filterMontevideo);
        ArrayList<DTBancoInterior> listaInterior = ctrl.listarBancosInterior();
        ArrayList<DTBancoMontevideo> listaMontevideo = ctrl.listarBancosMontevideo();
        
        modelInterior.addColumn("DIRECCION");
        modelInterior.addColumn("BANCO");
        modelInterior.addColumn("CIUDAD");
        modelInterior.addColumn("SUCURSAL");
        for (DTBancoInterior dt: listaInterior){
            Object fila[] = {dt.getDireccion(), dt.getNombre(), dt.getCiudad(), dt.getSucursal()};
            modelInterior.addRow(fila);
        }
        
        modelMontevideo.addColumn("ZONA");
        modelMontevideo.addColumn("BANCO");
        modelMontevideo.addColumn("NOMBRE SUCURSAL");
        modelMontevideo.addColumn("DIRECCION");
        modelMontevideo.addColumn("SUCURSAL");
        for (DTBancoMontevideo dt: listaMontevideo){
            Object fila[] = {dt.getZona(), dt.getNombre(), dt.getNombreSucursal(), dt.getDireccion(), dt.getSucursal()};
            modelMontevideo.addRow(fila);
        }
    }

    public int getIndice() {
        return indice;
    }

    private void setIndice(int indice) {
        this.indice = indice;
    }
    
    private void centrar(){
        this.setLocationRelativeTo(null);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTMontevideo = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableMontevideo = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTInterior = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableInterior = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel2.setText("Filtrar:");

        jTMontevideo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTMontevideoKeyReleased(evt);
            }
        });

        jTableMontevideo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableMontevideoMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTableMontevideo);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 721, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTMontevideo)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTMontevideo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Sucursales Montevideo", jPanel2);

        jLabel1.setText("Filtrar:");

        jTInterior.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTInteriorKeyReleased(evt);
            }
        });

        jTableInterior.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableInteriorMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableInterior);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 721, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTInterior)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTInterior, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Sucursales interior", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTInteriorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTInteriorKeyReleased
        filterInterior.setRowFilter(javax.swing.RowFilter.regexFilter("(?i)" + jTInterior.getText()));
    }//GEN-LAST:event_jTInteriorKeyReleased

    private void jTableInteriorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableInteriorMouseClicked
        if (evt.getClickCount() == 2){
            setIndice(110 + jTableInterior.getRowSorter().convertRowIndexToModel(jTableInterior.getSelectedRow()));
            this.dispose();
        }        
    }//GEN-LAST:event_jTableInteriorMouseClicked

    private void jTMontevideoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTMontevideoKeyReleased
        filterMontevideo.setRowFilter(javax.swing.RowFilter.regexFilter("(?i)" + jTMontevideo.getText()));
    }//GEN-LAST:event_jTMontevideoKeyReleased

    private void jTableMontevideoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableMontevideoMouseClicked
        if (evt.getClickCount() == 2){
            setIndice(jTableMontevideo.getRowSorter().convertRowIndexToModel(jTableMontevideo.getSelectedRow()));
            this.dispose();
        } 
    }//GEN-LAST:event_jTableMontevideoMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTInterior;
    private javax.swing.JTextField jTMontevideo;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableInterior;
    private javax.swing.JTable jTableMontevideo;
    // End of variables declaration//GEN-END:variables
}
