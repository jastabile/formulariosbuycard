/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import cargaDatos.cargaDatos;
import components.ButtonTabComponent;
import controladores.Controlador;
import controladores.ControladorExceptions;
import excepciones.BDException;
import excepciones.noSeleccionoElementoException;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Javier
 */
public class jFMain extends javax.swing.JFrame {

    private int VERSION = 9;
    private Formulario form = null;
    private String hostname = "";
    
    public jFMain() {
        windowsLookAndFeel();
        checkAndUpdateVersion();
        initComponents();
        new cargaDatos();
        cargarRamos();
        cargarPreferencias();
        jBBorrarImport.setVisible(false);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPMain = new javax.swing.JPanel();
        jBAbrir = new javax.swing.JButton();
        jBNuevo = new javax.swing.JButton();
        jBLlamados = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jBGuardarLlamado = new javax.swing.JButton();
        jPForms = new javax.swing.JPanel();
        jBGuardarForm = new javax.swing.JButton();
        jBImprimir = new javax.swing.JButton();
        jBSucursal = new javax.swing.JButton();
        jBRamos = new javax.swing.JButton();
        jBExportPDF = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JSeparator();
        jPOpen = new javax.swing.JPanel();
        jBModificar = new javax.swing.JButton();
        jBBorrar = new javax.swing.JButton();
        jBRecargar = new javax.swing.JButton();
        jPTabPane = new javax.swing.JPanel();
        tabPane = new javax.swing.JTabbedPane();
        jPImport = new javax.swing.JPanel();
        jBCargar = new javax.swing.JButton();
        jBBorrarImport = new javax.swing.JButton();
        jBVerde = new javax.swing.JButton();
        jBAmarillo = new javax.swing.JButton();
        jBRojo = new javax.swing.JButton();
        jBImportar = new javax.swing.JButton();
        jBExportExcel = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jBBlanco = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jButton2 = new javax.swing.JButton();
        jPBottom = new javax.swing.JPanel();
        jBUsuario = new javax.swing.JButton();
        jLVersion = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        jBAbrir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Open_30.png"))); // NOI18N
        jBAbrir.setText("Abrir");
        jBAbrir.setBorder(null);
        jBAbrir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBAbrir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBAbrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAbrirActionPerformed(evt);
            }
        });

        jBNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/document_new_32.png"))); // NOI18N
        jBNuevo.setText("Nuevo");
        jBNuevo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBNuevo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBNuevoActionPerformed(evt);
            }
        });

        jBLlamados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Phone-30.png"))); // NOI18N
        jBLlamados.setText("LLamados");
        jBLlamados.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBLlamados.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBLlamados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBLlamadosActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jBGuardarLlamado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Save (2).png"))); // NOI18N
        jBGuardarLlamado.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jBGuardarLlamadoFocusLost(evt);
            }
        });
        jBGuardarLlamado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGuardarLlamadoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPMainLayout = new javax.swing.GroupLayout(jPMain);
        jPMain.setLayout(jPMainLayout);
        jPMainLayout.setHorizontalGroup(
            jPMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPMainLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jBNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBAbrir, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBLlamados)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBGuardarLlamado, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPMainLayout.setVerticalGroup(
            jPMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPMainLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBGuardarLlamado, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jBNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jBAbrir, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jBLlamados, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        jBGuardarForm.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Save (2).png"))); // NOI18N
        jBGuardarForm.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jBGuardarFormFocusLost(evt);
            }
        });
        jBGuardarForm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGuardarFormActionPerformed(evt);
            }
        });

        jBImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/printer.png"))); // NOI18N
        jBImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBImprimirActionPerformed(evt);
            }
        });

        jBSucursal.setText("SUCURSAL");
        jBSucursal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBSucursalActionPerformed(evt);
            }
        });

        jBRamos.setText("RAMOS");
        jBRamos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBRamosActionPerformed(evt);
            }
        });

        jBExportPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/PDF 2-48.png"))); // NOI18N
        jBExportPDF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExportPDFActionPerformed(evt);
            }
        });

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPFormsLayout = new javax.swing.GroupLayout(jPForms);
        jPForms.setLayout(jPFormsLayout);
        jPFormsLayout.setHorizontalGroup(
            jPFormsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormsLayout.createSequentialGroup()
                .addComponent(jBRamos, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBSucursal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBGuardarForm, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBExportPDF, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPFormsLayout.setVerticalGroup(
            jPFormsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFormsLayout.createSequentialGroup()
                .addGroup(jPFormsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBRamos, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBSucursal, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBGuardarForm, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBExportPDF, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jBModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Edit-30.png"))); // NOI18N
        jBModificar.setText("Cargar");
        jBModificar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBModificar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBModificarActionPerformed(evt);
            }
        });

        jBBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Delete-30.png"))); // NOI18N
        jBBorrar.setText("Eliminar");
        jBBorrar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBBorrar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBorrarActionPerformed(evt);
            }
        });

        jBRecargar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Synchronize-26.png"))); // NOI18N
        jBRecargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBRecargarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPOpenLayout = new javax.swing.GroupLayout(jPOpen);
        jPOpen.setLayout(jPOpenLayout);
        jPOpenLayout.setHorizontalGroup(
            jPOpenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPOpenLayout.createSequentialGroup()
                .addComponent(jBModificar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBBorrar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jBRecargar))
        );
        jPOpenLayout.setVerticalGroup(
            jPOpenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jBRecargar, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jBModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jBBorrar, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        tabPane.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentRemoved(java.awt.event.ContainerEvent evt) {
                tabPaneComponentRemoved(evt);
            }
        });
        tabPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tabPaneStateChanged(evt);
            }
        });
        tabPane.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tabPaneMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jPTabPaneLayout = new javax.swing.GroupLayout(jPTabPane);
        jPTabPane.setLayout(jPTabPaneLayout);
        jPTabPaneLayout.setHorizontalGroup(
            jPTabPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabPane, javax.swing.GroupLayout.DEFAULT_SIZE, 928, Short.MAX_VALUE)
        );
        jPTabPaneLayout.setVerticalGroup(
            jPTabPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPTabPaneLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(tabPane, javax.swing.GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE))
        );

        jBCargar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Edit-30.png"))); // NOI18N
        jBCargar.setText("Cargar");
        jBCargar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBCargar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBCargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBCargarActionPerformed(evt);
            }
        });

        jBBorrarImport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Delete-30.png"))); // NOI18N
        jBBorrarImport.setText("Eliminar");
        jBBorrarImport.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBBorrarImport.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBBorrarImport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBorrarImportActionPerformed(evt);
            }
        });

        jBVerde.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/circulo-verde.png"))); // NOI18N
        jBVerde.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBVerdeActionPerformed(evt);
            }
        });

        jBAmarillo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/circulo-amarillo.png"))); // NOI18N
        jBAmarillo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBAmarilloActionPerformed(evt);
            }
        });

        jBRojo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/circulo-rojo.png"))); // NOI18N
        jBRojo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBRojoActionPerformed(evt);
            }
        });

        jBImportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Import.png"))); // NOI18N
        jBImportar.setText("Importar");
        jBImportar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBImportar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBImportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBImportarActionPerformed(evt);
            }
        });

        jBExportExcel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Export-30.png"))); // NOI18N
        jBExportExcel.setText("Exportar");
        jBExportExcel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jBExportExcel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jBExportExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBExportExcelActionPerformed(evt);
            }
        });

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jBBlanco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/circulo_blanco_48.png"))); // NOI18N
        jBBlanco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBBlancoActionPerformed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Synchronize_30.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPImportLayout = new javax.swing.GroupLayout(jPImport);
        jPImport.setLayout(jPImportLayout);
        jPImportLayout.setHorizontalGroup(
            jPImportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPImportLayout.createSequentialGroup()
                .addComponent(jBCargar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBBorrarImport, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBImportar, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBExportExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBBlanco, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBVerde, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBAmarillo, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBRojo, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 291, Short.MAX_VALUE)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPImportLayout.setVerticalGroup(
            jPImportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPImportLayout.createSequentialGroup()
                .addGroup(jPImportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jBBorrarImport, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBCargar, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBImportar, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBExportExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBVerde, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBAmarillo, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBRojo, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator2)
                    .addComponent(jBBlanco, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator3)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jBUsuario.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jBUsuario.setBorder(null);
        jBUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBUsuarioActionPerformed(evt);
            }
        });

        jLVersion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLVersion.setText("Version: ");

        javax.swing.GroupLayout jPBottomLayout = new javax.swing.GroupLayout(jPBottom);
        jPBottom.setLayout(jPBottomLayout);
        jPBottomLayout.setHorizontalGroup(
            jPBottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPBottomLayout.createSequentialGroup()
                .addComponent(jLVersion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 879, Short.MAX_VALUE)
                .addComponent(jBUsuario))
        );
        jPBottomLayout.setVerticalGroup(
            jPBottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jBUsuario)
            .addComponent(jLVersion)
        );

        jMenu1.setText("Archivo");

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/new doc chico.png"))); // NOI18N
        jMenuItem1.setText("Nuevo");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Open Folder-22.png"))); // NOI18N
        jMenuItem2.setText("Cargar");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Phone-22.png"))); // NOI18N
        jMenuItem3.setText("Llamados");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Editar");

        jMenuItem4.setText("Ramos");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPTabPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPForms, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPOpen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPImport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPBottom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPForms, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPOpen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPImport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPTabPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPBottom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cargarRamos(){
        try{
            Controlador ctrl = Controlador.getInstance();
            ctrl.cargarRamos();
        }catch(BDException e){
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    private void checkAndUpdateVersion(){
        try{
            Controlador ctrl = Controlador.getInstance();
            int version = ctrl.buscarVersion();
            if (version > this.VERSION){
                startXR3PlayerUpdater(version);
            }
        }catch(BDException e){
            JOptionPane.showMessageDialog(null, e);
            ControladorExceptions.logErrorsStatic(e.toString(), e);
        }
    }
    
    private void maximizar(){
        setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
    }
    
    private void cargarPreferencias(){
        maximizar();
        //ESCONDO PANELS
        jPMain.setVisible(true);
        jPForms.setVisible(false);
        jPOpen.setVisible(false);
        jPImport.setVisible(false);
        jLVersion.setText(jLVersion.getText() + this.VERSION);
        botonNuevo();
    }
    
    private static boolean lockInstance(final String lockFile) {
        try {
            final File file = new File(lockFile);
            final RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            final FileLock fileLock = randomAccessFile.getChannel().tryLock();
            if (fileLock != null) {
                Runtime.getRuntime().addShutdownHook(new Thread() {
                    public void run() {
                        try {
                            fileLock.release();
                            randomAccessFile.close();
                            file.delete();
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                            ControladorExceptions.logErrorsStatic(e.toString(), e);
                        }
                    }
                });
                return true;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            ControladorExceptions.logErrorsStatic(e.toString(), e);
        }
        return false;
    }
    
    private void windowsLookAndFeel(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            Logger.getLogger(jFMain.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void jBNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBNuevoActionPerformed
        botonNuevo();
    }//GEN-LAST:event_jBNuevoActionPerformed

    private void jBAbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAbrirActionPerformed
        botonOpen();
    }//GEN-LAST:event_jBAbrirActionPerformed

    private void tabPaneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tabPaneStateChanged
//      CAMBIAR MENU SUPERIOR SEGÚN LA TAB
        if (tabPane.getSelectedIndex() != -1){
            if (tabPane.getTitleAt(tabPane.getSelectedIndex()).toLowerCase().equals("nuevo formulario")){
                jPMain.setVisible(true);
                jBGuardarLlamado.setVisible(true);
                jSeparator1.setVisible(true);
                jPOpen.setVisible(false);
                jPForms.setVisible(false);
                jPImport.setVisible(false);
            }else if(tabPane.getTitleAt(tabPane.getSelectedIndex()).toLowerCase().equals("cargar formulario")){
                jPMain.setVisible(false);
                jPOpen.setVisible(true);
                jPForms.setVisible(false);
                jPImport.setVisible(false);
            }else if(tabPane.getTitleAt(tabPane.getSelectedIndex()).toLowerCase().startsWith("anda") 
                    || tabPane.getTitleAt(tabPane.getSelectedIndex()).toLowerCase().startsWith("master") 
                    || tabPane.getTitleAt(tabPane.getSelectedIndex()).toLowerCase().startsWith("cabal")){
                jPMain.setVisible(false);
                jPOpen.setVisible(false);
                jPForms.setVisible(true);
                jPImport.setVisible(false);
                if (tabPane.getTitleAt(tabPane.getSelectedIndex()).toLowerCase().startsWith("anda")){
                    jBImprimir.setVisible(false);
                    jBExportPDF.setIcon(new ImageIcon(getClass().getResource("/imagenes/word-48.png")));
                }else{
                    jBImprimir.setVisible(true);
                    jBExportPDF.setIcon(new ImageIcon(getClass().getResource("/imagenes/PDF 2-48.png")));
                }
                if (tabPane.getTitleAt(tabPane.getSelectedIndex()).toLowerCase().startsWith("master")){
                    if (((jIFVistaFormFirstData)(Formulario)tabPane.getSelectedComponent()).isVal())
                        jBImprimir.setVisible(false);
                }
            }else if (tabPane.getTitleAt(tabPane.getSelectedIndex()).toLowerCase().startsWith("llamados")){
                jPMain.setVisible(false);
                jPOpen.setVisible(false);
                jPForms.setVisible(false);
                jPImport.setVisible(true);
            }else if (tabPane.getTitleAt(tabPane.getSelectedIndex()).toLowerCase().startsWith("editar")){
                jPMain.setVisible(true);
                jBGuardarLlamado.setVisible(false);
                jSeparator1.setVisible(false);
                jPOpen.setVisible(false);
                jPForms.setVisible(false);
                jPImport.setVisible(false);
            }
        }else{
            jPMain.setVisible(true);
            jBGuardarLlamado.setVisible(true);
            jSeparator1.setVisible(true);
            jPOpen.setVisible(false);
            jPForms.setVisible(false);
            jPImport.setVisible(false);
        }
    }//GEN-LAST:event_tabPaneStateChanged

    private void jBGuardarFormActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGuardarFormActionPerformed
        botonGuardarFormularios();
    }//GEN-LAST:event_jBGuardarFormActionPerformed

    private void jBImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBImprimirActionPerformed
        form = (Formulario)tabPane.getSelectedComponent();
        form.imprimir();
    }//GEN-LAST:event_jBImprimirActionPerformed

    private void jBSucursalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBSucursalActionPerformed
        form = (Formulario)tabPane.getSelectedComponent();
        form.listarSucursales();
    }//GEN-LAST:event_jBSucursalActionPerformed

    private void jBRamosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBRamosActionPerformed
        form = (Formulario)tabPane.getSelectedComponent();
        form.listarRamos();
    }//GEN-LAST:event_jBRamosActionPerformed

    private void jBExportPDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExportPDFActionPerformed
        botonExportPDFFormularios();
    }//GEN-LAST:event_jBExportPDFActionPerformed

    private void jBModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBModificarActionPerformed
        jIFOpen ji = (jIFOpen)tabPane.getSelectedComponent();
        ji.modificar();
    }//GEN-LAST:event_jBModificarActionPerformed

    private void jBBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBorrarActionPerformed
        botonBorrarOpen();
    }//GEN-LAST:event_jBBorrarActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        botonNuevo();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        botonOpen();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void tabPaneComponentRemoved(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_tabPaneComponentRemoved
        if(tabPane.getTabCount() == 0){
            botonNuevo();
        }
    }//GEN-LAST:event_tabPaneComponentRemoved

    private void jBRecargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBRecargarActionPerformed
        jIFOpen ji = (jIFOpen)tabPane.getSelectedComponent();
        ji.wkRecargar();
    }//GEN-LAST:event_jBRecargarActionPerformed

    private void tabPaneMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabPaneMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON2)
            botonMedioRaton();
    }//GEN-LAST:event_tabPaneMouseReleased

    private void jBLlamadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBLlamadosActionPerformed
        botonLlamados();
    }//GEN-LAST:event_jBLlamadosActionPerformed

    private void jBCargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBCargarActionPerformed
        try {    
            jIFImport ji = (jIFImport)tabPane.getSelectedComponent();
            ji.cargarGenerador();
        }catch (noSeleccionoElementoException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        }
    }//GEN-LAST:event_jBCargarActionPerformed

    private void jBBorrarImportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBorrarImportActionPerformed
        botonBorrarJiImport();
    }//GEN-LAST:event_jBBorrarImportActionPerformed

    private void jBVerdeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBVerdeActionPerformed
        colorJiImport(Color.GREEN);
    }//GEN-LAST:event_jBVerdeActionPerformed

    private void jBAmarilloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBAmarilloActionPerformed
        colorJiImport(Color.YELLOW);
    }//GEN-LAST:event_jBAmarilloActionPerformed

    private void jBRojoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBRojoActionPerformed
        colorJiImport(Color.RED);
    }//GEN-LAST:event_jBRojoActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        botonLlamados();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jBImportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBImportarActionPerformed
        botonImportarJiImport();
    }//GEN-LAST:event_jBImportarActionPerformed

    private void jBGuardarFormFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jBGuardarFormFocusLost
        if (((ImageIcon)jBGuardarForm.getIcon()).getDescription().equals("save close"))
            jBGuardarForm.setIcon(new ImageIcon(getClass().getResource("/imagenes/Save (2).png")));
    }//GEN-LAST:event_jBGuardarFormFocusLost

    private void jBExportExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBExportExcelActionPerformed
        botonExportJiImport();
    }//GEN-LAST:event_jBExportExcelActionPerformed

    private void jBGuardarLlamadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGuardarLlamadoActionPerformed
        botonGuardarGenerador();
    }//GEN-LAST:event_jBGuardarLlamadoActionPerformed

    private void jBGuardarLlamadoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jBGuardarLlamadoFocusLost
        if (((ImageIcon)jBGuardarLlamado.getIcon()).getDescription().equals("save close"))
            jBGuardarLlamado.setIcon(new ImageIcon(getClass().getResource("/imagenes/Save (2).png")));
    }//GEN-LAST:event_jBGuardarLlamadoFocusLost

    private void jBBlancoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBBlancoActionPerformed
        colorJiImport(Color.WHITE);
    }//GEN-LAST:event_jBBlancoActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        botonRecargarJiImport();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jBUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBUsuarioActionPerformed
        try {    
            jDUsuario jd = new jDUsuario(this, true, jBUsuario.getText());
            jd.setVisible(true);
            String usuario = jd.getUsuario();
            jBUsuario.setText(usuario);
            controladores.Controlador.modificarUsuario(hostname, usuario);
        } catch (BDException e) {
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            System.out.println(e);
        }
    }//GEN-LAST:event_jBUsuarioActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        botonEditarRamos();
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    public void startXR3PlayerUpdater(int update) {
        String applicationName = "XR3PlayerUpdater";
        // Start XR3Player Updater
        new Thread(() -> {
            String[] applicationPath = { new File(applicationName + ".jar").getAbsolutePath() };

            //Show message that application is restarting
//			Platform.runLater(() -> ActionTool.showNotification("Starting " + applicationName,
//					"Application Path:[ " + applicationPath[0] + " ]\n\tIf this takes more than 10 seconds either the computer is slow or it has failed....", Duration.seconds(25),
//					NotificationType.INFORMATION));

            try {

                //Create a process builder
                ProcessBuilder builder = new ProcessBuilder("java", "-jar", applicationPath[0], String.valueOf(update));
                builder.redirectErrorStream(true);
                Process process = builder.start();

            } catch (IOException e){
                System.out.println(e);
            }

        }, "Start XR3Application Thread").start();
        System.exit(0);
    }
    private void botonEditarRamos(){
        boolean abierto = false;
        for (int i = 0; i < tabPane.getTabCount(); i++){
            if (tabPane.getComponentAt(i) instanceof jIFConfiguracion){
                tabPane.setSelectedIndex(i);
                abierto = true;
                break;
            }
        }
        if (!abierto){
            jIFConfiguracion jif = new jIFConfiguracion();
            agregarTab("Editar ramos", jif);
        }
    }
    
    private void botonRecargarJiImport(){
        jIFImport ji = (jIFImport)tabPane.getSelectedComponent();
        ji.wkActualizarTablas(false);
    }
    
    private void botonBorrarOpen(){
        jIFOpen ji = (jIFOpen)tabPane.getSelectedComponent();
        ji.wkEliminar(jBBorrar);
    }
    
    private void botonExportPDFFormularios(){
        form = (Formulario)tabPane.getSelectedComponent();
        form.wkExportPDF(jBExportPDF);
    }
    
    private void botonGuardarGenerador(){
        jIFGenerarForm genForm = (jIFGenerarForm)tabPane.getSelectedComponent();
        genForm.wkGuardar(jBGuardarLlamado);
    }
    
    private void botonExportJiImport(){
        jIFImport ji = (jIFImport)tabPane.getSelectedComponent();
        ji.wkExportar(jBExportExcel);
    }
    
    private void botonGuardarFormularios(){
        form = (Formulario)tabPane.getSelectedComponent();
        form.wkGuardar(jBGuardarForm);
    }
    
    private void colorJiImport(Color c){
        try{
            jIFImport ji = (jIFImport)tabPane.getSelectedComponent();
            ji.pintarFilaSeleccionada(c);
        }catch(noSeleccionoElementoException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void botonMedioRaton(){
        tabPane.remove(tabPane.getSelectedComponent());
    }
    
    private void botonBorrarJiImport(){
        jIFImport ji = (jIFImport)tabPane.getSelectedComponent();
        ji.wkBorrarSeleccionados(jBBorrarImport);
    }
    
    private void botonImportarJiImport(){
        jIFImport jif = (jIFImport)tabPane.getSelectedComponent();
        jif.wkAgregarFilas();
    }
    
    private void botonLlamados(){
        boolean abierto = false;
        for (int i = 0; i < tabPane.getTabCount(); i++){
            if (tabPane.getComponentAt(i) instanceof jIFImport){
                tabPane.setSelectedIndex(i);
                abierto = true;
                break;
            }
        }
        if (!abierto){
            jIFImport jif = jIFImport.getInstance(tabPane, jBBorrarImport);
            agregarTab("Llamados", jif);
        }
    }
    
    private void botonOpen(){
        boolean hayOpen = false;
        for (int i = 0; i < tabPane.getTabCount(); i++){
            if (tabPane.getComponentAt(i) instanceof jIFOpen){
                tabPane.setSelectedIndex(i);
                hayOpen = true;
            }
        }
        if (!hayOpen){
            jIFOpen ji = new jIFOpen(tabPane);
            agregarTab("Cargar formulario", ji);
        }
    }
    
    private void botonNuevo(){
        jIFGenerarForm jif = new jIFGenerarForm(tabPane, null);
        agregarTab("Nuevo formulario", jif);
    }
    
    private void agregarTab(String titulo, JInternalFrame ji) {
        tabPane.add(titulo, ji);
        tabPane.setTabComponentAt(tabPane.getTabCount()- 1,
                 new ButtonTabComponent(tabPane));
        tabPane.setSelectedIndex(tabPane.getTabCount()- 1);
    }
    
    private static void userName(){
        // Pongo nombre de usuario según la PC
        jDUsuario jd = new jDUsuario(null, true, null);
        jd.setVisible(true);
        String usuario = jd.getUsuario();
        jBUsuario.setText(usuario);
    }
    
    public static void main(String args[]) {
        /* Create and display the form */
        if (lockInstance("lock.txt")){
            java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                    new jFMain().setVisible(true);
                    userName();
                }
            });
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBAbrir;
    private javax.swing.JButton jBAmarillo;
    private javax.swing.JButton jBBlanco;
    private javax.swing.JButton jBBorrar;
    private javax.swing.JButton jBBorrarImport;
    private javax.swing.JButton jBCargar;
    private javax.swing.JButton jBExportExcel;
    private javax.swing.JButton jBExportPDF;
    private javax.swing.JButton jBGuardarForm;
    private javax.swing.JButton jBGuardarLlamado;
    private javax.swing.JButton jBImportar;
    private javax.swing.JButton jBImprimir;
    private javax.swing.JButton jBLlamados;
    private javax.swing.JButton jBModificar;
    private javax.swing.JButton jBNuevo;
    private javax.swing.JButton jBRamos;
    private javax.swing.JButton jBRecargar;
    private javax.swing.JButton jBRojo;
    private javax.swing.JButton jBSucursal;
    public static javax.swing.JButton jBUsuario;
    private javax.swing.JButton jBVerde;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLVersion;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JPanel jPBottom;
    private javax.swing.JPanel jPForms;
    private javax.swing.JPanel jPImport;
    private javax.swing.JPanel jPMain;
    private javax.swing.JPanel jPOpen;
    private javax.swing.JPanel jPTabPane;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JTabbedPane tabPane;
    // End of variables declaration//GEN-END:variables
}
