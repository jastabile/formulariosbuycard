/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import controladores.Controlador;
import datatypes.DTBanco;
import datatypes.DTFormularioMaster;
import excepciones.BDException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import logica.ramoMaster;
import logica.workerExportar;
import logica.workerGuardar;

/**
 *
 * @author Javier
 */
public class jIFVistaFormFirstData extends javax.swing.JInternalFrame implements Formulario{
    
    private DTFormularioMaster dt;
    private boolean val;
    ArrayList<ramoMaster> listaRamosMaster;
    ArrayList<DTBanco> listaBancos;
    
    public jIFVistaFormFirstData(DTFormularioMaster dt, boolean val) {
        initComponents();
        this.listaBancos = null;
        this.listaRamosMaster = null;
        this.val = val;
        
        cargarPreferencias();
        this.dt = dt;
        cargar();
    }
    
    private void cargarPreferencias(){
        ((BasicInternalFrameUI)this.getUI()).setNorthPane(null);
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(16);
        
        if (!this.val){
            jPVista2.setVisible(false);
            jPSeparador.setVisible(false);
        }
    }

    public boolean isVal() {
        return val;
    }

    private void cargar(){
        jTPos.setText(dt.getPos());
        jTEntidad.setText(dt.getEntidad());
        jTSucursal.setText(dt.getSucursal());
        jTNombre.setText(dt.getNombreBanco());
        jTTarjetaCredito.setText(dt.getTarjetaCredito());
        jTMaestro.setText(dt.getMaestro());
        jTPosnetCelular.setText(dt.getPosnetCelular());
        jTDia.setText(dt.getDia());
        jTMes.setText(dt.getMes());
        jTAnio.setText(dt.getAnio());
        jTNombreFantasia.setText(dt.getNombreFantasia());
        jTRazonSocial.setText(dt.getRazonSocial());
        jTCalle.setText(dt.getCalle());
        jTNumero.setText(dt.getNumero());
        jTUbicacion.setText(dt.getUbicacion());
        jTCodPostal.setText(dt.getCodigoPostal());
        jTLocalidad.setText(dt.getLocalidad());
        jTUsuario.setText(dt.getDireccionAdicional());
        jTTelefonos.setText(dt.getTelefonos());
        jTCUIT.setText(dt.getCUIT());
        jTRUT.setText(dt.getRUT());
        jTNombreResponsable.setText(dt.getNombreResponsable());
        jTTipoDocumento.setText(dt.getTipoDocumento());
        jTNumDocumento.setText(dt.getDocumento());
        jTRamo.setText(dt.getRamo());
        jTDescripcion.setText(dt.getDescripcion());
        jTMail.setText(dt.getMail());
        jTTDebito.setText(dt.gettDebito());
        jTNumCuentaDebito.setText(dt.getNumCuentaDebito());
        jTEntidadDebito.setText(dt.getEntidadDebito());
        jTSucursalDebito.setText(dt.getSucursalDebito());
        jTPlazoPago.setText(dt.getPlazoPago());
        jTPorcDescuento.setText(dt.getPorcDescuento());
        jTShopping.setText(dt.getShopping());
        jTZonaComercial.setText(dt.getZonaComercial());
        jTTCredito.setText(dt.gettCredito());
        jTNumCuentaCredito.setText(dt.getNumCuentaCredito());
        jTEntidadCredito.setText(dt.getEntidadCredito());
        jTSucursalCredito.setText(dt.getSucursalCredito());
        jTOperaDolares.setText(dt.getOperaEnDolares());
        jTTDolaresCredito.setText(dt.gettDolaresCredito());
        jTNumCuentaDolaresCredito.setText(dt.getNumCuentaDolaresCredito());
        jTEntidadDolaresCredito.setText(dt.getEntidadDolaresCredito());
        jTSucursalDolaresCredito.setText(dt.getSucursalDolaresCredito());
        jTCuotasCobroAntic.setText(dt.getCuotasCobroAnticipado());
        jTPlazoPagoPesos.setText(dt.getPlazoPagoPesos());
        jTPlazoPagoDolares.setText(dt.getPlazoPagoDolares());
        jTPorcDescuentoPesos.setText(dt.getPorcDescuentoPesos());
        jTPorcDescuentoDolares.setText(dt.getPorcDescuentoDolares());
        
//        if (jTCUIT.getText().equals("CAT 0") || jTCUIT.getText().equals("CAT 1"))
//            jTPorcDescuento.setText("2.2");
//        else if (jTCUIT.getText().equals("CAT 4"))
//            jTPorcDescuento.setText("2");
        
        if (this.val){
            jTDia2.setText(dt.getDia());
            jTMes2.setText(dt.getMes());
            jTAnio2.setText(dt.getAnio());
            jTRut2.setText(dt.getRUT());
            jTNombreFantasia2.setText(dt.getNombreFantasia());
            jTRazonSocial2.setText(dt.getRazonSocial());
            jTCalle2.setText(dt.getCalle());
            jTNumero2.setText(dt.getNumero());
            jTCodPostal2.setText(dt.getCodigoPostal());
            jTLocalidad2.setText(dt.getLocalidad());
            jTTelefonos2.setText(dt.getTelefonos());
            jTNombreResponsable2.setText(dt.getNombreResponsable());
            jTNumDocumento2.setText(dt.getDocumento());
            jTUbicacion2.setText(dt.getUbicacion());
        }
    }
    
    
//    public void descargarImagen(){
//        try{
//            BufferedImage img = createImage(jPVista);
//            String userDir = System.getProperty("user.home");
//            JFileChooser chooser = new JFileChooser(userDir +"/Desktop");
//            chooser.setFileFilter(new FileNameExtensionFilter("PNG", ".png"));
//            if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION){
//                File outputfile = new File(chooser.getSelectedFile().getPath()+ ".png");
//                ImageIO.write(img, "png", outputfile);
//            }
//        }catch(IOException e){
//            JOptionPane.showMessageDialog(null, e.getMessage());
//        }
//    }
    
//    private BufferedImage createImage(JPanel panel){
//        BufferedImage bi = new BufferedImage((int)(panel.getWidth() + panel.getWidth() * 0.37), 
//                (int)(panel.getHeight() + panel.getHeight() * 0.37), BufferedImage.TYPE_INT_ARGB);
//        Graphics2D g2d = bi.createGraphics();
//        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
//        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
//        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
//        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
//        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
//        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
//        g2d.setTransform(AffineTransform.getScaleInstance(1.37, 1.37));
//        panel.print(g2d);
//        return bi;
//    
//    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPScroll = new javax.swing.JPanel();
        jPVista = new javax.swing.JPanel();
        jTNumDocumento = new javax.swing.JTextField();
        jTAnio = new javax.swing.JTextField();
        jTEntidad = new javax.swing.JTextField();
        jTSucursal = new javax.swing.JTextField();
        jTNombre = new javax.swing.JTextField();
        jTTarjetaCredito = new javax.swing.JTextField();
        jTPosnetCelular = new javax.swing.JTextField();
        jTMaestro = new javax.swing.JTextField();
        jTNombreFantasia = new javax.swing.JTextField();
        jTMes = new javax.swing.JTextField();
        jTDia = new javax.swing.JTextField();
        jTRazonSocial = new javax.swing.JTextField();
        jTCalle = new javax.swing.JTextField();
        jTNumero = new javax.swing.JTextField();
        jTUbicacion = new javax.swing.JTextField();
        jTCodPostal = new javax.swing.JTextField();
        jTLocalidad = new javax.swing.JTextField();
        jTTelefonos = new javax.swing.JTextField();
        jTUsuario = new javax.swing.JTextField();
        jTZonaComercial = new javax.swing.JTextField();
        jTRUT = new javax.swing.JTextField();
        jTNombreResponsable = new javax.swing.JTextField();
        jTTipoDocumento = new javax.swing.JTextField();
        jTCUIT = new javax.swing.JTextField();
        jTRamo = new javax.swing.JTextField();
        jTDescripcion = new javax.swing.JTextField();
        jTMail = new javax.swing.JTextField();
        jTTCredito = new javax.swing.JTextField();
        jTNumCuentaCredito = new javax.swing.JTextField();
        jTEntidadCredito = new javax.swing.JTextField();
        jTOperaDolares = new javax.swing.JTextField();
        jTPlazoPago = new javax.swing.JTextField();
        jTPorcDescuento = new javax.swing.JTextField();
        jTShopping = new javax.swing.JTextField();
        jTTDebito = new javax.swing.JTextField();
        jTNumCuentaDebito = new javax.swing.JTextField();
        jTEntidadDebito = new javax.swing.JTextField();
        jTSucursalDebito = new javax.swing.JTextField();
        jTTDolaresCredito = new javax.swing.JTextField();
        jTNumCuentaDolaresCredito = new javax.swing.JTextField();
        jTEntidadDolaresCredito = new javax.swing.JTextField();
        jTSucursalDolaresCredito = new javax.swing.JTextField();
        jTSucursalCredito = new javax.swing.JTextField();
        jTPlazoPagoDolares = new javax.swing.JTextField();
        jTCuotasCobroAntic = new javax.swing.JTextField();
        jTPlazoPagoPesos = new javax.swing.JTextField();
        jTPorcDescuentoPesos = new javax.swing.JTextField();
        jTPorcDescuentoDolares = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jTPos = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jPSeparador = new javax.swing.JPanel();
        jLabel49 = new javax.swing.JLabel();
        jPVista2 = new javax.swing.JPanel();
        jTRut2 = new javax.swing.JTextField();
        jTNombreFantasia2 = new javax.swing.JTextField();
        jTRazonSocial2 = new javax.swing.JTextField();
        jTCalle2 = new javax.swing.JTextField();
        jTNumero2 = new javax.swing.JTextField();
        jTCodPostal2 = new javax.swing.JTextField();
        jTLocalidad2 = new javax.swing.JTextField();
        jTTelefonos2 = new javax.swing.JTextField();
        jTNombreResponsable2 = new javax.swing.JTextField();
        jTTipoDoc2 = new javax.swing.JTextField();
        jTNumDocumento2 = new javax.swing.JTextField();
        jTDia2 = new javax.swing.JTextField();
        jTMes2 = new javax.swing.JTextField();
        jTAnio2 = new javax.swing.JTextField();
        jTUbicacion2 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setBorder(null);
        setClosable(true);
        setIconifiable(true);
        setTitle("Master");

        jPScroll.setBackground(new java.awt.Color(255, 255, 255));

        jPVista.setBackground(new java.awt.Color(255, 255, 255));
        jPVista.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTNumDocumento.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNumDocumento.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNumDocumento.setBorder(null);
        jTNumDocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTNumDocumentoKeyReleased(evt);
            }
        });
        jPVista.add(jTNumDocumento, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 230, 110, 10));

        jTAnio.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTAnio.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTAnio.setBorder(null);
        jTAnio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTAnioKeyReleased(evt);
            }
        });
        jPVista.add(jTAnio, new org.netbeans.lib.awtextra.AbsoluteConstraints(1016, 45, 25, 20));

        jTEntidad.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTEntidad.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTEntidad.setBorder(null);
        jTEntidad.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTEntidadCaretUpdate(evt);
            }
        });
        jTEntidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTEntidadKeyReleased(evt);
            }
        });
        jPVista.add(jTEntidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(395, 45, 35, -1));

        jTSucursal.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTSucursal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTSucursal.setBorder(null);
        jTSucursal.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTSucursalCaretUpdate(evt);
            }
        });
        jTSucursal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTSucursalKeyReleased(evt);
            }
        });
        jPVista.add(jTSucursal, new org.netbeans.lib.awtextra.AbsoluteConstraints(437, 45, 35, -1));

        jTNombre.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNombre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNombre.setBorder(null);
        jPVista.add(jTNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 45, 140, -1));

        jTTarjetaCredito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTTarjetaCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTTarjetaCredito.setText("X");
        jTTarjetaCredito.setBorder(null);
        jPVista.add(jTTarjetaCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 50, 10, 15));

        jTPosnetCelular.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTPosnetCelular.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTPosnetCelular.setBorder(null);
        jTPosnetCelular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTPosnetCelularActionPerformed(evt);
            }
        });
        jPVista.add(jTPosnetCelular, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 50, 10, 15));

        jTMaestro.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTMaestro.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTMaestro.setText("X");
        jTMaestro.setBorder(null);
        jPVista.add(jTMaestro, new org.netbeans.lib.awtextra.AbsoluteConstraints(705, 50, 10, 15));

        jTNombreFantasia.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNombreFantasia.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNombreFantasia.setBorder(null);
        jTNombreFantasia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTNombreFantasiaKeyReleased(evt);
            }
        });
        jPVista.add(jTNombreFantasia, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 93, 320, 25));

        jTMes.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTMes.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTMes.setBorder(null);
        jTMes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTMesKeyReleased(evt);
            }
        });
        jPVista.add(jTMes, new org.netbeans.lib.awtextra.AbsoluteConstraints(988, 45, 25, 20));

        jTDia.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTDia.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTDia.setBorder(null);
        jTDia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTDiaKeyReleased(evt);
            }
        });
        jPVista.add(jTDia, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 45, 25, 20));

        jTRazonSocial.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTRazonSocial.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTRazonSocial.setBorder(null);
        jTRazonSocial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTRazonSocialKeyReleased(evt);
            }
        });
        jPVista.add(jTRazonSocial, new org.netbeans.lib.awtextra.AbsoluteConstraints(605, 93, 435, 25));

        jTCalle.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTCalle.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTCalle.setBorder(null);
        jTCalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTCalleActionPerformed(evt);
            }
        });
        jTCalle.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTCalleKeyReleased(evt);
            }
        });
        jPVista.add(jTCalle, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 138, 390, 20));

        jTNumero.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNumero.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNumero.setBorder(null);
        jTNumero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTNumeroKeyReleased(evt);
            }
        });
        jPVista.add(jTNumero, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 138, 70, 20));

        jTUbicacion.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTUbicacion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTUbicacion.setBorder(null);
        jTUbicacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTUbicacionKeyReleased(evt);
            }
        });
        jPVista.add(jTUbicacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 138, 74, 20));

        jTCodPostal.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTCodPostal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTCodPostal.setBorder(null);
        jTCodPostal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTCodPostalKeyReleased(evt);
            }
        });
        jPVista.add(jTCodPostal, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 138, 74, 20));

        jTLocalidad.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTLocalidad.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTLocalidad.setBorder(null);
        jTLocalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTLocalidadKeyReleased(evt);
            }
        });
        jPVista.add(jTLocalidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(725, 138, 310, 20));

        jTTelefonos.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTTelefonos.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTTelefonos.setBorder(null);
        jTTelefonos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTTelefonosKeyReleased(evt);
            }
        });
        jPVista.add(jTTelefonos, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 180, 280, 20));

        jTUsuario.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTUsuario.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTUsuario.setText("BC");
        jTUsuario.setBorder(null);
        jPVista.add(jTUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, 320, 20));

        jTZonaComercial.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTZonaComercial.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTZonaComercial.setText("Z0001");
        jTZonaComercial.setBorder(null);
        jPVista.add(jTZonaComercial, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 353, 72, 20));

        jTRUT.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTRUT.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTRUT.setBorder(null);
        jTRUT.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTRUTKeyReleased(evt);
            }
        });
        jPVista.add(jTRUT, new org.netbeans.lib.awtextra.AbsoluteConstraints(215, 220, 190, 20));

        jTNombreResponsable.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNombreResponsable.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNombreResponsable.setBorder(null);
        jTNombreResponsable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTNombreResponsableKeyReleased(evt);
            }
        });
        jPVista.add(jTNombreResponsable, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 228, 420, 15));

        jTTipoDocumento.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTTipoDocumento.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTTipoDocumento.setText("CI");
        jTTipoDocumento.setBorder(null);
        jPVista.add(jTTipoDocumento, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 230, 20, 10));

        jTCUIT.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTCUIT.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTCUIT.setBorder(null);
        jPVista.add(jTCUIT, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, 160, 20));

        jTRamo.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTRamo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTRamo.setBorder(null);
        jPVista.add(jTRamo, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 263, 53, 20));

        jTDescripcion.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTDescripcion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTDescripcion.setBorder(null);
        jPVista.add(jTDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(94, 263, 250, 20));

        jTMail.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTMail.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTMail.setBorder(null);
        jPVista.add(jTMail, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 305, 560, 20));

        jTTCredito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTTCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTTCredito.setBorder(null);
        jPVista.add(jTTCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(35, 448, 10, 20));

        jTNumCuentaCredito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNumCuentaCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNumCuentaCredito.setBorder(null);
        jPVista.add(jTNumCuentaCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 448, 140, 20));

        jTEntidadCredito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTEntidadCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTEntidadCredito.setBorder(null);
        jPVista.add(jTEntidadCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 448, 37, 20));

        jTOperaDolares.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTOperaDolares.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTOperaDolares.setText("NO");
        jTOperaDolares.setBorder(null);
        jTOperaDolares.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTOperaDolaresKeyReleased(evt);
            }
        });
        jPVista.add(jTOperaDolares, new org.netbeans.lib.awtextra.AbsoluteConstraints(295, 448, 30, 20));

        jTPlazoPago.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jTPlazoPago.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTPlazoPago.setText("1");
        jTPlazoPago.setBorder(null);
        jPVista.add(jTPlazoPago, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 310, 20, 20));

        jTPorcDescuento.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jTPorcDescuento.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTPorcDescuento.setText("2");
        jTPorcDescuento.setBorder(null);
        jPVista.add(jTPorcDescuento, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 310, 60, 20));

        jTShopping.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTShopping.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTShopping.setText("S0001");
        jTShopping.setBorder(null);
        jPVista.add(jTShopping, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 353, 72, 20));

        jTTDebito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTTDebito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTTDebito.setBorder(null);
        jTTDebito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTTDebitoKeyReleased(evt);
            }
        });
        jPVista.add(jTTDebito, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 310, 10, 20));

        jTNumCuentaDebito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNumCuentaDebito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNumCuentaDebito.setBorder(null);
        jTNumCuentaDebito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTNumCuentaDebitoKeyReleased(evt);
            }
        });
        jPVista.add(jTNumCuentaDebito, new org.netbeans.lib.awtextra.AbsoluteConstraints(625, 310, 130, 20));

        jTEntidadDebito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTEntidadDebito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTEntidadDebito.setBorder(null);
        jPVista.add(jTEntidadDebito, new org.netbeans.lib.awtextra.AbsoluteConstraints(765, 310, 37, 20));

        jTSucursalDebito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTSucursalDebito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTSucursalDebito.setBorder(null);
        jPVista.add(jTSucursalDebito, new org.netbeans.lib.awtextra.AbsoluteConstraints(809, 310, 37, 20));

        jTTDolaresCredito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTTDolaresCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTTDolaresCredito.setBorder(null);
        jPVista.add(jTTDolaresCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(338, 448, 10, 20));

        jTNumCuentaDolaresCredito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNumCuentaDolaresCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNumCuentaDolaresCredito.setBorder(null);
        jPVista.add(jTNumCuentaDolaresCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(354, 448, 140, 20));

        jTEntidadDolaresCredito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTEntidadDolaresCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTEntidadDolaresCredito.setBorder(null);
        jPVista.add(jTEntidadDolaresCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(503, 448, 37, 20));

        jTSucursalDolaresCredito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTSucursalDolaresCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTSucursalDolaresCredito.setBorder(null);
        jPVista.add(jTSucursalDolaresCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(547, 448, 40, 20));

        jTSucursalCredito.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTSucursalCredito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTSucursalCredito.setBorder(null);
        jPVista.add(jTSucursalCredito, new org.netbeans.lib.awtextra.AbsoluteConstraints(243, 448, 40, 20));

        jTPlazoPagoDolares.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTPlazoPagoDolares.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTPlazoPagoDolares.setText("CUOTAS");
        jTPlazoPagoDolares.setBorder(null);
        jPVista.add(jTPlazoPagoDolares, new org.netbeans.lib.awtextra.AbsoluteConstraints(453, 492, 44, 10));

        jTCuotasCobroAntic.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTCuotasCobroAntic.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTCuotasCobroAntic.setBorder(null);
        jPVista.add(jTCuotasCobroAntic, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 485, 40, 30));

        jTPlazoPagoPesos.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTPlazoPagoPesos.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTPlazoPagoPesos.setText("15H");
        jTPlazoPagoPesos.setBorder(null);
        jPVista.add(jTPlazoPagoPesos, new org.netbeans.lib.awtextra.AbsoluteConstraints(403, 492, 42, 10));

        jTPorcDescuentoPesos.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTPorcDescuentoPesos.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTPorcDescuentoPesos.setText("4");
        jTPorcDescuentoPesos.setBorder(null);
        jTPorcDescuentoPesos.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                jTPorcDescuentoPesosCaretUpdate(evt);
            }
        });
        jPVista.add(jTPorcDescuentoPesos, new org.netbeans.lib.awtextra.AbsoluteConstraints(403, 510, 42, 10));

        jTPorcDescuentoDolares.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTPorcDescuentoDolares.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTPorcDescuentoDolares.setBorder(null);
        jPVista.add(jTPorcDescuentoDolares, new org.netbeans.lib.awtextra.AbsoluteConstraints(453, 510, 42, 10));

        jLabel13.setText("POS:");
        jPVista.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(395, 10, -1, -1));

        jTPos.setFont(new java.awt.Font("Calibri", 0, 13)); // NOI18N
        jTPos.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTPos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPVista.add(jTPos, new org.netbeans.lib.awtextra.AbsoluteConstraints(427, 5, 200, -1));

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/formVisto.png"))); // NOI18N
        jPVista.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jPSeparador.setBackground(new java.awt.Color(255, 255, 255));

        jLabel49.setText("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

        javax.swing.GroupLayout jPSeparadorLayout = new javax.swing.GroupLayout(jPSeparador);
        jPSeparador.setLayout(jPSeparadorLayout);
        jPSeparadorLayout.setHorizontalGroup(
            jPSeparadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPSeparadorLayout.createSequentialGroup()
                .addComponent(jLabel49)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPSeparadorLayout.setVerticalGroup(
            jPSeparadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPSeparadorLayout.createSequentialGroup()
                .addComponent(jLabel49)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPVista2.setBackground(new java.awt.Color(255, 255, 255));
        jPVista2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTRut2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTRut2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTRut2.setBorder(null);
        jTRut2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTRut2KeyReleased(evt);
            }
        });
        jPVista2.add(jTRut2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 225, 310, -1));

        jTNombreFantasia2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNombreFantasia2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNombreFantasia2.setBorder(null);
        jTNombreFantasia2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTNombreFantasia2KeyReleased(evt);
            }
        });
        jPVista2.add(jTNombreFantasia2, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 225, 570, 20));

        jTRazonSocial2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTRazonSocial2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTRazonSocial2.setBorder(null);
        jTRazonSocial2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTRazonSocial2KeyReleased(evt);
            }
        });
        jPVista2.add(jTRazonSocial2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 290, 920, -1));

        jTCalle2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTCalle2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTCalle2.setBorder(null);
        jTCalle2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTCalle2KeyReleased(evt);
            }
        });
        jPVista2.add(jTCalle2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 350, 750, -1));

        jTNumero2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNumero2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNumero2.setBorder(null);
        jTNumero2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTNumero2KeyReleased(evt);
            }
        });
        jPVista2.add(jTNumero2, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 350, 130, -1));

        jTCodPostal2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTCodPostal2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTCodPostal2.setBorder(null);
        jTCodPostal2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTCodPostal2KeyReleased(evt);
            }
        });
        jPVista2.add(jTCodPostal2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 410, 130, 20));

        jTLocalidad2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTLocalidad2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTLocalidad2.setBorder(null);
        jTLocalidad2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTLocalidad2KeyReleased(evt);
            }
        });
        jPVista2.add(jTLocalidad2, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 410, 520, 20));

        jTTelefonos2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTTelefonos2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTTelefonos2.setBorder(null);
        jTTelefonos2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTTelefonos2KeyReleased(evt);
            }
        });
        jPVista2.add(jTTelefonos2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 480, 430, -1));

        jTNombreResponsable2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNombreResponsable2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNombreResponsable2.setBorder(null);
        jTNombreResponsable2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTNombreResponsable2KeyReleased(evt);
            }
        });
        jPVista2.add(jTNombreResponsable2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 560, 660, -1));

        jTTipoDoc2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTTipoDoc2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTTipoDoc2.setText("C.I.");
        jTTipoDoc2.setBorder(null);
        jPVista2.add(jTTipoDoc2, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 560, 30, -1));

        jTNumDocumento2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTNumDocumento2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTNumDocumento2.setBorder(null);
        jTNumDocumento2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTNumDocumento2KeyReleased(evt);
            }
        });
        jPVista2.add(jTNumDocumento2, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 560, 200, -1));

        jTDia2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTDia2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTDia2.setBorder(null);
        jTDia2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTDia2ActionPerformed(evt);
            }
        });
        jTDia2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTDia2KeyReleased(evt);
            }
        });
        jPVista2.add(jTDia2, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 170, 20, -1));

        jTMes2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTMes2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTMes2.setBorder(null);
        jTMes2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTMes2KeyReleased(evt);
            }
        });
        jPVista2.add(jTMes2, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 170, 20, -1));

        jTAnio2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTAnio2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTAnio2.setBorder(null);
        jTAnio2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTAnio2KeyReleased(evt);
            }
        });
        jPVista2.add(jTAnio2, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 170, 30, -1));

        jTUbicacion2.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jTUbicacion2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTUbicacion2.setBorder(null);
        jTUbicacion2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTUbicacion2KeyReleased(evt);
            }
        });
        jPVista2.add(jTUbicacion2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 410, 120, 20));

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 13)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/masterForm2-1.png"))); // NOI18N
        jPVista2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        javax.swing.GroupLayout jPScrollLayout = new javax.swing.GroupLayout(jPScroll);
        jPScroll.setLayout(jPScrollLayout);
        jPScrollLayout.setHorizontalGroup(
            jPScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPSeparador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPScrollLayout.createSequentialGroup()
                .addComponent(jPVista, javax.swing.GroupLayout.PREFERRED_SIZE, 1203, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jPVista2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPScrollLayout.setVerticalGroup(
            jPScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPScrollLayout.createSequentialGroup()
                .addComponent(jPVista, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPSeparador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPVista2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPScroll);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1119, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTEntidadCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTEntidadCaretUpdate
        jTEntidadCredito.setText(jTEntidad.getText());
        jTEntidadDebito.setText(jTEntidad.getText());
        if (jTOperaDolares.getText().equals("SI"))
        jTEntidadDolaresCredito.setText(jTEntidad.getText());
    }//GEN-LAST:event_jTEntidadCaretUpdate

    private void jTEntidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTEntidadKeyReleased
        jTEntidadDebito.setText(jTEntidad.getText());
        jTEntidadCredito.setText(jTEntidad.getText());
        if (jTOperaDolares.getText().equals("SI"))
        jTEntidadDolaresCredito.setText(jTEntidad.getText());
    }//GEN-LAST:event_jTEntidadKeyReleased

    private void jTSucursalCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTSucursalCaretUpdate
        jTSucursalCredito.setText(jTSucursal.getText());
        jTSucursalDebito.setText(jTSucursal.getText());
        if (jTOperaDolares.getText().equals("SI"))
        jTSucursalDolaresCredito.setText(jTSucursal.getText());
    }//GEN-LAST:event_jTSucursalCaretUpdate

    private void jTSucursalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTSucursalKeyReleased
        jTSucursalCredito.setText(jTSucursal.getText());
        jTSucursalDebito.setText(jTSucursal.getText());
        if (jTOperaDolares.getText().equals("SI"))
        jTSucursalDolaresCredito.setText(jTSucursal.getText());
    }//GEN-LAST:event_jTSucursalKeyReleased

    private void jTOperaDolaresKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTOperaDolaresKeyReleased
        if (jTOperaDolares.getText().equals("si") || jTOperaDolares.getText().equals("SI") || jTOperaDolares.getText().equals("Si") || jTOperaDolares.getText().equals("sI")){
            jTTDolaresCredito.setText(jTTCredito.getText());
            jTNumCuentaDolaresCredito.setText(jTNumCuentaCredito.getText());
            jTEntidadDolaresCredito.setText(jTEntidadCredito.getText());
            jTSucursalDolaresCredito.setText(jTSucursalCredito.getText());
        }else if (jTOperaDolares.getText().equals("no") || jTOperaDolares.getText().equals("NO") || jTOperaDolares.getText().equals("No") || jTOperaDolares.getText().equals("nO")){
            jTTDolaresCredito.setText("");
            jTNumCuentaDolaresCredito.setText("");
            jTEntidadDolaresCredito.setText("");
            jTSucursalDolaresCredito.setText("");
        }
    }//GEN-LAST:event_jTOperaDolaresKeyReleased

    private void jTTDebitoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTTDebitoKeyReleased
        jTTCredito.setText(jTTDebito.getText());
    }//GEN-LAST:event_jTTDebitoKeyReleased

    private void jTNumCuentaDebitoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTNumCuentaDebitoKeyReleased
        jTNumCuentaCredito.setText(jTNumCuentaDebito.getText());
    }//GEN-LAST:event_jTNumCuentaDebitoKeyReleased

    private void jTPorcDescuentoPesosCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_jTPorcDescuentoPesosCaretUpdate
        if (jTPorcDescuentoPesos.getText().equals("4.5") || jTPorcDescuentoPesos.getText().equals("4,5"))
        jTPorcDescuentoDolares.setText("4.9");
        else if (jTPorcDescuentoPesos.getText().equals("4.0") || jTPorcDescuentoPesos.getText().equals("4,0") || jTPorcDescuentoPesos.getText().equals("4"))
        jTPorcDescuentoDolares.setText("4.26");
    }//GEN-LAST:event_jTPorcDescuentoPesosCaretUpdate

    private void jTPosnetCelularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTPosnetCelularActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTPosnetCelularActionPerformed

    private void jTCalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTCalleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTCalleActionPerformed

    private void jTNombreFantasiaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTNombreFantasiaKeyReleased
        jTNombreFantasia2.setText(jTNombreFantasia.getText());
    }//GEN-LAST:event_jTNombreFantasiaKeyReleased

    private void jTRazonSocialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTRazonSocialKeyReleased
        jTRazonSocial2.setText(jTRazonSocial.getText());
    }//GEN-LAST:event_jTRazonSocialKeyReleased

    private void jTCalleKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTCalleKeyReleased
        jTCalle2.setText(jTCalle.getText());
    }//GEN-LAST:event_jTCalleKeyReleased

    private void jTNumeroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTNumeroKeyReleased
        jTNumero2.setText(jTNumero.getText());
    }//GEN-LAST:event_jTNumeroKeyReleased

    private void jTCodPostalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTCodPostalKeyReleased
        jTCodPostal2.setText(jTCodPostal.getText());
    }//GEN-LAST:event_jTCodPostalKeyReleased

    private void jTLocalidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTLocalidadKeyReleased
        jTLocalidad2.setText(jTLocalidad.getText());
    }//GEN-LAST:event_jTLocalidadKeyReleased

    private void jTTelefonosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTTelefonosKeyReleased
        jTTelefonos2.setText(jTTelefonos.getText());
    }//GEN-LAST:event_jTTelefonosKeyReleased

    private void jTRUTKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTRUTKeyReleased
        jTRut2.setText(jTRUT.getText());
    }//GEN-LAST:event_jTRUTKeyReleased

    private void jTNombreResponsableKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTNombreResponsableKeyReleased
        jTNombreResponsable2.setText(jTNombreResponsable.getText());
    }//GEN-LAST:event_jTNombreResponsableKeyReleased

    private void jTNumDocumentoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTNumDocumentoKeyReleased
        jTNumDocumento2.setText(jTNumDocumento.getText());
    }//GEN-LAST:event_jTNumDocumentoKeyReleased

    private void jTDiaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTDiaKeyReleased
        jTDia2.setText(jTDia.getText());
    }//GEN-LAST:event_jTDiaKeyReleased

    private void jTMesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTMesKeyReleased
        jTMes2.setText(jTMes.getText());
    }//GEN-LAST:event_jTMesKeyReleased

    private void jTAnioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTAnioKeyReleased
        jTAnio2.setText(jTAnio.getText());
    }//GEN-LAST:event_jTAnioKeyReleased

    private void jTUbicacionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTUbicacionKeyReleased
        jTUbicacion2.setText(jTUbicacion.getText());
    }//GEN-LAST:event_jTUbicacionKeyReleased

    private void jTRut2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTRut2KeyReleased
        jTRUT.setText(jTRut2.getText());
    }//GEN-LAST:event_jTRut2KeyReleased

    private void jTNombreFantasia2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTNombreFantasia2KeyReleased
        jTNombreFantasia.setText(jTNombreFantasia2.getText());
    }//GEN-LAST:event_jTNombreFantasia2KeyReleased

    private void jTRazonSocial2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTRazonSocial2KeyReleased
        jTRazonSocial.setText(jTRazonSocial2.getText());
    }//GEN-LAST:event_jTRazonSocial2KeyReleased

    private void jTCalle2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTCalle2KeyReleased
        jTCalle.setText(jTCalle2.getText());
    }//GEN-LAST:event_jTCalle2KeyReleased

    private void jTNumero2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTNumero2KeyReleased
        jTNumero.setText(jTNumero2.getText());
    }//GEN-LAST:event_jTNumero2KeyReleased

    private void jTUbicacion2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTUbicacion2KeyReleased
        jTUbicacion.setText(jTUbicacion2.getText());
    }//GEN-LAST:event_jTUbicacion2KeyReleased

    private void jTCodPostal2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTCodPostal2KeyReleased
        jTCodPostal.setText(jTCodPostal2.getText());
    }//GEN-LAST:event_jTCodPostal2KeyReleased

    private void jTLocalidad2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTLocalidad2KeyReleased
        jTLocalidad.setText(jTLocalidad2.getText());
    }//GEN-LAST:event_jTLocalidad2KeyReleased

    private void jTTelefonos2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTTelefonos2KeyReleased
        jTTelefonos.setText(jTTelefonos2.getText());
    }//GEN-LAST:event_jTTelefonos2KeyReleased

    private void jTNombreResponsable2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTNombreResponsable2KeyReleased
        jTNombreResponsable.setText(jTNombreResponsable2.getText());
    }//GEN-LAST:event_jTNombreResponsable2KeyReleased

    private void jTNumDocumento2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTNumDocumento2KeyReleased
        jTNumDocumento.setText(jTNumDocumento2.getText());
    }//GEN-LAST:event_jTNumDocumento2KeyReleased

    private void jTDia2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTDia2KeyReleased
        jTDia.setText(jTDia2.getText());
    }//GEN-LAST:event_jTDia2KeyReleased

    private void jTMes2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTMes2KeyReleased
        jTMes.setText(jTMes2.getText());
    }//GEN-LAST:event_jTMes2KeyReleased

    private void jTAnio2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTAnio2KeyReleased
        jTAnio.setText(jTAnio2.getText());
    }//GEN-LAST:event_jTAnio2KeyReleased

    private void jTDia2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTDia2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTDia2ActionPerformed

    @Override
    public void listarRamos(){
        if (listaRamosMaster == null)
            listaRamosMaster = Controlador.getInstance().listarRamosMaster();
        jDBuscarRamoMaster jd = new jDBuscarRamoMaster(null, true);
        jd.setVisible(true);
        int i = jd.getIndice();
        if (i != -1){
            ramoMaster dt = listaRamosMaster.get(i);
            jTRamo.setText(dt.getCodigo().toUpperCase());
            jTDescripcion.setText(dt.getNombre().toUpperCase());
            if (dt.getDescuento() != null)
                jTPorcDescuentoPesos.setText(dt.getDescuento().toUpperCase());
            else
                jTPorcDescuentoPesos.setText("");
            if (dt.getDescuentoCuota() != null)
                jTPorcDescuentoDolares.setText(dt.getDescuentoCuota().toUpperCase());
            else
                jTPorcDescuentoDolares.setText("");
        }
    }
    
    @Override
    public void listarSucursales(){
        if (listaBancos == null)
            listaBancos = Controlador.getInstance().listarBancos();
        jDBuscarBanco jd =  new jDBuscarBanco(null, true);
        jd.setVisible(true);
        int i = jd.getIndice();
        if (i != -1){
            DTBanco dt = listaBancos.get(i);
            jTSucursal.setText(dt.getSucursal());
            jTEntidad.setText(dt.getEntidadMaster());
            jTNombre.setText(dt.getNombre());
        }
    }
    
    @Override
    public void wkGuardar(JButton boton){
        workerGuardar wk = new workerGuardar(this, boton);
        wk.execute();
    }
    
    @Override
    public void wkExportPDF(JButton boton){
        workerExportar wk = new workerExportar(this, boton);
        wk.execute();
    }
    
    @Override
    public void imprimir(){
        DTFormularioMaster dtNuevo = crearDT();
        jDImpresionMaster jdi;
        jdi = new jDImpresionMaster(null, true, dtNuevo, this.val);
        jdi.imprimir(dtNuevo);
        jdi.dispose();
    }
    
    @Override
    public boolean guardar(){
        boolean exito = false;
        try{
            DTFormularioMaster dtGuardar = crearDT();
            if (this.dt.getId() == -1){
                int id = Controlador.insertarFormMaster(dtGuardar);
                dtGuardar.setId(id);
                this.dt = dtGuardar;
            }else{
                Controlador.modificarFormMaster(dtGuardar);
            }
            exito = true;
        }catch(BDException e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return exito;
    }
    
    @Override
    public void exportPDF(){
        DTFormularioMaster dtNuevo = crearDT();
        jDImpresionMaster jd = new jDImpresionMaster(null, true, dtNuevo, dtNuevo.isVal());
        jd.descargarImagen();
    }
        
    private DTFormularioMaster crearDT(){
        String pos = jTPos.getText();
        String entidad = jTEntidad.getText();
        String sucursal = jTSucursal.getText();
        String nombreBanco = jTNombre.getText();
        String tarjetaCredito = jTTarjetaCredito.getText();
        String maestro = jTMaestro.getText();
        String posnetCelular = jTPosnetCelular.getText();
        String dia = jTDia.getText();
        String mes = jTMes.getText();
        String anio = jTAnio.getText();
        String nombreFantasia = jTNombreFantasia.getText();
        String razonSocial = jTRazonSocial.getText();
        String calle = jTCalle.getText();
        String numero = jTNumero.getText();
        String ubicacion = jTUbicacion.getText();
        String codigoPostal = jTCodPostal.getText();
        String localidad = jTLocalidad.getText();
        String direccionAdicional = jTUsuario.getText();
        String telefonos = jTTelefonos.getText();
        String CUIT = jTCUIT.getText();
        String RUT = jTRUT.getText();
        String nombreResponsable = jTNombreResponsable.getText();
        String tipoDocumento = jTTipoDocumento.getText();
        String documento = jTNumDocumento.getText();
        String ramo = jTRamo.getText();
        String descripcion = jTDescripcion.getText();
        String mail = jTMail.getText();
        String tDebito = jTTDebito.getText();
        String numCuentaDebito = jTNumCuentaDebito.getText();
        String entidadDebito = jTEntidadDebito.getText();
        String sucursalDebito = jTSucursalDebito.getText();
        String plazoPago = jTPlazoPago.getText();
        String porcDescuento = jTPorcDescuento.getText();
        String shopping = jTShopping.getText();
        String zonaComercial = jTZonaComercial.getText();
        String tCredito = jTTCredito.getText();
        String numCuentaCredito = jTNumCuentaCredito.getText();
        String entidadCredito = jTEntidadCredito.getText();
        String sucursalCredito = jTSucursalCredito.getText();
        String operaEnDolares = jTOperaDolares.getText();
        String tDolaresCredito = jTTDolaresCredito.getText();
        String numCuentaDolaresCredito = jTNumCuentaDolaresCredito.getText();
        String entidadDolaresCredito = jTEntidadDolaresCredito.getText();
        String sucursalDolaresCredito = jTSucursalDolaresCredito.getText();
        String cuotasCobroAnticipado = jTCuotasCobroAntic.getText();
        String plazoPagoPesos = jTPlazoPagoPesos.getText();
        String plazoPagoDolares = jTPlazoPagoDolares.getText();
        String porcDescuentoPesos = jTPorcDescuentoPesos.getText();
        String porcDescuentoDolares = jTPorcDescuentoDolares.getText();
        boolean val = this.val;
        
        DTFormularioMaster dtNuevo = new DTFormularioMaster(this.dt.getId(),pos, entidad, sucursal, nombreBanco,
                tarjetaCredito, maestro, posnetCelular, dia, mes, anio, nombreFantasia, 
                razonSocial, calle, numero, ubicacion, codigoPostal, localidad, direccionAdicional, 
                telefonos, CUIT, RUT, nombreResponsable, tipoDocumento, documento, ramo, 
                descripcion, mail, tDebito, numCuentaDebito, entidadDebito, sucursalDebito,
                plazoPago, porcDescuento, shopping, zonaComercial, tCredito, numCuentaCredito, 
                entidadCredito, sucursalCredito, operaEnDolares, tDolaresCredito, 
                numCuentaDolaresCredito, entidadDolaresCredito, sucursalDolaresCredito,
                cuotasCobroAnticipado, plazoPagoPesos, plazoPagoDolares, porcDescuentoPesos,
                porcDescuentoDolares, val);
        return dtNuevo;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JPanel jPScroll;
    private javax.swing.JPanel jPSeparador;
    private javax.swing.JPanel jPVista;
    private javax.swing.JPanel jPVista2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTAnio;
    private javax.swing.JTextField jTAnio2;
    private javax.swing.JTextField jTCUIT;
    private javax.swing.JTextField jTCalle;
    private javax.swing.JTextField jTCalle2;
    private javax.swing.JTextField jTCodPostal;
    private javax.swing.JTextField jTCodPostal2;
    private javax.swing.JTextField jTCuotasCobroAntic;
    private javax.swing.JTextField jTDescripcion;
    private javax.swing.JTextField jTDia;
    private javax.swing.JTextField jTDia2;
    private javax.swing.JTextField jTEntidad;
    private javax.swing.JTextField jTEntidadCredito;
    private javax.swing.JTextField jTEntidadDebito;
    private javax.swing.JTextField jTEntidadDolaresCredito;
    private javax.swing.JTextField jTLocalidad;
    private javax.swing.JTextField jTLocalidad2;
    private javax.swing.JTextField jTMaestro;
    private javax.swing.JTextField jTMail;
    private javax.swing.JTextField jTMes;
    private javax.swing.JTextField jTMes2;
    private javax.swing.JTextField jTNombre;
    private javax.swing.JTextField jTNombreFantasia;
    private javax.swing.JTextField jTNombreFantasia2;
    private javax.swing.JTextField jTNombreResponsable;
    private javax.swing.JTextField jTNombreResponsable2;
    private javax.swing.JTextField jTNumCuentaCredito;
    private javax.swing.JTextField jTNumCuentaDebito;
    private javax.swing.JTextField jTNumCuentaDolaresCredito;
    private javax.swing.JTextField jTNumDocumento;
    private javax.swing.JTextField jTNumDocumento2;
    private javax.swing.JTextField jTNumero;
    private javax.swing.JTextField jTNumero2;
    private javax.swing.JTextField jTOperaDolares;
    private javax.swing.JTextField jTPlazoPago;
    private javax.swing.JTextField jTPlazoPagoDolares;
    private javax.swing.JTextField jTPlazoPagoPesos;
    private javax.swing.JTextField jTPorcDescuento;
    private javax.swing.JTextField jTPorcDescuentoDolares;
    private javax.swing.JTextField jTPorcDescuentoPesos;
    private javax.swing.JTextField jTPos;
    private javax.swing.JTextField jTPosnetCelular;
    private javax.swing.JTextField jTRUT;
    private javax.swing.JTextField jTRamo;
    private javax.swing.JTextField jTRazonSocial;
    private javax.swing.JTextField jTRazonSocial2;
    private javax.swing.JTextField jTRut2;
    private javax.swing.JTextField jTShopping;
    private javax.swing.JTextField jTSucursal;
    private javax.swing.JTextField jTSucursalCredito;
    private javax.swing.JTextField jTSucursalDebito;
    private javax.swing.JTextField jTSucursalDolaresCredito;
    private javax.swing.JTextField jTTCredito;
    private javax.swing.JTextField jTTDebito;
    private javax.swing.JTextField jTTDolaresCredito;
    private javax.swing.JTextField jTTarjetaCredito;
    private javax.swing.JTextField jTTelefonos;
    private javax.swing.JTextField jTTelefonos2;
    private javax.swing.JTextField jTTipoDoc2;
    private javax.swing.JTextField jTTipoDocumento;
    private javax.swing.JTextField jTUbicacion;
    private javax.swing.JTextField jTUbicacion2;
    private javax.swing.JTextField jTUsuario;
    private javax.swing.JTextField jTZonaComercial;
    // End of variables declaration//GEN-END:variables
}
