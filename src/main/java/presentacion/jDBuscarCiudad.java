/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import controladores.Controlador;
import datatypes.DTCiudad;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Javier
 */
public class jDBuscarCiudad extends javax.swing.JDialog {

    DefaultTableModel model;
    TableRowSorter<DefaultTableModel> filter;
    private int indice;
    Controlador ctrl;
    
    public jDBuscarCiudad(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.ctrl = Controlador.getInstance();
        cargarTabla();
        centrar();
        indice = -1;
    }

    private void cargarTabla(){
        model = new DefaultTableModel(){
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };
        filter = new TableRowSorter(model);
        jTableCiudades.setModel(model);
        jTableCiudades.setRowSorter(filter);
        ArrayList<DTCiudad> listaCiudades = ctrl.listarCiudades();
        
        model.addColumn("NOMBRE");
        model.addColumn("COD POSTAL");
        for (DTCiudad dt: listaCiudades){
            Object fila[] = {dt,dt.getCodigoPostal()};
            model.addRow(fila);
        }
    }
    
    public int getIndice() {
        return indice;
    }

    private void setIndice(int indice) {
        this.indice = indice;
    }
    
    private void centrar(){
        this.setLocationRelativeTo(null);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jLabel = new javax.swing.JLabel();
        jTFiltrar = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableCiudades = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel.setText("Filtrar:");

        jTFiltrar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTFiltrarKeyReleased(evt);
            }
        });

        jTableCiudades.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableCiudadesMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTableCiudades);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 721, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTFiltrar)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel)
                    .addComponent(jTFiltrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 741, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 328, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTFiltrarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTFiltrarKeyReleased
        filter.setRowFilter(javax.swing.RowFilter.regexFilter("(?i)" + jTFiltrar.getText()));
    }//GEN-LAST:event_jTFiltrarKeyReleased

    private void jTableCiudadesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableCiudadesMouseClicked
        if (evt.getClickCount() == 2){
            setIndice(jTableCiudades.getRowSorter().convertRowIndexToModel(jTableCiudades.getSelectedRow()));
            this.dispose();
        }
    }//GEN-LAST:event_jTableCiudadesMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTFiltrar;
    private javax.swing.JTable jTableCiudades;
    // End of variables declaration//GEN-END:variables
}
