/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import controladores.Controlador;
import datatypes.DTBanco;
import datatypes.DTBancoInterior;
import datatypes.DTFormularioAnda;
import excepciones.BDException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import logica.ramoAnda;
import logica.ramoCabal;
import logica.workerExportar;
import logica.workerGuardar;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

/**
 *
 * @author Javier
 */
public class jIFFormAnda extends javax.swing.JInternalFrame implements Printable, Formulario{

    private ArrayList<ramoAnda> listaRamosAnda;
    private ArrayList<DTBanco> listaBancos;
    private DTFormularioAnda dt;
    
    public jIFFormAnda(DTFormularioAnda dt) {
        initComponents();
        this.listaRamosAnda = null;
        this.listaBancos = null;
        this.dt = dt;
        cargarPreferencias();
        cargarDatos();
    }

    private void cargarDatos(){
        jTRazonSocial.setText(dt.getRazonSocial());
        jTNombreComercial.setText(dt.getNombreComercial());
        jTRUC.setText(dt.getRUT());
        jTNombreContacto.setText(dt.getNombreContacto());
        jTCI.setText(dt.getCedula());
        jTCaracterDe.setText(dt.getCaracterDe());
        jTTel.setText(dt.getTelefono());
        jTCalle.setText(dt.getCalle());
        jTNumero.setText(dt.getNumero());
        jTComplemento.setText(dt.getApto());
        jTCiudad.setText(dt.getCiudad());
        jTDepartamento.setText(dt.getDepartamento());
        jtTelAdmin.setText(dt.getTelefono());
        jTCel.setText(dt.getCelular());
        jTMail.setText(dt.getMail());
        jTCalleCentral.setText(dt.getCalleCentral());
        jTNumCentral.setText(dt.getNumeroCentral());
        jTComplementoCentral.setText(dt.getAptoCentral());
        jTCiudadCentral.setText(dt.getCiudadCentral());
        jTDepartamentoCentral.setText(dt.getDepartamentoCentral());
        jTTelCentral.setText(dt.getTelefonoCentral());
        jTCelCentral.setText(dt.getCelularCentral());
        jTRamoPpal.setText(dt.getRamoPpal());
        jTAcuerdo.setText(dt.getAcuerdo());
        jTPos.setText(dt.getPOS());
        jTFecha.setText(dt.getFecha());
        jTCC.setText(dt.getCuentaCorriente());
        jTCA.setText(dt.getCajaAhorro());
        jTTitular.setText(dt.getTitularCuenta());
        jTBanco.setText(dt.getBanco());
        jTLocalidadBanco.setText(dt.getLocalidadBanco());
        jTSucursal.setText(dt.getSucursal());
        jTLocalidad.setText(dt.getLocalidad());
        jTUsuario.setText(dt.getUsuario());
        jTDescuento.setText(dt.getDescuento());
        jTDescuentoCuota.setText(dt.getDescuentoCuota());
    }
    
    private void cargarPreferencias(){
        ((BasicInternalFrameUI)this.getUI()).setNorthPane(null);
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(16);
    }
    
    public void listarRamos(){
        if (listaRamosAnda == null)
            listaRamosAnda = Controlador.getInstance().listarRamosAnda();
        jDBuscarRamoCabal jd = new jDBuscarRamoCabal(null, true);
        jd.setVisible(true);
        int i = jd.getIndice();
        if (i != -1){
            ramoAnda dt = listaRamosAnda.get(i);
            jTRamoPpal.setText(dt.getNombre().toUpperCase());
            jTDescuento.setText(dt.getDescuento());
            jTDescuentoCuota.setText(dt.getDescuentoCuota());
        }
    }

    @Override
    public void listarSucursales() {
        if (listaBancos == null)
            listaBancos = Controlador.getInstance().listarBancos();
        jDBuscarBanco jd =  new jDBuscarBanco(null, true);
        jd.setVisible(true);
        int i = jd.getIndice();
        if (i != -1){
            DTBanco dt = listaBancos.get(i);
            jTBanco.setText(dt.getNombre());
            jTSucursal.setText(dt.getSucursal());
            jTLocalidadBanco.setText("MONTEVIDEO");
            if (dt instanceof DTBancoInterior)
                jTLocalidadBanco.setText(((DTBancoInterior) dt).getCiudad());
        }
        
    }

    @Override
    public void imprimir() {
        try {
            PrinterJob job = PrinterJob.getPrinterJob();
            job.setPrintable(this);
            if (job.printDialog()){
                job.print();
            }
        } catch (PrinterException e) { 
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            System.out.println(e.getMessage());
        }
    }

    @Override
    public boolean guardar() {
        boolean exito = false;
        try{
            DTFormularioAnda dtGuardar = obtenerDatosFormulario();
            if (this.dt.getId() == -1){
                int id = Controlador.insertarFormAnda(dtGuardar);
                dtGuardar.setId(id);
                this.dt = dtGuardar;
            }else{
                Controlador.modificarFormAnda(dtGuardar);
            }
            exito = true;
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return exito;
    }

    private DTFormularioAnda obtenerDatosFormulario(){
        String razonSocial = jTRazonSocial.getText();
        String nombreComercial = jTNombreComercial.getText();
        String RUT = jTRUC.getText();
        String nombreContacto = jTNombreContacto.getText();
        String cedula = jTCI.getText();
        String caracterDe = jTCaracterDe.getText();
        String telefono = jTTel.getText();
        String calle = jTCalle.getText();
        String numero = jTNumero.getText();
        String apto = jTComplemento.getText();
        String ciudad = jTCiudad.getText();
        String departamento = jTDepartamento.getText();
        String celular = jTCel.getText();
        String mail = jTMail.getText();
        String calleCentral = jTCalleCentral.getText();
        String numeroCentral = jTNumCentral.getText();
        String aptoCentral = jTComplementoCentral.getText();
        String ciudadCentral = jTCiudadCentral.getText();
        String departamentoCentral = jTDepartamentoCentral.getText();
        String telefonoCentral = jTTelCentral.getText();
        String celularCentral = jTCelCentral.getText();
        String ramoPpal = jTRamoPpal.getText();
        String acuerdo = jTAcuerdo.getText();
        String pos = jTPos.getText();
        String fecha = jTFecha.getText();
        String cuentaCorriente = jTCC.getText();
        String cajaAhorro = jTCA.getText();
        String titularCuenta = jTTitular.getText();
        String banco = jTBanco.getText();
        String localidadBanco = jTLocalidadBanco.getText();
        String sucursal = jTSucursal.getText();
        String localidad = jTLocalidad.getText();
        String usuario = jTUsuario.getText();
        String descuento = jTDescuento.getText();
        String descuentoCuota = jTDescuentoCuota.getText();
        return new DTFormularioAnda(dt.getId(), razonSocial, nombreComercial,
                RUT, nombreContacto, cedula, caracterDe, telefono, calle, numero, apto, ciudad, 
                departamento, celular, mail, calleCentral, numeroCentral, aptoCentral, ciudadCentral, 
                departamentoCentral, telefonoCentral, celularCentral, ramoPpal, acuerdo, pos, fecha, 
                cuentaCorriente, cajaAhorro, titularCuenta, banco, localidadBanco, sucursal, localidad, usuario, descuento, descuentoCuota);
    }
    
    @Override
    public void exportPDF() {
        try{
            String userDir = System.getProperty("user.home");
            JFileChooser chooser = new JFileChooser(userDir +"/Desktop");
            String extension = ".docx";
            if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION){
                if (chooser.getSelectedFile().getPath().endsWith(".docx"))
                    extension = "";
                String outputfile = chooser.getSelectedFile().getPath()+ extension;
                XWPFDocument doc = null;
                if (dt.getCiudad().toUpperCase().contains("MONTEVIDEO"))
                    doc = openDocument("recursos/contratoAndaMontevideo.DOCX");
                else
                    doc = openDocument("recursos/contratoAndaInterior.DOCX");
                doc = replaceText(doc);
                saveDocument(doc, outputfile);
                doc.close();
            }
        }catch(IOException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            System.out.println(e.getMessage());
        }catch(Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            System.out.println(e.getMessage());
        }
    }
    
    private XWPFDocument replaceText(XWPFDocument doc) {
        this.dt = obtenerDatosFormulario();
        String dias2 = "";
        String dias3 = "";
        String dias4 = "";
        String dias5 = "";
        String dias6 = "";
        if (!dt.getDescuentoCuota().equals("")){
            dias2 = "30";
            dias3 = "45";
            dias4 = "60";
            dias5 = "75";
            dias6 = "90";
        }
        for (XWPFParagraph p : doc.getParagraphs()) {
            List<XWPFRun> runs = p.getRuns();
            if (runs != null) {
                for (XWPFRun r : runs) {
                    String text = r.getText(0);
                    if (text != null && text.contains("$RAZONSOCIAL")) {
                        text = text.replace("$RAZONSOCIAL", dt.getRazonSocial());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$TITULAR")) {
                        text = text.replace("$TITULAR", dt.getTitularCuenta());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$FANTASIA")) {
                        text = text.replace("$FANTASIA", dt.getNombreComercial());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$RUT")) {
                        text = text.replace("$RUT", dt.getRUT());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$CONTACTO")) {
                        text = text.replace("$CONTACTO", dt.getNombreContacto());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$CEDULA")) {
                        text = text.replace("$CEDULA", dt.getCedula());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$TELEFONO")) {
                        text = text.replace("$TELEFONO", dt.getTelefono());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$CALLE")) {
                        text = text.replace("$CALLE", dt.getCalle());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$NUMERO")) {
                        text = text.replace("$NUMERO", dt.getNumero());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$CIUDAD")) {
                        text = text.replace("$CIUDAD", dt.getCiudad());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$DEPART")) {
                        text = text.replace("$DEPART", dt.getDepartamento());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$CORREO")) {
                        text = text.replace("$CORREO", dt.getMail());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$RUBRO")) {
                        text = text.replace("$RUBRO", dt.getRamoPpal());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$POS")) {
                        text = text.replace("$POS", dt.getPOS());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$FECHA")) {
                        text = text.replace("$FECHA", dt.getFecha());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$CC")) {
                        text = text.replace("$CC", dt.getCuentaCorriente());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$CA")) {
                        text = text.replace("$CA", dt.getCajaAhorro());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$BANCO")) {
                        text = text.replace("$BANCO", dt.getBanco());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$SUC")) {
                        text = text.replace("$SUC", dt.getSucursal());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$LOC")) {
                        text = text.replace("$LOC", dt.getLocalidadBanco());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$USUARIO")) {
                        text = text.replace("$USUARIO", dt.getUsuario());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$%1")) {
                        text = text.replace("$%1", dt.getDescuento() + "%");
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$%2")) {
                        text = text.replace("$%2", dt.getDescuentoCuota() + "%");
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$dias2")) {
                        text = text.replace("$dias2", dias2);
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$dias3")) {
                        text = text.replace("$dias3", dias3);
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$dias4")) {
                        text = text.replace("$dias4", dias4);
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$dias5")) {
                        text = text.replace("$dias5", dias5);
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$dias6")) {
                        text = text.replace("$dias6", dias6);
                        r.setText(text, 0);
                    }
                }
            }
        }
        for (XWPFTable tbl : doc.getTables()) {
           for (XWPFTableRow row : tbl.getRows()) {
              for (XWPFTableCell cell : row.getTableCells()) {
                 for (XWPFParagraph p : cell.getParagraphs()) {
                    for (XWPFRun r : p.getRuns()) {
                        String text = r.getText(0);
                        if (text != null && text.contains("$RAZONSOCIAL")) {
                        text = text.replace("$RAZONSOCIAL", dt.getRazonSocial());
                        r.setText(text, 0);
                        }
                        if (text != null && text.contains("$TITULAR")) {
                            text = text.replace("$TITULAR", dt.getTitularCuenta());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$FANTASIA")) {
                            text = text.replace("$FANTASIA", dt.getNombreComercial());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$RUT")) {
                            text = text.replace("$RUT", dt.getRUT());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$CONTACTO")) {
                            text = text.replace("$CONTACTO", dt.getNombreContacto());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$CEDULA")) {
                            text = text.replace("$CEDULA", dt.getCedula());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$TELEFONO")) {
                            text = text.replace("$TELEFONO", dt.getTelefono());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$CALLE")) {
                            text = text.replace("$CALLE", dt.getCalle());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$NUMERO")) {
                            text = text.replace("$NUMERO", dt.getNumero());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$CIUDAD")) {
                            text = text.replace("$CIUDAD", dt.getCiudad());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$DEPART")) {
                            text = text.replace("$DEPART", dt.getDepartamento());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$CORREO")) {
                            text = text.replace("$CORREO", dt.getMail());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$RUBRO")) {
                            text = text.replace("$RUBRO", dt.getRamoPpal());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$POS")) {
                            text = text.replace("$POS", dt.getPOS());
                            r.setText(text, 0);
                        }
                        if (text != null && text.contains("$FECHA")) {
                        text = text.replace("$FECHA", dt.getFecha());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$CC")) {
                        text = text.replace("$CC", dt.getCuentaCorriente());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$CA")) {
                        text = text.replace("$CA", dt.getCajaAhorro());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$BANCO")) {
                        text = text.replace("$BANCO", dt.getBanco());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$SUC")) {
                        text = text.replace("$SUC", dt.getSucursal());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$LOC")) {
                        text = text.replace("$LOC", dt.getLocalidadBanco());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$USUARIO")) {
                        text = text.replace("$USUARIO", dt.getUsuario());
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$%1")) {
                        text = text.replace("$%1", dt.getDescuento() + "%");
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$%2")) {
                        text = text.replace("$%2", dt.getDescuentoCuota() + "%");
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$dias2")) {
                        text = text.replace("$dias2", dias2);
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$dias3")) {
                        text = text.replace("$dias3", dias3);
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$dias4")) {
                        text = text.replace("$dias4", dias4);
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$dias5")) {
                        text = text.replace("$dias5", dias5);
                        r.setText(text, 0);
                    }
                    if (text != null && text.contains("$dias6")) {
                        text = text.replace("$dias6", dias6);
                        r.setText(text, 0);
                    }
                    }
                 }
              }
           }
        }
        return doc;
    }

    private XWPFDocument openDocument(String file) throws Exception {
        XWPFDocument document = new XWPFDocument(getClass().getClassLoader().getResourceAsStream(file));
        return document;
    }

    private void saveDocument(XWPFDocument doc, String file) {
        try (FileOutputStream out = new FileOutputStream(file)) {
            doc.write(out);
        } catch (IOException e) {
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
        }
    }
    
    private void agregarContrato(Document document){
        try{
            document.newPage();
            Paragraph subPara = new Paragraph("Subcategory 1");
            document.add(subPara);
        }catch(DocumentException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    @Override
    public void wkGuardar(JButton boton){
        workerGuardar wk = new workerGuardar(this, boton);
        wk.execute();
    }
    
    @Override
    public void wkExportPDF(JButton boton){
        workerExportar wk = new workerExportar(this, boton);
        wk.execute();
    }
    
    private BufferedImage createImage(JPanel panel){
        BufferedImage bi = new BufferedImage((int)(panel.getWidth()), (int)(panel.getHeight()), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bi.createGraphics();
//        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
//        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
//        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
//        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
//        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
//        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
//        g2d.setTransform(AffineTransform.getScaleInstance(1, 1));
        panel.print(g2d);
        return bi;
    }
    
    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
//        if (pageIndex > 0) 
//            return NO_SUCH_PAGE;
//        
//        Graphics2D g2d = (Graphics2D)graphics;
//        g2d.scale(pageFormat.getImageableWidth()/jPImpresion.getWidth(),pageFormat.getImageableHeight()/jPImpresion.getHeight());
//        
//        jPImpresion.print(g2d);
//        return PAGE_EXISTS;
        
        BufferedImage img = createImage(jPVista);
        if (pageIndex == 0) {
            int pWidth = 0;
            int pHeight = 0;

            pHeight = (int) Math.min(pageFormat.getImageableWidth(), (double) img.getHeight());
            pHeight = pHeight - 25;
            pWidth = pHeight * img.getWidth() / img.getHeight();
            graphics.drawImage(img, (int) pageFormat.getImageableX() + 25, (int) pageFormat.getImageableY() - 15, (int)(pWidth + pWidth * 0.37), (int)(pHeight + pHeight * 0.37), null);
            return PAGE_EXISTS;
        } else {
            return NO_SUCH_PAGE;
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jPScroll = new javax.swing.JPanel();
        jPVista = new javax.swing.JPanel();
        jTRazonSocial = new javax.swing.JTextField();
        jTNombreComercial = new javax.swing.JTextField();
        jTRUC = new javax.swing.JTextField();
        jTNombreContacto = new javax.swing.JTextField();
        jTCI = new javax.swing.JTextField();
        jTCaracterDe = new javax.swing.JTextField();
        jTTel = new javax.swing.JTextField();
        jTCalle = new javax.swing.JTextField();
        jTNumero = new javax.swing.JTextField();
        jTComplemento = new javax.swing.JTextField();
        jTCiudad = new javax.swing.JTextField();
        jTDepartamento = new javax.swing.JTextField();
        jtTelAdmin = new javax.swing.JTextField();
        jTCel = new javax.swing.JTextField();
        jTMail = new javax.swing.JTextField();
        jTCalleCentral = new javax.swing.JTextField();
        jTNumCentral = new javax.swing.JTextField();
        jTComplementoCentral = new javax.swing.JTextField();
        jTCiudadCentral = new javax.swing.JTextField();
        jTDepartamentoCentral = new javax.swing.JTextField();
        jTTelCentral = new javax.swing.JTextField();
        jTCelCentral = new javax.swing.JTextField();
        jTRamoPpal = new javax.swing.JTextField();
        jTAcuerdo = new javax.swing.JTextField();
        jTPos = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPVista2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTFecha = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTLocalidad = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTCC = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTCA = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTBanco = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTSucursal = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTDescuento = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTDescuentoCuota = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTUsuario = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTLocalidadBanco = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTTitular = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(null);

        jPScroll.setBackground(new java.awt.Color(255, 255, 255));

        jPVista.setBackground(new java.awt.Color(255, 255, 255));
        jPVista.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTRazonSocial.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTRazonSocial.setBorder(null);
        jTRazonSocial.setOpaque(false);
        jPVista.add(jTRazonSocial, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 174, 670, 20));

        jTNombreComercial.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTNombreComercial.setBorder(null);
        jTNombreComercial.setOpaque(false);
        jPVista.add(jTNombreComercial, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 203, 610, 20));

        jTRUC.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTRUC.setBorder(null);
        jTRUC.setOpaque(false);
        jPVista.add(jTRUC, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 232, 260, 20));

        jTNombreContacto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTNombreContacto.setBorder(null);
        jTNombreContacto.setOpaque(false);
        jPVista.add(jTNombreContacto, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 290, 360, 20));

        jTCI.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTCI.setBorder(null);
        jTCI.setOpaque(false);
        jPVista.add(jTCI, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 288, 200, 20));

        jTCaracterDe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTCaracterDe.setBorder(null);
        jTCaracterDe.setOpaque(false);
        jPVista.add(jTCaracterDe, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 320, 170, 20));

        jTTel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTTel.setBorder(null);
        jTTel.setOpaque(false);
        jPVista.add(jTTel, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 318, 190, 20));

        jTCalle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTCalle.setBorder(null);
        jTCalle.setOpaque(false);
        jPVista.add(jTCalle, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 390, 590, -1));

        jTNumero.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTNumero.setBorder(null);
        jTNumero.setOpaque(false);
        jPVista.add(jTNumero, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 388, 110, -1));

        jTComplemento.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTComplemento.setBorder(null);
        jTComplemento.setOpaque(false);
        jPVista.add(jTComplemento, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 419, 350, -1));

        jTCiudad.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTCiudad.setBorder(null);
        jTCiudad.setOpaque(false);
        jPVista.add(jTCiudad, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 450, 370, -1));

        jTDepartamento.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTDepartamento.setBorder(null);
        jTDepartamento.setOpaque(false);
        jPVista.add(jTDepartamento, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 448, 270, -1));

        jtTelAdmin.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jtTelAdmin.setBorder(null);
        jtTelAdmin.setOpaque(false);
        jPVista.add(jtTelAdmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 477, 160, -1));

        jTCel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTCel.setBorder(null);
        jTCel.setOpaque(false);
        jPVista.add(jTCel, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 475, 160, -1));

        jTMail.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTMail.setBorder(null);
        jTMail.setOpaque(false);
        jPVista.add(jTMail, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 505, 740, -1));

        jTCalleCentral.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTCalleCentral.setText("IDEM");
        jTCalleCentral.setBorder(null);
        jTCalleCentral.setOpaque(false);
        jPVista.add(jTCalleCentral, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 573, 580, -1));

        jTNumCentral.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTNumCentral.setBorder(null);
        jTNumCentral.setOpaque(false);
        jPVista.add(jTNumCentral, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 570, 110, -1));

        jTComplementoCentral.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTComplementoCentral.setBorder(null);
        jTComplementoCentral.setOpaque(false);
        jPVista.add(jTComplementoCentral, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 602, 350, -1));

        jTCiudadCentral.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTCiudadCentral.setBorder(null);
        jTCiudadCentral.setOpaque(false);
        jPVista.add(jTCiudadCentral, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 631, 380, -1));

        jTDepartamentoCentral.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTDepartamentoCentral.setBorder(null);
        jTDepartamentoCentral.setOpaque(false);
        jPVista.add(jTDepartamentoCentral, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 629, 270, -1));

        jTTelCentral.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTTelCentral.setBorder(null);
        jTTelCentral.setOpaque(false);
        jPVista.add(jTTelCentral, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 658, 150, -1));

        jTCelCentral.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTCelCentral.setBorder(null);
        jTCelCentral.setOpaque(false);
        jPVista.add(jTCelCentral, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 656, 160, -1));

        jTRamoPpal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTRamoPpal.setBorder(null);
        jTRamoPpal.setOpaque(false);
        jPVista.add(jTRamoPpal, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 720, 650, -1));

        jTAcuerdo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTAcuerdo.setBorder(null);
        jTAcuerdo.setOpaque(false);
        jPVista.add(jTAcuerdo, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 783, 560, -1));

        jTPos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jTPos.setBorder(null);
        jTPos.setOpaque(false);
        jPVista.add(jTPos, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 810, 240, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Anda form chico.png"))); // NOI18N
        jPVista.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 990, -1));

        jPVista2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setText("FECHA");

        jLabel7.setText("LOCALIDAD");

        jLabel4.setText("CUENTA CORRIENTE");

        jLabel5.setText("CAJA AHORRO");

        jLabel3.setText("BANCO");

        jLabel6.setText("SUCURSAL");

        jLabel9.setText("DESCUENTO");

        jLabel10.setText("DESCUENTO CUOTA");

        jLabel8.setText("USUARIO");

        jLabel11.setText("LOCALIDAD BANCO");

        jLabel12.setText("TITULAR CUENTA");

        javax.swing.GroupLayout jPVista2Layout = new javax.swing.GroupLayout(jPVista2);
        jPVista2.setLayout(jPVista2Layout);
        jPVista2Layout.setHorizontalGroup(
            jPVista2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPVista2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPVista2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel10)
                    .addComponent(jLabel8)
                    .addComponent(jTDescuentoCuota, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                    .addComponent(jLabel2)
                    .addComponent(jTFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5)
                    .addComponent(jLabel9)
                    .addComponent(jTSucursal)
                    .addComponent(jTCA)
                    .addComponent(jTCC)
                    .addComponent(jTLocalidad)
                    .addComponent(jTDescuento)
                    .addComponent(jTUsuario)
                    .addComponent(jLabel3)
                    .addComponent(jTBanco)
                    .addComponent(jLabel11)
                    .addComponent(jTLocalidadBanco)
                    .addComponent(jLabel12)
                    .addComponent(jTTitular))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPVista2Layout.setVerticalGroup(
            jPVista2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPVista2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTLocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTCC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTCA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTTitular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTBanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTLocalidadBanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTSucursal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTDescuento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTDescuentoCuota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPScrollLayout = new javax.swing.GroupLayout(jPScroll);
        jPScroll.setLayout(jPScrollLayout);
        jPScrollLayout.setHorizontalGroup(
            jPScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPScrollLayout.createSequentialGroup()
                .addComponent(jPVista, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPVista2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPScrollLayout.setVerticalGroup(
            jPScrollLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPScrollLayout.createSequentialGroup()
                .addComponent(jPVista, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPScrollLayout.createSequentialGroup()
                .addGap(145, 145, 145)
                .addComponent(jPVista2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPScroll);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1172, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1538, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPScroll;
    private javax.swing.JPanel jPVista;
    private javax.swing.JPanel jPVista2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTAcuerdo;
    private javax.swing.JTextField jTBanco;
    private javax.swing.JTextField jTCA;
    private javax.swing.JTextField jTCC;
    private javax.swing.JTextField jTCI;
    private javax.swing.JTextField jTCalle;
    private javax.swing.JTextField jTCalleCentral;
    private javax.swing.JTextField jTCaracterDe;
    private javax.swing.JTextField jTCel;
    private javax.swing.JTextField jTCelCentral;
    private javax.swing.JTextField jTCiudad;
    private javax.swing.JTextField jTCiudadCentral;
    private javax.swing.JTextField jTComplemento;
    private javax.swing.JTextField jTComplementoCentral;
    private javax.swing.JTextField jTDepartamento;
    private javax.swing.JTextField jTDepartamentoCentral;
    private javax.swing.JTextField jTDescuento;
    private javax.swing.JTextField jTDescuentoCuota;
    private javax.swing.JTextField jTFecha;
    private javax.swing.JTextField jTLocalidad;
    private javax.swing.JTextField jTLocalidadBanco;
    private javax.swing.JTextField jTMail;
    private javax.swing.JTextField jTNombreComercial;
    private javax.swing.JTextField jTNombreContacto;
    private javax.swing.JTextField jTNumCentral;
    private javax.swing.JTextField jTNumero;
    private javax.swing.JTextField jTPos;
    private javax.swing.JTextField jTRUC;
    private javax.swing.JTextField jTRamoPpal;
    private javax.swing.JTextField jTRazonSocial;
    private javax.swing.JTextField jTSucursal;
    private javax.swing.JTextField jTTel;
    private javax.swing.JTextField jTTelCentral;
    private javax.swing.JTextField jTTitular;
    private javax.swing.JTextField jTUsuario;
    private javax.swing.JTextField jtTelAdmin;
    // End of variables declaration//GEN-END:variables
}
