/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import datatypes.DTFormularioAnda;
import datatypes.DTFormularioCabal;
import datatypes.DTFormularioMaster;
import datatypes.DTImport;
import excepciones.BDException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import logica.ramo;
import logica.ramoAnda;
import logica.ramoCabal;
import logica.ramoMaster;

/**
 *
 * @author Javier
 */
public class persistencia {
    
    private static final String PS_SELECT_VERSION = "SELECT * FROM settings WHERE keyy = 'version'";
    
    //CABAL
    private static final String PS_INSERT_FORMCABAL = "INSERT INTO formulario_cabal (fecha,altaOModificacion,numEstablecimiento,"
            + "nombreFantasia,razonSocial,RUT,tipoRUT,moneda,unificaCheques,tipoDePago,entidadSucursal,sucursal,cuentaNumero, entidadPagadora,calle,numPuerta,apto,"
            + "ciudad,codPostalDepartamento,telefono,pos,ramoPpal,representanteLegal,personaContacto,mail,callePago,numPuertaPago,aptoPago,ciudadPago,codigoPostalDepartamentoPago,"
            + "grupo,subgrupo,observaciones,descuento,descuentoCuota,usuario,horaGuardado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    private static final String PS_UPDATE_FORMCABAL = "UPDATE formulario_cabal SET fecha=?,altaOModificacion=?,numEstablecimiento=?,"
            + "nombreFantasia=?,razonSocial=?,RUT=?,tipoRUT=?,moneda=?,unificaCheques=?,tipoDePago=?,entidadSucursal=?,sucursal=?,cuentaNumero=?,"
            + " entidadPagadora=?,calle=?,numPuerta=?,apto=?,ciudad=?,codPostalDepartamento=?,telefono=?,pos=?,ramoPpal=?,"
            + "representanteLegal=?,personaContacto=?,mail=?,callePago=?,numPuertaPago=?,aptoPago=?,ciudadPago=?,codigoPostalDepartamentoPago=?,"
            + "grupo=?,subgrupo=?,observaciones=?,descuento=?,descuentoCuota=?,usuario=?,horaGuardado=? WHERE idCabal=?";
    
    private static final String PS_DELETE_FORMCABAL = "DELETE FROM formulario_cabal WHERE idCabal = ?";
    
    private static final String PS_SELECT_FORMCABAL = "SELECT * FROM formulario_cabal ORDER BY horaGuardado DESC";
    
    
    //MASTER
    private static final String PS_INSERT_FORMMASTER = "INSERT INTO form_master (pos,entidad,sucursal,"
            + "nombreBanco,tarjetaCredito,maestro,posnetCelular,dia,mes,anio,"
            + "nombreFantasia,razonSocial,calle,numero,ubicacion,codigoPostal,localidad,direccionAdicional,"
            + "telefonos,CUIT,RUT,nombreResponsable,tipoDocumento,documento,ramo,descripcion,mail,tDebito,"
            + "numCuentaDebito,entidadDebito,sucursalDebito,plazoPago,porcDescuento,shopping,zonaComercial,"
            + "tCredito,numCuentaCredito,entidadCredito,sucursalCredito,operaEnDolares,tDolaresCredito,"
            + "numCuentaDolaresCredito,entidadDolaresCredito,sucursalDolaresCredito,cuotasCobroAnticipado,"
            + "plazoPagoPesos,plazoPagoDolares,porcDescuentoPesos,porcDescuentoDolares,horaGuardado, val) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    private static final String PS_UPDATE_FORMMASTER = "UPDATE form_master SET pos=?,entidad=?,sucursal=?,"
            + "nombreBanco=?,tarjetaCredito=?,maestro=?,posnetCelular=?,dia=?,mes=?,anio=?,"
            + "nombreFantasia=?,razonSocial=?,calle=?,numero=?,ubicacion=?,codigoPostal=?,localidad=?,direccionAdicional=?,"
            + "telefonos=?,CUIT=?,RUT=?,nombreResponsable=?,tipoDocumento=?,documento=?,ramo=?,descripcion=?,mail=?,tDebito=?,"
            + "numCuentaDebito=?,entidadDebito=?,sucursalDebito=?,plazoPago=?,porcDescuento=?,shopping=?,zonaComercial=?,"
            + "tCredito=?,numCuentaCredito=?,entidadCredito=?,sucursalCredito=?,operaEnDolares=?,tDolaresCredito=?,"
            + "numCuentaDolaresCredito=?,entidadDolaresCredito=?,sucursalDolaresCredito=?,cuotasCobroAnticipado=?,"
            + "plazoPagoPesos=?,plazoPagoDolares=?,porcDescuentoPesos=?,porcDescuentoDolares=?,horaGuardado=?, val=? WHERE idMaster=?";
    
    private static final String PS_DELETE_FORMMASTER = "DELETE FROM form_master WHERE idMaster = ?";
    
    private static final String PS_SELECT_FORMMASTER = "SELECT * FROM form_master ORDER BY horaGuardado DESC";
    
    
    
    //ANDA
    private static final String PS_INSERT_FORMANDA = "INSERT INTO form_anda (razonSocial,nombreComercial,RUT,"
            + "nombreContacto,cedula,caracterDe,telefono,calle,numero,apto,"
            + "ciudad,departamento,celular,mail,calleCentral,numeroCentral,aptoCentral,ciudadCentral,"
            + "departamentoCentral,telefonoCentral,celularCentral,ramoPpal,acuerdo,pos, fecha, "
            + "cuentaCorriente, cajaAhorro, banco, sucursal, localidad, usuario, descuento, descuentoCuota,horaGuardado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    private static final String PS_UPDATE_FORMANDA = "UPDATE form_anda SET razonSocial=?,nombreComercial=?,RUT=?,"
            + "nombreContacto=?,cedula=?,caracterDe=?,telefono=?,calle=?,numero=?,apto=?,"
            + "ciudad=?,departamento=?,celular=?,mail=?,calleCentral=?,numeroCentral=?,aptoCentral=?,ciudadCentral=?,"
            + "departamentoCentral=?,telefonoCentral=?,celularCentral=?,ramoPpal=?,acuerdo=?,pos=?, fecha=?, "
            + "cuentaCorriente=?, cajaAhorro=?, banco=?, sucursal=?, localidad=?, usuario=?, descuento=?, descuentoCuota=?,"
            + "horaGuardado=? WHERE idAnda=?";
    
    private static final String PS_DELETE_FORMANDA = "DELETE FROM form_anda WHERE idAnda = ?";
    
    private static final String PS_SELECT_FORMANDA = "SELECT * FROM form_anda ORDER BY horaGuardado DESC";
    
    //IMPORTS_LLAMADOS
    private static final String PS_INSERT_IMPORTS_LLAMADOS = "INSERT INTO imports_llamados(`tipo`,"
            + "`nombreFantasia`,`RUT`,`razonSocial`,`tipoCabal`,`calle`,`numero`,"
            + "`apto`,`montevideo`,`codPostal`,`localidad`, `localidadString`, `telefono`,`celular`,"
            + "`pos`,`mail`,`respresentanteLegal`,`documento`,`cajaAhorro`,`pesosSelected`,"
            + "`dolaresSelected`,`pesos`,`dolares`,`banco`, `bancoString`,`ramoCabal`,`ramoMaster`,"
            + "`ramoString`,`color`,comentarios1,comentarios2,`observaciones`,"
            + "estado,`fecha`,`horaGuardado`) VALUES "
            + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    private static final String PS_INSERT_IMPORTS_CABAL = "INSERT INTO imports_cabal(`tipo`,"
            + "`nombreFantasia`,`RUT`,`razonSocial`,`tipoCabal`,`calle`,`numero`,"
            + "`apto`,`montevideo`,`codPostal`,`localidad`, `localidadString`, `telefono`,`celular`,"
            + "`pos`,`mail`,`respresentanteLegal`,`documento`,`cajaAhorro`,`pesosSelected`,"
            + "`dolaresSelected`,`pesos`,`dolares`,`banco`, `bancoString`,`ramoCabal`,`ramoMaster`,"
            + "`ramoString`,`color`,`observaciones`,"
            + "estado,`fecha`,`horaGuardado`) VALUES "
            + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    private static final String PS_DELETE_ALL_IMPORTS_LLAMADOS = "DELETE FROM imports_llamados";
    private static final String PS_RESET_AI = "ALTER TABLE imports_llamados AUTO_INCREMENT = 1";
    
    private static final String PS_SELECT_LLAMADOS = "SELECT * FROM VW_LLAMADOS_MASTER";
    
    private static final String PS_SELECT_PEDIDOS = "SELECT * FROM VW_PEDIDOS_MASTER";
    
    private static final String PS_SELECT_CABAL = "SELECT * FROM VW_LLAMADOS_CABAL";
    
    private static final String PS_SELECT_MASTER_Y_CABAL = "SELECT * FROM VW_MASTER_Y_CABAL";
    
    private static final String PS_SELECT_LLAMADOS_Y_PEDIDOS = "SELECT * FROM VW_LLAMADOS_Y_PEDIDOS_MASTER";
    
    private static final String PS_SELECT_LLAMADOS_Y_PEDIDOS_UNO_POR_FILA = "SELECT * FROM VW_LLAMADOS_Y_PEDIDOS_MASTER a INNER JOIN (SELECT  RUT, COUNT(*) totalCount FROM vw_llamados_y_pedidos_master GROUP BY RUT) b ON a.RUT = b.RUT ORDER BY a.idImports DESC";
    
    private static final String PS_DELETE_IMPORTS_LLAMADOS = "DELETE FROM imports_llamados WHERE idImports=?";
    
    private static final String PS_UPDATE_IMPORTS_LLAMADOS = "UPDATE imports_llamados SET `tipo`=?,"
            + "`nombreFantasia`=?,`RUT`=?,`razonSocial`=?,`tipoCabal`=?,`calle`=?,`numero`=?,"
            + "`apto`=?,`montevideo`=?,`codPostal`=?,`localidad`=?, `localidadString`=?, `telefono`=?,`celular`=?,"
            + "`pos`=?,`mail`=?,`respresentanteLegal`=?,`documento`=?,`cajaAhorro`=?,`pesosSelected`=?,"
            + "`dolaresSelected`=?,`pesos`=?,`dolares`=?,`banco`=?, `bancoString`=?,`ramoCabal`=?,`ramoMaster`=?,"
            + "`ramoString`=?,`color`=?,comentarios1=?,comentarios2=?,`observaciones`=?,"
            + "estado=?,`fecha`=? WHERE `idImports` = ?";
    
    private static final String PS_UPDATE_IMPORTS_CABAL = "UPDATE imports_cabal SET `tipo`=?,"
            + "`nombreFantasia`=?,`RUT`=?,`razonSocial`=?,`tipoCabal`=?,`calle`=?,`numero`=?,"
            + "`apto`=?,`montevideo`=?,`codPostal`=?,`localidad`=?, `localidadString`=?, `telefono`=?,`celular`=?,"
            + "`pos`=?,`mail`=?,`respresentanteLegal`=?,`documento`=?,`cajaAhorro`=?,`pesosSelected`=?,"
            + "`dolaresSelected`=?,`pesos`=?,`dolares`=?,`banco`=?, `bancoString`=?,`ramoCabal`=?,`ramoMaster`=?,"
            + "`ramoString`=?,`color`=?,`observaciones`=?,"
            + "estado=?,`fecha`=? WHERE `idImports` = ?";
    
    private static final String PS_INSERT_USUARIO = "INSERT INTO usuarios (nombrePc,usuario) VALUES (?,?)";
    private static final String PS_SELECT_USUARIO = "SELECT * FROM usuarios WHERE nombrePc = ?";
    private static final String PS_UPDATE_USUARIO = "UPDATE usuarios SET usuario=? WHERE nombrePc=?";
    
    private static final String PS_SELECT_RAMOS_TIPO = "SELECT * FROM ramos WHERE tipo = ? ORDER BY NOMBRE ASC";
    private static final String PS_UPDATE_RAMOS = "UPDATE ramos SET codigo=?, nombre=?, descuento=?, descuentoCuota=? WHERE id=?";
    
    
    // <editor-fold defaultstate="collapsed" desc="Inicialización">
    private static Connection miConexion = null; 
  
    private static Connection getConnection() throws BDException {
        if(miConexion == null) {
            try {
                //REMOTO PRODUCCION
                miConexion = DriverManager.getConnection("jdbc:mysql://200.108.194.122:3306/buycard_formularios", "buycard_user", "lilarossi123");
                //LOCAL DESARROLLO
//                miConexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/buycard_formularios", "root", "root");
            } catch (Exception e) {
                controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                throw new BDException(e.toString());
            }
        }
        return miConexion;
    }
    
    public static void CerrarConexion() throws BDException {
        try {
            miConexion.close();
            miConexion = null;
        } catch (Exception e) {
            throw new BDException(e.toString());
        }
    }
    // </editor-fold>
    
    public static int buscarVersion() throws BDException{
        try{
            PreparedStatement ps = getConnection().prepareStatement(PS_SELECT_VERSION);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                return Integer.valueOf(rs.getString("value"));
            return -1;
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        } catch (SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al buscar versión");
        } finally{
            CerrarConexion();
        }
    }
    
    //USUARIOS
    public static void InsertarUsuario(String nombrePc, String usuario) throws BDException{
        try{
            PreparedStatement ps = getConnection().prepareStatement(PS_INSERT_USUARIO);
            ps.setString(1, nombrePc);
            ps.setString(2, usuario);
            ps.executeUpdate();
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        } catch (SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al guardar usuario " + usuario);
        } finally{
            CerrarConexion();
        }
    }
    
    public static void ModificarUsuario(String nombrePc, String usuario) throws BDException{
        try{
            PreparedStatement ps = getConnection().prepareStatement(PS_UPDATE_USUARIO);
            ps.setString(1, usuario);
            ps.setString(2, nombrePc);
            ps.executeUpdate();
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        } catch (SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al modificar usuario " + usuario);
        } finally{
            CerrarConexion();
        }
    }
    
    public static String buscarUsuario(String nombrePc) throws BDException{
        try{
            PreparedStatement ps = getConnection().prepareStatement(PS_SELECT_USUARIO);
            ps.setString(1, nombrePc);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                return rs.getString(2);
            return null;
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        } catch (SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al buscar usuario de pc " + nombrePc);
        } finally{
            CerrarConexion();
        }
    }
    
    //RAMOS
    public static ArrayList<ramo> buscarRamos(String tipo) throws BDException{
        ArrayList<ramo> lista = new ArrayList();
        try{
            PreparedStatement ps = getConnection().prepareStatement(PS_SELECT_RAMOS_TIPO);
            ps.setString(1, tipo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                ramo r;
                if (tipo.toUpperCase().equals("ANDA")){
                    r = new ramoAnda(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                }else if (tipo.toUpperCase().equals("MASTER")){
                    r = new ramoMaster(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                }else{
                    r = new ramoCabal(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                }
                lista.add(r);
            }
            return lista;
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        } catch (SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al buscar ramos");
        } finally{
            CerrarConexion();
        }
    }
    
    public static void modificarRamo(ramo r) throws BDException{
        PreparedStatement ps;
        try{
            ps = getConnection().prepareStatement(PS_UPDATE_RAMOS);
            ps.setString(1, r.getCodigo());
            ps.setString(2, r.getNombre());
            ps.setString(3, r.getDescuento());
            ps.setString(4, r.getDescuentoCuota());
            ps.setInt(5, r.getId());
            ps.executeUpdate();
        } catch (Exception e){
            if (e instanceof BDException){
                controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                throw (BDException)e;
            }else{
                controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                throw new BDException("Error al intentar modificar el ramo " + r.getNombre());
            }
        } finally{
            CerrarConexion();
        }
    }
    
    //CABAL
    public static int InsertarFormCabal (DTFormularioCabal dt) throws BDException{
        PreparedStatement ps;
        try {
            ps = getConnection().prepareStatement(PS_INSERT_FORMCABAL, Statement.RETURN_GENERATED_KEYS);
            ps.setDate(1, new java.sql.Date(dt.getFecha().getTime()));
            ps.setString(2, dt.getAltaOModificacion());
            ps.setString(3, dt.getNumEstablecimiento());
            ps.setString(4, dt.getNombreFantasia());
            ps.setString(5, dt.getRazonSocial());
            ps.setString(6, dt.getRUT());
            ps.setString(7, dt.getTipoRUT());
            ps.setString(8, dt.getMoneda());
            ps.setString(9, dt.getUnificaCheques());
            ps.setString(10, dt.getTipoDePago());
            ps.setString(11, dt.getEntidadSucursal());
            ps.setString(12, dt.getSucursal());
            ps.setString(13, dt.getCuentaNumero());
            ps.setString(14, dt.getEntidadPagadora());
            ps.setString(15, dt.getCalle());
            ps.setString(16, dt.getNumPuerta());
            ps.setString(17, dt.getApto());
            ps.setString(18, dt.getCiudad());
            ps.setString(19, dt.getCodPostalDepartamento());
            ps.setString(20, dt.getTelefono());
            ps.setString(21, dt.getPos());
            ps.setString(22, dt.getRamoPpal());
            ps.setString(23, dt.getRepresentanteLegal());
            ps.setString(24, dt.getPersonaContacto());
            ps.setString(25, dt.getMail());
            ps.setString(26, dt.getCallePago());
            ps.setString(27, dt.getNumPuertaPago());
            ps.setString(28, dt.getAptoPago());
            ps.setString(29, dt.getCiudadPago());
            ps.setString(30, dt.getCodigoPostalDepartamentoPago());
            ps.setString(31, dt.getGrupo());
            ps.setString(32, dt.getSubgrupo());
            ps.setString(33, dt.getObservaciones());
            ps.setString(34, dt.getDescuento());
            ps.setString(35, dt.getDescuentoCuota());
            ps.setString(36, dt.getUsuario());
            ps.setTimestamp(37, new Timestamp(new java.util.Date().getTime()));
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            return rs.getInt(1);
        } catch (Exception e){
            if (e instanceof BDException){
                controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                throw (BDException)e;
            }else{
                controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
                throw new BDException("Error al intentar guardar el formulario de cabal con RUT: " + dt.getRUT());
            }
        } finally{
            CerrarConexion();
        }
    }
    
    public static void ModificarFormCabal (DTFormularioCabal dt) throws BDException{
        PreparedStatement ps;
        try{
            ps = getConnection().prepareStatement(PS_UPDATE_FORMCABAL);
            ps.setDate(1, new Date(dt.getFecha().getTime()));
            ps.setString(2, dt.getAltaOModificacion());
            ps.setString(3, dt.getNumEstablecimiento());
            ps.setString(4, dt.getNombreFantasia());
            ps.setString(5, dt.getRazonSocial());
            ps.setString(6, dt.getRUT());
            ps.setString(7, dt.getTipoRUT());
            ps.setString(8, dt.getMoneda());
            ps.setString(9, dt.getUnificaCheques());
            ps.setString(10, dt.getTipoDePago());
            ps.setString(11, dt.getEntidadSucursal());
            ps.setString(12, dt.getSucursal());
            ps.setString(13, dt.getCuentaNumero());
            ps.setString(14, dt.getEntidadPagadora());
            ps.setString(15, dt.getCalle());
            ps.setString(16, dt.getNumPuerta());
            ps.setString(17, dt.getApto());
            ps.setString(18, dt.getCiudad());
            ps.setString(19, dt.getCodPostalDepartamento());
            ps.setString(20, dt.getTelefono());
            ps.setString(21, dt.getPos());
            ps.setString(22, dt.getRamoPpal());
            ps.setString(23, dt.getRepresentanteLegal());
            ps.setString(24, dt.getPersonaContacto());
            ps.setString(25, dt.getMail());
            ps.setString(26, dt.getCallePago());
            ps.setString(27, dt.getNumPuertaPago());
            ps.setString(28, dt.getAptoPago());
            ps.setString(29, dt.getCiudadPago());
            ps.setString(30, dt.getCodigoPostalDepartamentoPago());
            ps.setString(31, dt.getGrupo());
            ps.setString(32, dt.getSubgrupo());
            ps.setString(33, dt.getObservaciones());
            ps.setString(34, dt.getDescuento());
            ps.setString(35, dt.getDescuentoCuota());
            ps.setString(36, dt.getUsuario());
            ps.setTimestamp(37, new Timestamp(new java.util.Date().getTime()));
            ps.setInt(38, dt.getId());
            ps.executeUpdate();
        } catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al intentar modificar el formulario de cabal con RUT: " + dt.getRUT());
            }
        } finally{
            CerrarConexion();
        }
    }
    
    public static ArrayList<DTFormularioCabal> buscarCabal() throws BDException{
        ArrayList<DTFormularioCabal> lista = new ArrayList();
        PreparedStatement ps;
        int fila = 0;
        try {
            ps = getConnection().prepareStatement(PS_SELECT_FORMCABAL);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                fila ++;
                DTFormularioCabal dtC = new DTFormularioCabal(rs.getInt(1), rs.getDate(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12),
                        rs.getString(13), rs.getString(14), rs.getString(15), rs.getString(16), rs.getString(17),
                        rs.getString(18), rs.getString(19), rs.getString(20), rs.getString(21), rs.getString(22),
                        rs.getString(23), rs.getString(24), rs.getString(25), rs.getString(26), rs.getString(27),
                        rs.getString(28), rs.getString(29), rs.getString(30), rs.getString(31), rs.getString(32),
                        rs.getString(33), rs.getString(34), rs.getString(35), rs.getString(36),
                        rs.getString(37), rs.getTimestamp(38));
                lista.add(dtC);
            }
            return lista;
        }catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al buscar formularios guardados de cabal. Fila: " + fila);
            }
        } finally{
            CerrarConexion();
        }
    }
    
    public static void borrarCabal(int idCabal) throws BDException{
        PreparedStatement ps;
        try{
            ps = getConnection().prepareStatement(PS_DELETE_FORMCABAL);
            ps.setInt(1, idCabal);
            ps.executeUpdate();
        }catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al borrar formulario cabal");
            }
        } finally{
            CerrarConexion();
        }
    }
    
    
    //MASTER
    public static int InsertarFormMaster (DTFormularioMaster dt) throws BDException{
        PreparedStatement ps;
        try {
            ps = getConnection().prepareStatement(PS_INSERT_FORMMASTER, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, dt.getPos());
            ps.setString(2, dt.getEntidad());
            ps.setString(3, dt.getSucursal());
            ps.setString(4, dt.getNombreBanco());
            ps.setString(5, dt.getTarjetaCredito());
            ps.setString(6, dt.getMaestro());
            ps.setString(7, dt.getPosnetCelular());
            ps.setString(8, dt.getDia());
            ps.setString(9, dt.getMes());
            ps.setString(10, dt.getAnio());
            ps.setString(11, dt.getNombreFantasia());
            ps.setString(12, dt.getRazonSocial());
            ps.setString(13, dt.getCalle());
            ps.setString(14, dt.getNumero());
            ps.setString(15, dt.getUbicacion());
            ps.setString(16, dt.getCodigoPostal());
            ps.setString(17, dt.getLocalidad());
            ps.setString(18, dt.getDireccionAdicional());
            ps.setString(19, dt.getTelefonos());
            ps.setString(20, dt.getCUIT());
            ps.setString(21, dt.getRUT());
            ps.setString(22, dt.getNombreResponsable());
            ps.setString(23, dt.getTipoDocumento());
            ps.setString(24, dt.getDocumento());
            ps.setString(25, dt.getRamo());
            ps.setString(26, dt.getDescripcion());
            ps.setString(27, dt.getMail());
            ps.setString(28, dt.gettDebito());
            ps.setString(29, dt.getNumCuentaDebito());
            ps.setString(30, dt.getEntidadDebito());
            ps.setString(31, dt.getSucursalDebito());
            ps.setString(32, dt.getPlazoPago());
            ps.setString(33, dt.getPorcDescuento());
            ps.setString(34, dt.getShopping());
            ps.setString(35, dt.getZonaComercial());
            ps.setString(36, dt.gettCredito());
            ps.setString(37, dt.getNumCuentaCredito());
            ps.setString(38, dt.getEntidadCredito());
            ps.setString(39, dt.getSucursalCredito());
            ps.setString(40, dt.getOperaEnDolares());
            ps.setString(41, dt.gettDolaresCredito());
            ps.setString(42, dt.getNumCuentaDolaresCredito());
            ps.setString(43, dt.getEntidadDolaresCredito());
            ps.setString(44, dt.getSucursalDolaresCredito());
            ps.setString(45, dt.getCuotasCobroAnticipado());
            ps.setString(46, dt.getPlazoPagoPesos());
            ps.setString(47, dt.getPlazoPagoDolares());
            ps.setString(48, dt.getPorcDescuentoPesos());
            ps.setString(49, dt.getPorcDescuentoDolares());
            ps.setTimestamp(50, new Timestamp(new java.util.Date().getTime()));
            ps.setBoolean(51, dt.isVal());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            return rs.getInt(1);
        } catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al intentar guardar el formulario de master con RUT " + dt.getRUT());
            }
        } finally{
            CerrarConexion();
        }
    }
    
    public static void ModificarFormMaster (DTFormularioMaster dt) throws BDException{
        PreparedStatement ps;
        try{
            ps = getConnection().prepareStatement(PS_UPDATE_FORMMASTER);
            ps.setString(1, dt.getPos());
            ps.setString(2, dt.getEntidad());
            ps.setString(3, dt.getSucursal());
            ps.setString(4, dt.getNombreBanco());
            ps.setString(5, dt.getTarjetaCredito());
            ps.setString(6, dt.getMaestro());
            ps.setString(7, dt.getPosnetCelular());
            ps.setString(8, dt.getDia());
            ps.setString(9, dt.getMes());
            ps.setString(10, dt.getAnio());
            ps.setString(11, dt.getNombreFantasia());
            ps.setString(12, dt.getRazonSocial());
            ps.setString(13, dt.getCalle());
            ps.setString(14, dt.getNumero());
            ps.setString(15, dt.getUbicacion());
            ps.setString(16, dt.getCodigoPostal());
            ps.setString(17, dt.getLocalidad());
            ps.setString(18, dt.getDireccionAdicional());
            ps.setString(19, dt.getTelefonos());
            ps.setString(20, dt.getCUIT());
            ps.setString(21, dt.getRUT());
            ps.setString(22, dt.getNombreResponsable());
            ps.setString(23, dt.getTipoDocumento());
            ps.setString(24, dt.getDocumento());
            ps.setString(25, dt.getRamo());
            ps.setString(26, dt.getDescripcion());
            ps.setString(27, dt.getMail());
            ps.setString(28, dt.gettDebito());
            ps.setString(29, dt.getNumCuentaDebito());
            ps.setString(30, dt.getEntidadDebito());
            ps.setString(31, dt.getSucursalDebito());
            ps.setString(32, dt.getPlazoPago());
            ps.setString(33, dt.getPorcDescuento());
            ps.setString(34, dt.getShopping());
            ps.setString(35, dt.getZonaComercial());
            ps.setString(36, dt.gettCredito());
            ps.setString(37, dt.getNumCuentaCredito());
            ps.setString(38, dt.getEntidadCredito());
            ps.setString(39, dt.getSucursalCredito());
            ps.setString(40, dt.getOperaEnDolares());
            ps.setString(41, dt.gettDolaresCredito());
            ps.setString(42, dt.getNumCuentaDolaresCredito());
            ps.setString(43, dt.getEntidadDolaresCredito());
            ps.setString(44, dt.getSucursalDolaresCredito());
            ps.setString(45, dt.getCuotasCobroAnticipado());
            ps.setString(46, dt.getPlazoPagoPesos());
            ps.setString(47, dt.getPlazoPagoDolares());
            ps.setString(48, dt.getPorcDescuentoPesos());
            ps.setString(49, dt.getPorcDescuentoDolares());
            ps.setTimestamp(50, new Timestamp(new java.util.Date().getTime()));
            ps.setBoolean(51, dt.isVal());
            ps.setInt(52, dt.getId());
            ps.executeUpdate();
        } catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al intentar modificar el formulario de master con RUT " + dt.getRUT());
            }
        } finally{
            CerrarConexion();
        }
    }
    
    public static ArrayList<DTFormularioMaster> buscarMaster() throws BDException{
        ArrayList<DTFormularioMaster> lista = new ArrayList();
        PreparedStatement ps;
        int fila = 0;
        try {
            ps = getConnection().prepareStatement(PS_SELECT_FORMMASTER);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                fila++;
                DTFormularioMaster dtM = new DTFormularioMaster(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13),
                        rs.getString(14), rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18),
                        rs.getString(19), rs.getString(20), rs.getString(21), rs.getString(22), rs.getString(23),
                        rs.getString(24), rs.getString(25), rs.getString(26), rs.getString(27), rs.getString(28),
                        rs.getString(29), rs.getString(30), rs.getString(31), rs.getString(32), rs.getString(33),
                        rs.getString(34), rs.getString(35), rs.getString(36), rs.getString(37), rs.getString(38),
                        rs.getString(39), rs.getString(40), rs.getString(41), rs.getString(42), rs.getString(43),
                        rs.getString(44), rs.getString(45), rs.getString(46), rs.getString(47), rs.getString(48),
                        rs.getString(49), rs.getString(50), rs.getTimestamp(51), rs.getBoolean(52));
                lista.add(dtM);
            }
            return lista;
        }catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al buscar formularios guardados master. Fila: "+ fila);
            }
        } finally{
            CerrarConexion();
        }
    }
    
    public static void borrarMaster(int idMaster) throws BDException{
        PreparedStatement ps;
        try{
            ps = getConnection().prepareStatement(PS_DELETE_FORMMASTER);
            ps.setInt(1, idMaster);
            ps.executeUpdate();
        }catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al borrar formulario master");
            }
        } finally{
            CerrarConexion();
        }
    }
    
    
    //ANDA
    public static int InsertarFormAnda (DTFormularioAnda dt) throws BDException{
        PreparedStatement ps;
        try {
            ps = getConnection().prepareStatement(PS_INSERT_FORMANDA, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, dt.getRazonSocial());
            ps.setString(2, dt.getNombreComercial());
            ps.setString(3, dt.getRUT());
            ps.setString(4, dt.getNombreContacto());
            ps.setString(5, dt.getCedula());
            ps.setString(6, dt.getCaracterDe());
            ps.setString(7, dt.getTelefono());
            ps.setString(8, dt.getCalle());
            ps.setString(9, dt.getNumero());
            ps.setString(10, dt.getApto());
            ps.setString(11, dt.getCiudad());
            ps.setString(12, dt.getDepartamento());
            ps.setString(13, dt.getCelular());
            ps.setString(14, dt.getMail());
            ps.setString(15, dt.getCalleCentral());
            ps.setString(16, dt.getNumeroCentral());
            ps.setString(17, dt.getAptoCentral());
            ps.setString(18, dt.getCiudadCentral());
            ps.setString(19, dt.getDepartamentoCentral());
            ps.setString(20, dt.getTelefonoCentral());
            ps.setString(21, dt.getCelularCentral());
            ps.setString(22, dt.getRamoPpal());
            ps.setString(23, dt.getAcuerdo());
            ps.setString(24, dt.getPOS());
            ps.setString(25, dt.getFecha());
            ps.setString(26, dt.getCuentaCorriente());
            ps.setString(27, dt.getCajaAhorro());
            ps.setString(28, dt.getBanco());
            ps.setString(29, dt.getSucursal());
            ps.setString(30, dt.getLocalidad());
            ps.setString(31, dt.getUsuario());
            ps.setString(32, dt.getDescuento());
            ps.setString(33, dt.getDescuentoCuota());
            ps.setTimestamp(34, new Timestamp(new java.util.Date().getTime()));
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            return rs.getInt(1);
        } catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al intentar guardar el formulario de anda con RUT " + dt.getRUT());
            }
        } finally{
            CerrarConexion();
        }
    }
    
    public static void ModificarFormAnda (DTFormularioAnda dt) throws BDException{
        PreparedStatement ps;
        try{
            ps = getConnection().prepareStatement(PS_UPDATE_FORMANDA);
            ps.setString(1, dt.getRazonSocial());
            ps.setString(2, dt.getNombreComercial());
            ps.setString(3, dt.getRUT());
            ps.setString(4, dt.getNombreContacto());
            ps.setString(5, dt.getCedula());
            ps.setString(6, dt.getCaracterDe());
            ps.setString(7, dt.getTelefono());
            ps.setString(8, dt.getCalle());
            ps.setString(9, dt.getNumero());
            ps.setString(10, dt.getApto());
            ps.setString(11, dt.getCiudad());
            ps.setString(12, dt.getDepartamento());
            ps.setString(13, dt.getCelular());
            ps.setString(14, dt.getMail());
            ps.setString(15, dt.getCalleCentral());
            ps.setString(16, dt.getNumeroCentral());
            ps.setString(17, dt.getAptoCentral());
            ps.setString(18, dt.getCiudadCentral());
            ps.setString(19, dt.getDepartamentoCentral());
            ps.setString(20, dt.getTelefonoCentral());
            ps.setString(21, dt.getCelularCentral());
            ps.setString(22, dt.getRamoPpal());
            ps.setString(23, dt.getAcuerdo());
            ps.setString(24, dt.getPOS());
            ps.setString(25, dt.getFecha());
            ps.setString(26, dt.getCuentaCorriente());
            ps.setString(27, dt.getCajaAhorro());
            ps.setString(28, dt.getBanco());
            ps.setString(29, dt.getSucursal());
            ps.setString(30, dt.getLocalidad());
            ps.setString(31, dt.getUsuario());
            ps.setString(32, dt.getDescuento());
            ps.setString(33, dt.getDescuentoCuota());
            ps.setTimestamp(34, new Timestamp(new java.util.Date().getTime()));
            ps.setInt(35, dt.getId());
            ps.executeUpdate();
        } catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al intentar modificar el formulario de anda con RUT " + dt.getRUT());
            }
        } finally{
            CerrarConexion();
        }
    }
    
    public static ArrayList<DTFormularioAnda> buscarAnda() throws BDException{
        ArrayList<DTFormularioAnda> lista = new ArrayList();
        PreparedStatement ps;
        int fila = 0;
        try {
            ps = getConnection().prepareStatement(PS_SELECT_FORMANDA);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                fila++;
                DTFormularioAnda dtA = new DTFormularioAnda(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13),
                        rs.getString(14), rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18),
                        rs.getString(19), rs.getString(20), rs.getString(21), rs.getString(22), rs.getString(23),
                        rs.getString(24), rs.getString(25), rs.getString(26), rs.getString(27), rs.getString(28),
                        rs.getString(29), rs.getString(30), rs.getString(31), rs.getString(32), rs.getString(33),
                        rs.getString(34), rs.getString(35), rs.getString(36), rs.getTimestamp(37));
                lista.add(dtA);
            }
            return lista;
        }catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al buscar formularios anda. Fila: " + fila);
            }
        } finally{
            CerrarConexion();
        }
    }
    
    public static void borrarAnda(int idAnda) throws BDException{
        PreparedStatement ps;
        try{
            ps = getConnection().prepareStatement(PS_DELETE_FORMANDA);
            ps.setInt(1, idAnda);
            ps.executeUpdate();
        }catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al borrar formulario anda");
            }
        } finally{
            CerrarConexion();
        }
    }
    
    //IMPORTS
    public static int InsertarImport (DTImport dt) throws BDException{
        PreparedStatement ps;
        try {
            ps = getConnection().prepareStatement(PS_INSERT_IMPORTS_LLAMADOS, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, dt.getTipo());
            ps.setString(2, dt.getNombreFantasia());
            ps.setString(3, dt.getRUT());
            ps.setString(4, dt.getRazonSocial());
            ps.setString(5, dt.getTipoCabal());
            ps.setString(6, dt.getCalle());
            ps.setString(7, dt.getNumero());
            ps.setString(8, dt.getApto());
            ps.setBoolean(9, dt.isMontevideo());
            ps.setString(10, dt.getCodPostal());
            ps.setInt(11, dt.getLocalidad());
            ps.setString(12, dt.getLocalidadString());
            ps.setString(13, dt.getTelefono());
            ps.setString(14, dt.getCelular());
            ps.setString(15, dt.getPos());
            ps.setString(16, dt.getMail());
            ps.setString(17, dt.getRepresentanteLegal());
            ps.setString(18, dt.getDocumento());
            ps.setBoolean(19, dt.isCajaAhorro());
            ps.setBoolean(20, dt.isPesosSelected());
            ps.setBoolean(21, dt.isDolaresSelected());
            ps.setString(22, dt.getPesos());
            ps.setString(23, dt.getDolares());
            ps.setInt(24, dt.getBanco());
            ps.setString(25, dt.getBancoString());
            ps.setInt(26, dt.getRamoCabal());
            ps.setInt(27, dt.getRamoMaster());
            ps.setString(28, dt.getRamoString());
            ps.setInt(29, dt.getColor());
            ps.setString(30, dt.getComentarios1());
            ps.setString(31, dt.getComentarios2());
            ps.setString(32, dt.getObservaciones());
            ps.setString(33, dt.getEstado());
            ps.setString(34, dt.getFecha());
            ps.setTimestamp(35, new Timestamp(new java.util.Date().getTime()));
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            return rs.getInt(1);
        } catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        } catch (SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al guardar datos importados Master " + dt.getNombreFantasia());
        } finally{
            CerrarConexion();
        }
    }
    
    public static void borrarTodosImport() throws BDException{
        PreparedStatement ps;
        PreparedStatement ps2;
        try{
            ps = getConnection().prepareStatement(PS_DELETE_ALL_IMPORTS_LLAMADOS);
            ps2 = getConnection().prepareStatement(PS_RESET_AI);
            ps.executeUpdate();
            ps2.executeUpdate();
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        }catch (SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al borrar imports");
        } finally{
            CerrarConexion();
        }
    }
    
    public static ArrayList<DTImport> buscarTodosLlamados() throws BDException{
        ArrayList<DTImport> lista = new ArrayList();
        PreparedStatement ps;
        int fila = 0;
        try {
            ps = getConnection().prepareStatement(PS_SELECT_LLAMADOS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                fila++;
                DTImport dt = new DTImport(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getBoolean(10), rs.getString(11), rs.getInt(12), rs.getString(13), rs.getString(14),
                        rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18), rs.getString(19),
                        rs.getBoolean(20), rs.getBoolean(21), rs.getBoolean(22), rs.getString(23), rs.getString(24),
                        rs.getInt(25), rs.getString(26), rs.getInt(27), rs.getInt(28), rs.getString(29), rs.getInt(30),
                        rs.getString(31), rs.getString(32), rs.getString(33), rs.getString(34), rs.getString(35), rs.getTimestamp(36));
                lista.add(dt);
            }
            return lista;
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        }catch(SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al buscar llamados. Fila: " + fila);
        } finally{
            CerrarConexion();
        }
    }
    
    public static ArrayList<DTImport> buscarTodosPedidos() throws BDException{
        ArrayList<DTImport> lista = new ArrayList();
        PreparedStatement ps;
        int fila = 0;
        try {
            ps = getConnection().prepareStatement(PS_SELECT_PEDIDOS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                fila++;
                DTImport dt = new DTImport(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getBoolean(10), rs.getString(11), rs.getInt(12), rs.getString(13), rs.getString(14),
                        rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18), rs.getString(19),
                        rs.getBoolean(20), rs.getBoolean(21), rs.getBoolean(22), rs.getString(23), rs.getString(24),
                        rs.getInt(25), rs.getString(26), rs.getInt(27), rs.getInt(28), rs.getString(29), rs.getInt(30),
                        rs.getString(31), rs.getString(32), rs.getString(33), rs.getString(34), rs.getString(35), rs.getTimestamp(36));
                lista.add(dt);
            }
            return lista;
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        }catch(SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al buscar llamados. Fila: " + fila);
        } finally{
            CerrarConexion();
        }
    }
    
    public static ArrayList<DTImport> buscarTodosCabal() throws BDException{
        ArrayList<DTImport> lista = new ArrayList();
        PreparedStatement ps;
        int fila = 0;
        try {
            ps = getConnection().prepareStatement(PS_SELECT_CABAL);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                fila++;
                DTImport dt = new DTImport(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getBoolean(10), rs.getString(11), rs.getInt(12), rs.getString(13), rs.getString(14),
                        rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18), rs.getString(19),
                        rs.getBoolean(20), rs.getBoolean(21), rs.getBoolean(22), rs.getString(23), rs.getString(24),
                        rs.getInt(25), rs.getString(26), rs.getInt(27), rs.getInt(28), rs.getString(29), rs.getInt(30),
                        rs.getString(31), rs.getString(32), rs.getString(33), rs.getString(34), rs.getString(35), rs.getTimestamp(36));
                lista.add(dt);
            }
            return lista;
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        }catch(SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al buscar llamados Cabal. Fila: " + fila);
        } finally{
            CerrarConexion();
        }
    }
    
    public static ArrayList<DTImport> buscarTodosMasterYCabal() throws BDException{
        ArrayList<DTImport> lista = new ArrayList();
        PreparedStatement ps;
        int fila = 0;
        try {
            ps = getConnection().prepareStatement(PS_SELECT_MASTER_Y_CABAL);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                fila++;
                DTImport dt = new DTImport(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getBoolean(10), rs.getString(11), rs.getInt(12), rs.getString(13), rs.getString(14),
                        rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18), rs.getString(19),
                        rs.getBoolean(20), rs.getBoolean(21), rs.getBoolean(22), rs.getString(23), rs.getString(24),
                        rs.getInt(25), rs.getString(26), rs.getInt(27), rs.getInt(28), rs.getString(29), rs.getInt(30),
                        rs.getString(31), rs.getString(32), rs.getString(33), rs.getString(34), rs.getString(35), rs.getTimestamp(36));
                lista.add(dt);
            }
            return lista;
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        }catch(SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            System.out.println(e);
            throw new BDException("Error al buscar llamados Master y Cabal. Fila: " + fila);
        } finally{
            CerrarConexion();
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="DEJANDO UNA FILA POR RUT">
//    public static ArrayList<ArrayList<>> buscarTodosLlamadosYPedidosUnoPorFila() throws BDException{
//        ArrayList<ArrayList<>> lista = new ArrayList();
//        PreparedStatement ps;
//        int fila = 0;
//        try {
//            ps = getConnection().prepareStatement(PS_SELECT_LLAMADOS_Y_PEDIDOS);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()){
//                fila++;
//                int it = rs.getInt(38);
//                ArrayList<> listaInterna = new ArrayList();
//                for (int i = 0; i < it; i++){
//                     dt = new (rs.getInt(1), rs.getString(2), rs.getString(3),
//                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
//                        rs.getString(9), rs.getBoolean(10), rs.getString(11), rs.getInt(12), rs.getString(13), rs.getString(14),
//                        rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18), rs.getString(19),
//                        rs.getBoolean(20), rs.getBoolean(21), rs.getBoolean(22), rs.getString(23), rs.getString(24),
//                        rs.getInt(25), rs.getString(26), rs.getInt(27), rs.getInt(28), rs.getString(29), rs.getInt(30),
//                        rs.getString(31), rs.getString(32), rs.getString(33), rs.getString(34), rs.getString(35), rs.getTimestamp(36));
//                    listaInterna.add(dt);
//                    rs.next();
//                }
//                lista.add(listaInterna);
//            }
//            return lista;
//        }catch (BDException e){
//            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
//            throw e;
//        }catch(SQLException e){
//            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
//            throw new BDException("Error al buscar llamados y pedidos. Fila: " + fila);
//        } finally{
//            CerrarConexion();
//        }
//    }
// </editor-fold>
    
    //    <editor-fold defaultstate="collapsed" desc="DEJANDO TODAS LAS FILAS">
    public static ArrayList<DTImport> buscarTodosLlamadosYPedidos() throws BDException{
        ArrayList<DTImport> lista = new ArrayList();
        PreparedStatement ps;
        int fila = 0;
        try {
            ps = getConnection().prepareStatement(PS_SELECT_LLAMADOS_Y_PEDIDOS);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                fila++;
                DTImport dt = new DTImport(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                        rs.getString(9), rs.getBoolean(10), rs.getString(11), rs.getInt(12), rs.getString(13), rs.getString(14),
                        rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18), rs.getString(19),
                        rs.getBoolean(20), rs.getBoolean(21), rs.getBoolean(22), rs.getString(23), rs.getString(24),
                        rs.getInt(25), rs.getString(26), rs.getInt(27), rs.getInt(28), rs.getString(29), rs.getInt(30),
                        rs.getString(31), rs.getString(32), rs.getString(33), rs.getString(34), rs.getString(35), rs.getTimestamp(36));
                lista.add(dt);
            }
            return lista;
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        }catch(SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al buscar llamados y pedidos. Fila: " + fila);
        } finally{
            CerrarConexion();
        }
    }
// </editor-fold>
    
    public static void borrarImport(DTImport dt) throws BDException{
        PreparedStatement ps;
        try{
            ps = getConnection().prepareStatement(PS_DELETE_IMPORTS_LLAMADOS);
            ps.setInt(1, dt.getId());
            ps.executeUpdate();
        }catch (BDException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw e;
        }catch (SQLException e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            throw new BDException("Error al borrar imports");
        } finally{
            CerrarConexion();
        }
    }
    
    public static void ModificarImport (DTImport dt) throws BDException{
        PreparedStatement ps;
        try{
            ps = getConnection().prepareStatement(PS_UPDATE_IMPORTS_LLAMADOS);
            ps.setString(1, dt.getTipo());
            ps.setString(2, dt.getNombreFantasia());
            ps.setString(3, dt.getRUT());
            ps.setString(4, dt.getRazonSocial());
            ps.setString(5, dt.getTipoCabal());
            ps.setString(6, dt.getCalle());
            ps.setString(7, dt.getNumero());
            ps.setString(8, dt.getApto());
            ps.setBoolean(9, dt.isMontevideo());
            ps.setString(10, dt.getCodPostal());
            ps.setInt(11, dt.getLocalidad());
            ps.setString(12, dt.getLocalidadString());
            ps.setString(13, dt.getTelefono());
            ps.setString(14, dt.getCelular());
            ps.setString(15, dt.getPos());
            ps.setString(16, dt.getMail());
            ps.setString(17, dt.getRepresentanteLegal());
            ps.setString(18, dt.getDocumento());
            ps.setBoolean(19, dt.isCajaAhorro());
            ps.setBoolean(20, dt.isPesosSelected());
            ps.setBoolean(21, dt.isDolaresSelected());
            ps.setString(22, dt.getPesos());
            ps.setString(23, dt.getDolares());
            ps.setInt(24, dt.getBanco());
            ps.setString(25, dt.getBancoString());
            ps.setInt(26, dt.getRamoCabal());
            ps.setInt(27, dt.getRamoMaster());
            ps.setString(28, dt.getRamoString());
            ps.setInt(29, dt.getColor());
            ps.setString(30, dt.getComentarios1());
            ps.setString(31, dt.getComentarios2());
            ps.setString(32, dt.getObservaciones());
            ps.setString(33, dt.getEstado());
            ps.setString(34, dt.getFecha());
            ps.setInt(35, dt.getId());
            ps.executeUpdate();
        } catch (Exception e){
            controladores.ControladorExceptions.logErrorsStatic(e.toString(), e);
            if (e instanceof BDException){
                throw (BDException)e;
            }else{
                throw new BDException("Error al intentar modificar Master RUT: " + dt.getRUT());
            }
        } finally{
            CerrarConexion();
        }
    }
    
}
