/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manejadores;

import java.util.ArrayList;
import logica.bancoMontevideo;
import logica.bancoInterior;
import logica.ciudad;
import logica.ramoAnda;
import logica.ramoCabal;
import logica.ramoMaster;

/**
 *
 * @author Javier
 */
public class manejador {
    private static manejador instance = null;
    private ArrayList<bancoMontevideo> listaBancosMontevideo;
    private ArrayList<bancoInterior> listaBancosInterior;
    private ArrayList<ramoMaster> listaRamosMaster;
    private ArrayList<ramoCabal> listaRamosCabal;
    private ArrayList<ramoAnda> listaRamosAnda;
    private ArrayList<ciudad> listaCiudades;
    
    public static manejador getInstance() {
       if(instance == null) {
          instance = new manejador();
       }
       return instance;
    }
    
    public manejador() {
        this.listaBancosMontevideo = new ArrayList();
        this.listaBancosInterior = new ArrayList();
        this.listaRamosMaster = new ArrayList();
        this.listaRamosCabal = new ArrayList();
        this.listaRamosAnda = new ArrayList();
        this.listaCiudades = new ArrayList();
    }
    
    public void add(bancoMontevideo banco){
        listaBancosMontevideo.add(banco);
    }
    
    public void add(ramoMaster ramo){
        listaRamosMaster.add(ramo);
    }
    
    public void add(ramoAnda ramo){
        listaRamosAnda.add(ramo);
    }
    
    public void remove(ramoMaster ramo){
        listaRamosMaster.remove(ramo);
    }
    
    public void remove(bancoMontevideo banco){
        listaBancosMontevideo.remove(banco);
    }
    
    public void add(bancoInterior banco){
        listaBancosInterior.add(banco);
    }
    
    public void remove(bancoInterior banco){
        listaBancosInterior.remove(banco);
    }
    
    public void add(ramoCabal ramo){
        listaRamosCabal.add(ramo);
    }
    
    public void remove(ramoCabal ramo){
        listaRamosCabal.remove(ramo);
    }
    
    public void remove(ramoAnda ramo){
        listaRamosAnda.remove(ramo);
    }
    
    public void add(ciudad city){
        listaCiudades.add(city);
    }
    
    public void remove(ciudad city){
        listaCiudades.remove(city);
    }

    public ArrayList<bancoMontevideo> getListaBancosMontevideo() {
        return listaBancosMontevideo;
    }

    public ArrayList<bancoInterior> getListaBancosInterior() {
        return listaBancosInterior;
    }

    public ArrayList<ramoMaster> getListaRamosMaster() {
        return listaRamosMaster;
    }

    public ArrayList<ramoCabal> getListaRamosCabal() {
        return listaRamosCabal;
    }
    
    public ArrayList<ramoAnda> getListaRamosAnda() {
        return listaRamosAnda;
    }

    public ArrayList<ciudad> getListaCiudades() {
        return listaCiudades;
    }
    
}
