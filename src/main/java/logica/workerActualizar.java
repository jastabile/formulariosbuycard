/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import javax.swing.JDialog;
import javax.swing.SwingWorker;
import presentacion.jIFConfiguracion;
import presentacion.jIFImport;
import presentacion.jIFOpen;

/**
 *
 * @author Javier
 */
public class workerActualizar extends SwingWorker<Void, Void> {
    
    private JDialog jd;
    private jIFImport jifimport;
    private jIFOpen jifopen;
    private jIFConfiguracion jifConfig;
    private boolean importando;

    public workerActualizar(JDialog jd, jIFImport ji, boolean importando) {
        this.jd = jd;
        this.jifimport = ji;
        this.jifopen = null;
        this.jifConfig = null;
        this.importando = importando;
    }

    public workerActualizar(JDialog jd, jIFOpen jifopen) {
        this.jd = jd;
        this.jifopen = jifopen;
        this.jifimport = null;
        this.jifConfig = null;
    }
    
    public workerActualizar(JDialog jd, jIFConfiguracion jifconfig) {
        this.jd = jd;
        this.jifopen = null;
        this.jifimport = null;
        this.jifConfig = jifconfig;
    }

    @Override
    protected Void doInBackground() throws Exception {
        if (jifimport != null)
            jifimport.actualizarTablas(importando);
        else if (jifopen != null)
            jifopen.recargar();
        else if (jifConfig != null)
            jifConfig.actualizarTablas();
        return null;
    }

    @Override
    protected void done() {
      jd.dispose();
   }
    
}
