/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

/**
 *
 * @author Javier
 */
public class bancoInterior extends banco{
    private String ciudad;

    public bancoInterior(String ciudad, String nombre, String sucursal, String direccion) {
        super(nombre, sucursal, direccion);
        this.ciudad = ciudad;
    }

    public String getCiudad() {
        return ciudad;
    } 

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
    
}
