/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

/**
 *
 * @author Javier
 */
public class bancoMontevideo extends banco{
    private String zona;
    private String nombreSucursal;

    public bancoMontevideo(String zona, String nombreSucursal, String nombre, String sucursal, String direccion) {
        super(nombre, sucursal, direccion);
        this.zona = zona;
        this.nombreSucursal = nombreSucursal;
    }

    public String getZona() {
        return zona;
    }

    public String getNombreSucursal() {
        return nombreSucursal;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }    
}
