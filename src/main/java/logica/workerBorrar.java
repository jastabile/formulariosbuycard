/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingWorker;
import presentacion.jIFImport;
import presentacion.jIFOpen;

/**
 *
 * @author Javier
 */
public class workerBorrar extends SwingWorker<Void, Void>{
    
    private jIFImport jiImport;
    private jIFOpen jiOpen;
    private JButton boton;

    public workerBorrar(jIFImport jiImport, JButton boton) {
        this.jiImport = jiImport;
        this.jiOpen = null;
        this.boton = boton;
    }

    public workerBorrar(jIFOpen jiOpen, JButton boton) {
        this.jiImport = null;
        this.jiOpen = jiOpen;
        this.boton = boton;
    }
    
    @Override
    protected Void doInBackground() throws Exception {
        if (boton != null)
            boton.setIcon(new ImageIcon(getClass().getResource("/imagenes/loading.gif")));
        if (jiImport != null)
            jiImport.borrarSeleccionados();
        else if (jiOpen != null){
            jiOpen.eliminar();
        }
        return null;
    }
    
    @Override
    protected void done(){
        if (boton != null)
            boton.setIcon(new ImageIcon(getClass().getResource("/imagenes/Delete-30.png"),"delete close"));
   }
    
}
