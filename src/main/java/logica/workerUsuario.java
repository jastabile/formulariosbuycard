/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.ArrayList;
import javax.swing.SwingWorker;

/**
 *
 * @author Javier
 */
public class workerUsuario extends SwingWorker<String, Void> {
    
    private String opcion;
    private String nombrePc;
    private String usuario;

    public workerUsuario(String opcion, String nombrePc, String usuario) {
        this.opcion = opcion;
        this.nombrePc = nombrePc;
        this.usuario = usuario;
    }
    
    public workerUsuario(String opcion, String nombrePc) {
        this.opcion = opcion;
        this.nombrePc = nombrePc;
        this.usuario = null;
    }
    
    @Override
    protected String doInBackground() throws Exception {
        if (opcion.equals("insertar"))
            persistencia.persistencia.InsertarUsuario(nombrePc, usuario);
        else if (opcion.equals("modificar"))
            persistencia.persistencia.ModificarUsuario(nombrePc, usuario);
        else if (opcion.equals("buscar"))
            return persistencia.persistencia.buscarUsuario(nombrePc);
        return null;
    }
}
