/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

/**
 *
 * @author Javier
 */
public class ramo {
    private int id;
    private String codigo;
    private String nombre;
    private String descuento;
    private String descuentoCuota;

    public ramo(int id, String codigo, String nombre, String descuento, String descuentoCuota) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.descuento = descuento;
        this.descuentoCuota = descuentoCuota;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setDescuentoCuota(String descuentoCuota) {
        this.descuentoCuota = descuentoCuota;
    }

    public String getDescuentoCuota() {
        return descuentoCuota;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }
    
    @Override
    public String toString() {
        return nombre;
    }
}
