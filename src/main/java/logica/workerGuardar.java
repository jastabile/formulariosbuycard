/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingWorker;
import presentacion.Formulario;
import presentacion.jIFConfiguracion;
import presentacion.jIFGenerarForm;
import presentacion.jIFImport;

/**
 *
 * @author Javier
 */
public class workerGuardar extends SwingWorker<Void, Void> {
    
    private Formulario form;
    private jIFImport jiImport;
    private jIFGenerarForm jiGen;
    private jIFConfiguracion jiConfig;
    private JButton boton;

    public workerGuardar(Formulario form, JButton boton) {
        this.form = form;
        this.jiImport = null;
        this.jiGen = null;
        this.boton = boton;
        this.jiConfig = null;
    }
    
    public workerGuardar(jIFImport ji, JButton boton) {
        this.form = null;
        this.jiImport = ji;
        this.jiGen = null;
        this.boton = boton;
        this.jiConfig = null;
    }
    
    public workerGuardar(jIFGenerarForm ji, JButton boton) {
        this.form = null;
        this.jiImport = null;
        this.jiGen = ji;
        this.boton = boton;
        this.jiConfig = null;
    }
    
    public workerGuardar(jIFConfiguracion ji) {
        this.form = null;
        this.jiImport = null;
        this.jiGen = null;
        this.boton = null;
        this.jiConfig = ji;
    }

    @Override
    protected Void doInBackground() throws Exception {
        if (boton != null)
            boton.setIcon(new ImageIcon(getClass().getResource("/imagenes/loading.gif")));
        if (form != null)
            form.guardar();
        else if (jiImport != null)
            jiImport.guardarCambios();
        else if (jiGen != null){
            jiGen.guardarCambios();
            jiGen.actualizarTablaImports();
        }else if (jiConfig != null)
            jiConfig.guardarCambios();
        return null;
    }

    @Override
    protected void done(){
        if (boton != null)
            boton.setIcon(new ImageIcon(getClass().getResource("/imagenes/Save Close.png"),"save close"));
    }

    

}
