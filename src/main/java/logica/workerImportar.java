/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import controladores.Controlador;
import controladores.ControladorExcel;
import datatypes.DTImport;
import excepciones.BDException;
import excepciones.noHay12ColumnasException;
import excepciones.noHay28ColumnasException;
import excepciones.noHay9ColumnasException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JDialog;
import javax.swing.SwingWorker;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

/**
 *
 * @author Javier
 */
public class workerImportar extends SwingWorker<ArrayList<DTImport>, Void> {
    
    private String path;
    private JDialog jd;
    private ArrayList<DTImport> lista;
    private ArrayList<DTImport> listaCambiosAGuardar;

    public workerImportar(String path, JDialog jd, ArrayList<DTImport> lista) {
        this.path = path;
        this.jd = jd;
        this.lista = lista;
        this.listaCambiosAGuardar = new ArrayList();
    }
    
    @Override
    protected ArrayList<DTImport> doInBackground() throws BDException,IOException,InvalidFormatException,noHay12ColumnasException,noHay28ColumnasException,noHay9ColumnasException{
        importarYGuardar(path);
        return lista;
    }
    
    private void importarYGuardar(String path)throws BDException, IOException, InvalidFormatException, noHay12ColumnasException, noHay28ColumnasException, noHay9ColumnasException{
        ArrayList<DTImport> listaNueva = ControladorExcel.ImportarCabalOMaster(new File(path));
        this.lista.addAll(listaNueva);
        listaCambiosAGuardar.addAll(listaNueva);
        guardarCambios();
    }
    
    /** Agrega los que están en lista2 que no estan en this.lista. Si estan en lista1,
        se pone como tipo "MASTER Y CABAL"*/
    private void mergeLists(ArrayList<DTImport> lista2){
        for (DTImport dtLista2 : lista2){
            int indice = existe(dtLista2, lista);
            if (indice != -1){
                if (!sonDelMismoTipo(lista.get(indice), dtLista2)){
                    lista.get(indice).setTipo("MASTER Y CABAL");
                    listaCambiosAGuardar.add(lista.get(indice));
                }
            }else{
                lista.add(0,dtLista2);
                listaCambiosAGuardar.add(dtLista2);
            }
        }
    }
     
     private int existe(DTImport dt, ArrayList<DTImport> lista){
        int i = 0;
        for(DTImport dtLista: lista){
            if (dtLista.getRUT().equals(dt.getRUT()))
                return i;
            i++;
        }
        return -1;
    }
     
    private boolean sonDelMismoTipo(DTImport dt1, DTImport dt2){
        return dt1.getTipo().equals(dt2.getTipo());
    }
    
    public void guardarCambios() throws BDException{
        for (DTImport dt: listaCambiosAGuardar){
            if (dt.getId() == -1){
                int id = Controlador.insertarImport((DTImport)dt);
                dt.setId(id);
            }else{
                Controlador.modificarImport((DTImport)dt);
            }
        }
        listaCambiosAGuardar.clear();
    }
    
    @Override
    protected void done() {
      jd.dispose();
   }
}
