/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

/**
 *
 * @author Javier
 */
public class banco {
    
    private String nombre;
    private String entidadMaster;
    private String entidadCabal;
    private String sucursal;
    private String direccion;

    public banco(String nombre, String sucursal, String direccion) {
        this.nombre = nombre;
        this.sucursal = sucursal;
        this.direccion = direccion;
        if (nombre.equals("REPUBLICA")){
            this.entidadMaster = "218";
            this.entidadCabal = "805 - 091";
        }
        else if (nombre.equals("SANTANDER")){
            this.entidadMaster = "219";
            this.entidadCabal = "805 - 096";
        }
        else if (nombre.equals("SCOTIABANK")){
            this.entidadMaster = "225";
            this.entidadCabal = "805 - 092";
        }
        else if (nombre.equals("BBVA")){
            this.entidadMaster = "221";
            this.entidadCabal = "805 - 090";
        }
        else if (nombre.equals("HSBC") || nombre.equals("HSBC - Premier")){
            this.entidadMaster = "227";
            this.entidadCabal = "805 - 148";
        }
        else if (nombre.equals("ITAU")){
            this.entidadMaster = "223";
            this.entidadCabal = "805 - 098";
        }
        else if (nombre.equals("DISCOUNT")){
            this.entidadMaster = "211";
            this.entidadCabal = "805 - 146";
        }else if (nombre.equals("CITIBANK")){
            this.entidadMaster = "216";
            this.entidadCabal = "";
        }
    }

    public String getNombre() {
        return nombre;
    }

    public String getEntidadMaster() {
        return entidadMaster;
    }

    public String getEntidadCabal() {
        return entidadCabal;
    }

    public String getSucursal() {
        return sucursal;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEntidadMaster(String entidadMaster) {
        this.entidadMaster = entidadMaster;
    }

    public void setEntidadCabal(String entidadCabal) {
        this.entidadCabal = entidadCabal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
    
}
