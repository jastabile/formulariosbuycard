/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingWorker;
import presentacion.Formulario;
import presentacion.jIFFormAnda;
import presentacion.jIFImport;

/**
 *
 * @author Javier
 */
public class workerExportar extends SwingWorker<Void, Void> {
    
    private Formulario form;
    private jIFImport jiImport;
    private JButton boton;

    public workerExportar(Formulario form, JButton boton) {
        this.form = form;
        this.jiImport = null;
        this.boton = boton;
    }
    
    public workerExportar(jIFImport ji, JButton boton) {
        this.form = null;
        this.jiImport = ji;
        this.boton = boton;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Dimension dim = boton.getSize();
        boton.setIcon(new ImageIcon(getClass().getResource("/imagenes/loading.gif")));
        boton.setSize(dim.height,dim.height);
        if (form != null)
            form.exportPDF();    
        else if (jiImport != null)
            jiImport.export();
        return null;
    }

    @Override
    protected void done(){
        if (jiImport != null)
            boton.setIcon(new ImageIcon(getClass().getResource("/imagenes/Export-30.png")));
        else if (form != null){
            if (form instanceof jIFFormAnda)
                boton.setIcon(new ImageIcon(getClass().getResource("/imagenes/word-48.png")));
            else
                boton.setIcon(new ImageIcon(getClass().getResource("/imagenes/PDF 2-48.png")));
        }
   }    
}
