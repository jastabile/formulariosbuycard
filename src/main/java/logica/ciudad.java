/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

/**
 *
 * @author Javier
 */
public class ciudad {
    private String codigoCiudad;
    private String localidad;
    private String codigoPostal;
    private String codigoDepartamento;
    private String departamento;

    public ciudad(String codigoCiudad, String localidad, String codigoPostal, String codigoDepartamento, String departamento) {
        this.codigoCiudad = codigoCiudad;
        this.localidad = localidad;
        this.codigoPostal = codigoPostal;
        this.codigoDepartamento = codigoDepartamento;
        this.departamento = departamento;
    }

    public String getCodigoCiudad() {
        return codigoCiudad;
    }

    public String getLocalidad() {
        return localidad;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setCodigoCiudad(String codigoCiudad) {
        this.codigoCiudad = codigoCiudad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public void setCodigoDepartamento(String codigoDepartamento) {
        this.codigoDepartamento = codigoDepartamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    
    
}
