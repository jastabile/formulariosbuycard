package excepciones;

public class BDException extends Exception {

    public BDException(String message) {
        super(message);
    }
    
}
